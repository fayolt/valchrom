FROM python:3.7.1
RUN mkdir /app
WORKDIR /app
COPY valchrom/requirements.txt /app
RUN apt-get update  && apt-get install -y postgresql-client
RUN pip install -r requirements.txt
COPY . /app
RUN chmod +x /app/wait-for-it.sh
WORKDIR /app

