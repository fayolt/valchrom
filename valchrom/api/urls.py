from django.urls import include, path
from rest_framework_jwt.views import refresh_jwt_token, verify_jwt_token

urlpatterns = [
    path('experiments/', include('experiment_validation.urls')),
    path('accounts/signup/', include('rest_auth.registration.urls')),
    path('accounts/', include('rest_auth.urls')),
    path('token/refresh/', refresh_jwt_token),
    path('token/verify/', verify_jwt_token),
    path('', include('users.urls')),
]