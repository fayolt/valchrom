from django.http import HttpRequest
from django.core import serializers
from collections import defaultdict
from unittest import TestCase, skipIf, main
from django.conf import settings

import pendulum
import numpy as np
import base64
import json
import os
import csv
import time

from experiment_validation.views.experiment_data_uploader_view import ExperimentDataUploaderView
from experiment_validation.models import Analyte, ExperimentData, ExperimentDataValue, ExperimentDataset, AssessmentMethod, Method, Guideline, ExperimentalPlan, ValidationPlanTemplate
from experiment_validation.serializers import ExperimentDataUploaderSerializer, ExperimentDataValueSerializer, GuidelineSerializer, AssessmentMethodSerializer, CriteriaSerializer, ExperimentDataSerializer
from users.models import User

from am_code import assessment_method_checks, assessment_method_calculations

'''
1. How to use?

1.1. Prerequsites
Create virtual environment with valchrom dependencies
https://docs.python.org/3/library/venv.html

1.2. Executeing
Go into directory "<repository>/valchrom"
execute "python3 manage.py test"

1.3. Choosing which tests to run.
Every test has decorator "@skipIf" which switches of or on following test.
There are 2 global switches "MATH_MODULE" and "CHECKS" which turn on/off all
calculations or data check test.
Default swhitches can be overwritten by simply changing their value to correspond to True ar False

1.4. More verbose output
There is switch DEBUG which turns on more verbose output and by default turns off all tests.
Output from DEBUG is
1. output which should match expected output
2. Some info about comparison between expected output and actual output

2. How to further develop new tests for assessment methods?
2.1. Create subdirectory in directory DATA_DIR.
Subdirectory name should correspond to assessment_method_code in Guideline "All assessment methods"
2.2. Create input csv with name INPUT_FILE inside that subdirectory
2.3. Create new test case function which calls for self.case_template
with relevant parameters
2.4. Run the tests (1.2.) with DEBUG switch on (that will disable all test) and
only the new test enabled. Check the output.
If correct, save it as EXPECTED_OUTPUT_FILE in DATA_DIR

3. Overview of components?
For better understanding look below for
* case_template
* case_is_template
* core_for_check_test
* core_for_calculation_test
'''

MATH_MODULES = True
CHECKS = True
DEBUG = False

DIGITS = 7
BASE_DIR = 'tests'
DB_INIT_DIR = os.path.join(BASE_DIR, 'db_init')
DATA_DIR = os.path.join(BASE_DIR, 'calculations_data')
INPUT_FILE = 'input.csv'
EXPECTED_OUTPUT_FILE = 'expected_output.csv'
INPUT_IS_FILE = 'input_is_data.csv'
EXPECTED_OUTPUT_IS_FILE = 'expected_output_is_data.csv'

class CalculationsTests(TestCase):
    @skipIf((DEBUG or not MATH_MODULES), '')
    def test_lod_all_2019_03_28_cal_blanks(self):
        dir = 'lod_all_2019_03_28_cal_blanks'
        self.case_template(dir, 'LOD:All:2019-03-28:cal_blanks')

    @skipIf((DEBUG or not MATH_MODULES), '')
    def test_lod_all_2019_03_28_cal_blanks_is_data(self):
        dir = 'lod_all_2019_03_28_cal_blanks'
        self.case_is_template(dir, 'LOD:All:2019-03-28:cal_blanks')

    @skipIf((DEBUG or not MATH_MODULES), '')
    def test_lod_all_2019_03_28_cal_y(self):
        dir = 'lod_all_2019_03_28_cal_y'
        self.case_template(dir, 'LOD:All:2019-03-28:cal_y')

    @skipIf((DEBUG or not MATH_MODULES), '')
    def test_lod_all_2019_03_28_cal_y_is_data(self):
        dir = 'lod_all_2019_03_28_cal_y'
        self.case_is_template(dir, 'LOD:All:2019-03-28:cal_y')

    @skipIf((DEBUG or not MATH_MODULES), '')
    def test_lod_all_2019_03_28_cal_res(self):
        dir = 'lod_all_2019_03_28_cal_res'
        self.case_template(dir, 'LOD:All:2019-03-28:cal_res')

    @skipIf((DEBUG or not MATH_MODULES), '')
    def test_lod_all_2019_03_28_cal_res_is_data(self):
        dir = 'lod_all_2019_03_28_cal_res'
        self.case_is_template(dir, 'LOD:All:2019-03-28:cal_res')

    @skipIf((DEBUG or not MATH_MODULES), '')
    def test_lod_all_2019_03_28_sd_6spike(self):
        dir = 'lod_all_2019_03_28_sd_6spike'
        self.case_template(dir, 'LOD:All:2019-03-28:sd_6spike')

    @skipIf((DEBUG or not MATH_MODULES), '')
    def test_lod_all_2019_03_28_sd_6spike_is_data(self):
        dir = 'lod_all_2019_03_28_sd_6spike'
        self.case_is_template(dir, 'LOD:All:2019-03-28:sd_6spike')

    @skipIf((DEBUG or not MATH_MODULES), '')
    def test_lod_all_2019_03_28_cca_ccb_homo(self):
        dir = 'lod_all_2019_03_28_cca_ccb_homo'
        code_name = 'LOD:All:2019-03-28:cca_ccb_homo'
        def insert_params():
            def insert_param(name, value, analyte_id):
                am_id = AssessmentMethod.objects.get(code_name=code_name).id
                id = ExperimentData.objects.get(name=name).id
                ExperimentDataValue(value=value,
                    experiment_data_id=id,
                    experiment_dataset_id=1,
                    assessment_method_id=am_id,
                    analyte_id=analyte_id
                ).save()

            insert_param('alfa', 0.05, None)
            insert_param('beta', 0.05, None)
            insert_param('K', 1, None)

        self.case_template(dir, code_name, after_upload_fn=insert_params)

    @skipIf(True, '')
    def test_lod_all_2019_03_28_cca_ccb_homo_mismatch(self):
        # print('lod_all_2019_03_28_cca_ccb_homo_mismatch')
        dir = os.path.join(DATA_DIR, 'lod_all_2019_03_28_cca_ccb_homo')
        in_file = os.path.join(dir, 'input_mismatch.csv')
        out_file = os.path.join(dir, 'expected_output.csv')
        self.core_for_calculation_test(in_file, 'LOD:All:2019-03-28:cca_ccb_homo', 1, out_file)

    @skipIf((DEBUG or not MATH_MODULES), '')
    def test_lod_all_2019_03_28_cca_ccb_hetero(self):
        dir = 'lod_all_2019_03_28_cca_ccb_hetero'
        code_name = 'LOD:All:2019-03-28:cca_ccb_hetero'
        def insert_params():
            def insert_param(name, value, analyte_id):
                am_id = AssessmentMethod.objects.get(code_name=code_name).id
                id = ExperimentData.objects.get(name=name).id
                ExperimentDataValue(value=value,
                    experiment_data_id=id,
                    experiment_dataset_id=1,
                    assessment_method_id=am_id,
                    analyte_id=analyte_id
                ).save()

            insert_param('alfa', 0.05, None)
            insert_param('beta', 0.05, None)
            insert_param('K', 1, None)
            # insert_param('alfa', 0.05, 2)
            # insert_param('beta', 0.05, 2)
            # insert_param('K', 1, 2)

        self.case_template(dir, code_name, after_upload_fn=insert_params)

    @skipIf((DEBUG or not MATH_MODULES), '')
    def test_lin_all_2019_03_28_1_5_1(self):
        dir = 'lin_all_2019_03_28_1_5_1'
        self.case_template(dir, 'LIN:All:2019-03-28:1_5_1')

    @skipIf((DEBUG or not MATH_MODULES), '')
    def test_lin_all_2019_03_28_1_5_1_is_data(self):
        dir = 'lin_all_2019_03_28_1_5_1'
        self.case_is_template(dir, 'LIN:All:2019-03-28:1_5_1')

    @skipIf((DEBUG or not MATH_MODULES), '')
    def test_lin_all_2019_03_28_1_5_3(self):
        dir = 'lin_all_2019_03_28_1_5_3'
        self.case_template(dir, 'LIN:All:2019-03-28:1_5_3')

    @skipIf((DEBUG or not MATH_MODULES), '')
    def test_lin_all_2019_03_28_1_5_3_abr(self):
        dir = os.path.join(DATA_DIR, 'lin_all_2019_03_28_1_5_3')
        in_file = os.path.join(dir, 'input_abr.csv')
        out_file = os.path.join(dir, 'expected_output_abr.csv')
        self.core_for_calculation_test(in_file, 'LIN:All:2019-03-28:1_5_3', 1, out_file)

    @skipIf((DEBUG or not MATH_MODULES), '')
    def test_lin_all_2019_03_28_3_5_1(self):
        dir = 'lin_all_2019_03_28_3_5_1'
        self.case_template(dir, 'LIN:All:2019-03-28:3_5_1')

    @skipIf((DEBUG or not MATH_MODULES), '')
    def test_lin_all_2019_03_28_1_6_1(self):
        dir = 'lin_all_2019_03_28_1_6_1'
        self.case_template(dir, 'LIN:All:2019-03-28:1_6_1')

    @skipIf((DEBUG or not MATH_MODULES), '')
    def test_lin_all_2019_03_28_2_6_1(self):
        dir = 'lin_all_2019_03_28_2_6_1'
        self.case_template(dir, 'LIN:All:2019-03-28:2_6_1')

    @skipIf((DEBUG or not MATH_MODULES), '')
    def test_lin_all_2019_03_28_3_6_1(self):
        dir = 'lin_all_2019_03_28_3_6_1'
        self.case_template(dir, 'LIN:All:2019-03-28:3_6_2')

    @skipIf((DEBUG or not MATH_MODULES), '')
    def test_prec_all_2019_03_28_1_1_6(self):
        dir = 'prec_all_2019_03_28_1_1_6'
        self.case_template(dir, 'PREC:All:2019-03-28:1_1_6')

    @skipIf((DEBUG or not MATH_MODULES), '')
    def test_prec_all_2019_03_28_1_1_6_is_data(self):
        dir = 'prec_all_2019_03_28_1_1_6'
        self.case_is_template(dir, 'PREC:All:2019-03-28:1_1_6')

    @skipIf((DEBUG or not MATH_MODULES), '')
    def test_prec_all_2019_03_28_1_3_3(self):
        dir = 'prec_all_2019_03_28_1_3_3'
        self.case_template(dir, 'PREC:All:2019-03-28:1_3_3')

    @skipIf((DEBUG or not MATH_MODULES), '')
    def test_prec_all_2019_03_28_1_3_3_is_data(self):
        dir = 'prec_all_2019_03_28_1_3_3'
        self.case_is_template(dir, 'PREC:All:2019-03-28:1_3_3')

    @skipIf((DEBUG or not MATH_MODULES), '')
    def test_prec_all_2019_03_28_1_3_3_experimental_conc(self):
        dir = os.path.join(DATA_DIR, 'prec_all_2019_03_28_1_3_3')
        in_file = os.path.join(dir, 'input_experimental.csv')
        out_file = os.path.join(dir, 'expected_output_experimental.csv')
        self.core_for_calculation_test(in_file, 'PREC:All:2019-03-28:1_3_3', 1, out_file)

    @skipIf((DEBUG or not MATH_MODULES), '')
    def test_prec_all_2019_03_28_1_3_3_area(self):
        dir = os.path.join(DATA_DIR, 'prec_all_2019_03_28_1_3_3')
        in_file = os.path.join(dir, 'input_area.csv')
        out_file = os.path.join(dir, 'expected_output_area.csv')
        self.core_for_calculation_test(in_file, 'PREC:All:2019-03-28:1_3_3', 1, out_file)

    @skipIf((DEBUG or not MATH_MODULES), '')
    def test_prec_all_2019_03_28_1_4_5(self):
        dir = 'prec_all_2019_03_28_1_4_5'
        self.case_template(dir, 'PREC:All:2019-03-28:1_4_5')

    @skipIf((DEBUG or not MATH_MODULES), '')
    def test_prec_all_2019_03_28_2_1_6(self):
        dir = 'prec_all_2019_03_28_2_1_6'
        self.case_template(dir, 'PREC:All:2019-03-28:2_1_6')

    @skipIf((DEBUG or not MATH_MODULES), '')
    def test_prec_all_2019_03_28_2_1_6_is_data(self):
        dir = 'prec_all_2019_03_28_2_1_6'
        self.case_is_template(dir, 'PREC:All:2019-03-28:2_1_6')

    @skipIf((DEBUG or not MATH_MODULES), '')
    def test_prec_all_2019_03_28_2_3_3(self):
        dir = 'prec_all_2019_03_28_2_3_3'
        self.case_template(dir, 'PREC:All:2019-03-28:2_3_3')

    @skipIf((DEBUG or not MATH_MODULES), '')
    def test_prec_all_2019_03_28_2_3_3_is_data(self):
        dir = 'prec_all_2019_03_28_2_3_3'
        self.case_is_template(dir, 'PREC:All:2019-03-28:2_3_3')

    @skipIf((DEBUG or not MATH_MODULES), '')
    def test_prec_all_2019_03_28_3_4_1(self):
        dir = 'prec_all_2019_03_28_3_4_1'
        self.case_template(dir, 'PREC:All:2019-03-28:3_4_1')

    @skipIf((DEBUG or not MATH_MODULES), '')
    def test_acc_all_2019_03_28_1_3_3(self):
        dir = 'acc_all_2019_03_28_1_3_3'
        self.case_template(dir, 'ACC:All:2019-03-28:1_3_3')

    @skipIf((DEBUG or not MATH_MODULES), '')
    def test_acc_all_2019_03_28_1_3_3_error(self):
        dir = os.path.join(DATA_DIR, 'acc_all_2019_03_28_1_3_3')
        in_file = os.path.join(dir, '5 Acc 2x3x3.csv')
        out_file = os.path.join(dir, '5 Acc 2x3x3_output.csv')
        self.core_for_calculation_test(in_file, 'ACC:All:2019-03-28:1_3_3', 1, out_file)

    @skipIf((DEBUG or not MATH_MODULES), '')
    def test_acc_all_2019_03_28_ref_samp(self):
        dir = 'acc_all_2019_03_28_ref_samp'
        self.case_template(dir, 'ACC:All:2019-03-28:ref_samp')

    @skipIf((DEBUG or not MATH_MODULES), '')
    def test_acc_all_2019_03_28_ref_method(self):
        dir = 'acc_all_2019_03_28_ref_method'
        self.case_template(dir, 'ACC:All:2019-03-28:ref_method')

    @skipIf((DEBUG or not MATH_MODULES), '')
    def test_acc_all_2019_03_28_1_1_10(self):
        dir = 'acc_all_2019_03_28_1_1_10'
        self.case_template(dir, 'ACC:All:2019-03-28:1_1_10')

    @skipIf((DEBUG or not MATH_MODULES), '')
    def test_acc_all_2019_03_28_1_4_5(self):
        dir = 'acc_all_2019_03_28_1_4_5'
        self.case_template(dir, 'ACC:All:2019-03-28:1_4_5')

    @skipIf((DEBUG or not MATH_MODULES), '')
    def test_acc_all_2019_03_28_3_4_1(self):
        dir = 'acc_all_2019_03_28_3_4_1'
        self.case_template(dir, 'ACC:All:2019-03-28:3_4_1')

    @skipIf((DEBUG or not MATH_MODULES), '')
    def test_acc_all_2019_03_28_3_4_1_NaNs(self):
        dir = os.path.join(DATA_DIR, 'acc_all_2019_03_28_3_4_1')
        in_file = os.path.join(dir, 'input_NaNs.csv')
        out_file = os.path.join(dir, 'expected_output_NaNs.csv')
        self.core_for_calculation_test(in_file, 'ACC:All:2019-03-28:3_4_1', 1, out_file)

    @skipIf((DEBUG or not MATH_MODULES), '')
    def test_acc_all_2019_03_28_3_4_1_NaNs_lvlsasints(self):
        dir = os.path.join(DATA_DIR, 'acc_all_2019_03_28_3_4_1')
        in_file = os.path.join(dir, 'input_NaNs_lvlsasints.csv')
        out_file = os.path.join(dir, 'expected_output_NaNs_lvlsasints.csv')
        self.core_for_calculation_test(in_file, 'ACC:All:2019-03-28:3_4_1', 1, out_file)

    @skipIf((DEBUG or not MATH_MODULES), '')
    def test_rob_all_2019_03_28(self):
        dir = 'rob_all_2019_03_28'
        self.case_template(dir, 'ROB:All:2019-03-28:ph')

    @skipIf((DEBUG or not MATH_MODULES), '')
    def test_rob_all_2019_03_28_is_data(self):
        dir = 'rob_all_2019_03_28'
        self.case_is_template(dir, 'ROB:All:2019-03-28:ph')

    @skipIf((DEBUG or not MATH_MODULES), '')
    def test_rug_all_2019_03_28_stability(self):
        dir = os.path.join(DATA_DIR, 'rug_all_2019_03_28_stability')
        in_file = os.path.join(dir, 'input_full.csv')
        out_file = os.path.join(dir, 'expected_output_full.csv')
        self.core_for_calculation_test(in_file, 'RUG:All:2019-03-28:freeze_thaw', 1, out_file)

    @skipIf((DEBUG or not MATH_MODULES), '')
    def test_rug_all_2019_03_28_stability_is_data(self):
        dir = 'rug_all_2019_03_28_stability'
        self.case_is_template(dir, 'RUG:All:2019-03-28:freeze_thaw')

    @skipIf((DEBUG or not MATH_MODULES), '')
    def test_rug_all_2019_03_28_analyst(self):
        dir = 'rug_all_2019_03_28_analyst'
        self.case_template(dir, 'RUG:All:2019-03-28:analyst')

    @skipIf((DEBUG or not MATH_MODULES), '')
    def test_sst_all_2019_03_28(self):
        dir = 'sst_all_2019_03_28'
        self.case_template(dir, 'SST:All:2019-03-28:sst')

    @skipIf((DEBUG or not MATH_MODULES), '')
    def test_sst_all_2019_03_28_is_data(self):
        dir = 'sst_all_2019_03_28'
        self.case_is_template(dir, 'SST:All:2019-03-28:sst')

    # Templates for usual testcases

    def case_template(self, name, am_code, after_upload_fn=None):
        ''' Creates simple test case from few variables. Used for first smoke test
        @param name - subdirectory in directory "calculation_data" where test data is located
        @param am_code - assessment_method.code_name to be tested
        @after_upload_fn - function to execute after data upload. Used for inserting extra data, like in CCalfa-CCbeta
        '''
        dir = os.path.join(DATA_DIR, name)
        in_file = os.path.join(dir, INPUT_FILE)
        out_file = os.path.join(dir, EXPECTED_OUTPUT_FILE)
        self.core_for_calculation_test(in_file, am_code, 1, out_file, after_upload_fn=after_upload_fn)

    def case_is_template(self, name, am_code, after_upload_fn=None):
        ''' Creates simple test case from few variables.
        Used for first smoke testing with internal standard (IS) data
        @param name - subdirectory in directory "calculation_data" where test data is located
        @param am_code - assessment_method.code_name to be tested
        @after_upload_fn - function to execute after data upload. Used for inserting extra data, like in CCalfa-CCbeta
        '''
        dir = os.path.join(DATA_DIR, name)
        in_file = os.path.join(dir, INPUT_IS_FILE)
        out_file = os.path.join(dir, EXPECTED_OUTPUT_IS_FILE)
        self.core_for_calculation_test(in_file, am_code, 1, out_file, after_upload_fn=after_upload_fn)

    # Tests for data checks

    @skipIf((DEBUG or not CHECKS), '')
    def test_lod_all_2019_03_28_cal_blanks_check(self):
        dir = os.path.join(DATA_DIR, 'lod_all_2019_03_28_cal_blanks')
        in_file = os.path.join(dir, INPUT_FILE)
        self.core_for_check_test(in_file, 'LOD:All:2019-03-28:cal_blanks', [])

    @skipIf((DEBUG or not CHECKS), '')
    def test_lod_all_2019_03_28_cal_res_check(self):
        dir = os.path.join(DATA_DIR, 'lod_all_2019_03_28_cal_res')
        in_file = os.path.join(dir, INPUT_FILE)
        self.core_for_check_test(in_file, 'LOD:All:2019-03-28:cal_res', [])

    @skipIf((DEBUG or not CHECKS), '')
    def test_lod_all_2019_03_28_cal_blanks_level_missing_check(self):
        dir = os.path.join(DATA_DIR, 'lod_all_2019_03_28_cal_blanks')
        in_file = os.path.join(dir, 'level_missing.csv')
        self.core_for_check_test(in_file, 'LOD:All:2019-03-28:cal_blanks', [1])

    @skipIf((DEBUG or not CHECKS), '')
    def test_lod_all_2019_03_28_cal_blanks_area_missing_check(self):
        dir = os.path.join(DATA_DIR, 'lod_all_2019_03_28_cal_blanks')
        in_file = os.path.join(dir, 'area_missing.csv')
        self.core_for_check_test(in_file, 'LOD:All:2019-03-28:cal_blanks', [1, 4, 16, 1, 4, 32])

    @skipIf((DEBUG or not CHECKS), '')
    def test_lod_all_2019_03_28_cal_blanks_area_empty_check(self):
        dir = os.path.join(DATA_DIR, 'lod_all_2019_03_28_cal_blanks')
        in_file = os.path.join(dir, 'area_empty.csv')
        self.core_for_check_test(in_file, 'LOD:All:2019-03-28:cal_blanks', [4, 32])

    @skipIf((DEBUG or not CHECKS), '')
    def test_lod_all_2019_03_28_cal_blanks_is_check(self):
        dir = os.path.join(DATA_DIR, 'lod_all_2019_03_28_cal_blanks')
        in_file = os.path.join(dir, 'input_is_data.csv')
        self.core_for_check_test(in_file, 'LOD:All:2019-03-28:cal_blanks', [])

    @skipIf((DEBUG or not CHECKS), '')
    def test_lod_all_2019_03_28_cal_blanks_is_fail_check(self):
        dir = os.path.join(DATA_DIR, 'lod_all_2019_03_28_cal_blanks')
        in_file = os.path.join(dir, 'input_is_data_false.csv')
        self.core_for_check_test(in_file, 'LOD:All:2019-03-28:cal_blanks', [128])

    @skipIf((DEBUG or not CHECKS), '')
    def test_lin_all_2019_03_28_1_5_3_check(self):
        dir = os.path.join(DATA_DIR, 'lin_all_2019_03_28_1_5_3')
        in_file = os.path.join(dir, 'input_abr.csv')
        self.core_for_check_test(in_file, 'LIN:All:2019-03-28:1_5_3', [])

    @skipIf((DEBUG or not CHECKS), '')
    def test_prec_all_2019_03_28_1_1_6_calibration_1_row_check(self):
        dir = os.path.join(DATA_DIR, 'prec_all_2019_03_28_1_1_6')
        in_file = os.path.join(dir, 'calibration_1_row.csv')
        self.core_for_check_test(in_file, 'PREC:All:2019-03-28:1_1_6', [8])

    @skipIf((DEBUG or not CHECKS), '')
    def test_prec_all_2019_03_28_1_1_6_calibration_exp_conc_missing_check(self):
        dir = os.path.join(DATA_DIR, 'prec_all_2019_03_28_1_1_6')
        in_file = os.path.join(dir, 'calibration_exp_conc_missing.csv')
        self.core_for_check_test(in_file, 'PREC:All:2019-03-28:1_1_6', [16])

    @skipIf((DEBUG or not CHECKS), '')
    def test_prec_all_2019_03_28_1_1_6_sample_missing_area_check(self):
        dir = os.path.join(DATA_DIR, 'prec_all_2019_03_28_1_1_6')
        in_file = os.path.join(dir, 'sample_area_missing.csv')
        self.core_for_check_test(in_file, 'PREC:All:2019-03-28:1_1_6', [32])

    @skipIf((DEBUG or not CHECKS), '')
    def test_prec_all_2019_03_28_1_1_6_replicate_short_check(self):
        dir = os.path.join(DATA_DIR, 'prec_all_2019_03_28_1_1_6')
        in_file = os.path.join(dir, 'replicate_short.csv')
        self.core_for_check_test(in_file, 'PREC:All:2019-03-28:1_1_6', [64])


    def core_for_check_test(self, input, assessment_method_code, expected_output):
        """
        Check if "input" gives "expected_output" list of errors for
        assessment method related by "assessment_method_code"

        @param input - path to .csv file to be used as assessment method input
        @param assessment_method_code - assessment method code to check data with
        @param expected_output - list of expected error codes
        """
        assessment_method = AssessmentMethod.objects.get(code_name=assessment_method_code)

        target_keys = ['marker', 'series', 'level', 'parallel']
        experiment_data_values = []
        experiment_dataset = ExperimentDataset.objects.get(id=1)

        index = 0
        with open(input) as csvfile:
            csv_reader = csv.DictReader(csvfile, delimiter=",")
            for row in csv_reader:
                if DEBUG:
                    print('Row: {}'.format(row))
                target_dict = {target: row.pop(target, None) for target in target_keys}
                if target_dict.get('series') == None:
                    target_dict['series'] = f"Uploaded on {pendulum.today(tz=settings.TIME_ZONE).to_date_string()}"
                analyte = Analyte.objects.filter(abbreviation=row.pop('analyte'),
                        method=experiment_dataset.experimental_plan.method)[0]
                index += 1
                for key in row:
                    experiment_data = ExperimentData.objects.get(name=key)
                    experiment_data_values.append(
                        ExperimentDataValue(value=row[key],
                            experiment_data=experiment_data,
                            experiment_dataset_id=1, # experiment_dataset,
                            assessment_method_id=assessment_method.id,#assessment_method,
                            row=index,
                            analyte=analyte,
                            **target_dict
                        )
                    )

        result = assessment_method_checks[assessment_method_code](experiment_data_values)

        self.assertEqual(result, expected_output)


    def core_for_calculation_test(self, input, assessment_method_code, dataset_id, expected_output, after_upload_fn=None):
        """
        Checks if running assessment method "assessment_method_code" on data from "input"
        gives result as expected in expected_output.
        Function expects analytes to be either 'ACE' or 'THC' (abbreviation).

        @param input - path to .csv file to be used as assessment method input
        @param assessment_method_code - assessment method code to check data with
        @param dataset_id - dataset id to use for uploading data. Inserted in function "setUpClass" and expected to usually be 1
        @param expected_output - path to .csv file to expected output
        @param after_upload_fn - function to execute after data upload.
            Usually used to uplload extra parameters for assessment method.
            For example CCalfa-CCbeta.
        """
        assessment_method = AssessmentMethod.objects.get(code_name=assessment_method_code)

        # load csv to DB
        with open(input, 'r') as file:
            raw_data = file.read()
            csv_data = {}
            csv_data['data'] = [base64.b64encode(bytes(raw_data, 'utf-8')).decode('utf-8')]
            csv_data['experiment_dataset'] = str(dataset_id)
            csv_data['assessment_method'] = str(assessment_method.id)
            csv_data['analyte'] = None
            csv_data['file_type'] = 'excel'
        request = HttpRequest()
        request.data = csv_data

        res = ExperimentDataUploaderView().post(request, dataset_id, assessment_method.id, format=None)
        # print('Dataset: {} Assessmentmethod id: {}'.format(dataset_id, assessment_method.id))
        # print('Result: {}'.format(res))

        if after_upload_fn:
            after_upload_fn()

        # execute math module
        assessment_method_calculations[assessment_method_code](dataset_id, assessment_method.id, 1)
        assessment_method_calculations[assessment_method_code](dataset_id, assessment_method.id, 2)

        # read in expected results
        output_labels = map(lambda x: x.id, ExperimentData.objects.filter(type=ExperimentData.OUTPUT))
        data = ExperimentDataValue.objects.filter(\
            experiment_dataset=dataset_id, assessment_method=assessment_method.id, experiment_data_id__in=output_labels
            ).prefetch_related('experiment_data').order_by('row')

        if DEBUG:
            inner_values = defaultdict(list)
            values = defaultdict(list)

            # # using default serializer
            # for data_value in list(data):
            #     index = data_value.row if data_value.row else 'series_overall'
            #     inner_values[index].append(ExperimentDataValueSerializer(data_value).data)

            # DON'T USE rounding for value
            for data_value in list(data):
                index = data_value.row if data_value.row else 'series_overall'
                dv_dict = {}
                # dv_dict = ExperimentDataValueSerializer(data_value).data
                dv_dict['value'] = data_value.value
                dv_dict['experiment_data'] = data_value.experiment_data.name
                dv_dict['series'] = data_value.series
                dv_dict['level'] = data_value.level
                dv_dict['parallel'] = data_value.parallel
                dv_dict['marker'] = data_value.marker
                dv_dict['row'] = data_value.row
                dv_dict['analyte'] = data_value.analyte_id
                inner_values[index].append(dv_dict)
            for key in inner_values:
                values[key] = values.get(key, []) + inner_values[key]

            print('name,value,series,level,parallel,marker,row,analyte')

            for row in values:
                # print('Row: {}'.format(row))
                # print('Metadata: series {} level {} parallel {} marker {}'.format(values[row][0]['series'], values[row][0]['level'], values[row][0]['parallel'], values[row][0]['marker']))
                for col in values[row]:
                    # usual output: print('Col: value {}, experiment_data {}, id {}, analyte {}'.format(col['value'], col['experiment_data'], col['id'], col['analyte']))
                    # with series: print('Col: value {}, experiment_data {}, id {}, analyte {}, series {}, '.format(col['value'], col['experiment_data'], col['id'], col['analyte'], col['series']))
                    # .csv output
                    print('{},{},{},{},{},{},{},{} '.format(col['experiment_data'], col['value'], col['series'], col['level'], col['parallel'], col['marker'], col['row'], col['analyte']))

        # assert
        row_count = 0
        with open(expected_output, 'r') as out_file:
            csv_reader = csv.DictReader(out_file)
            for row in csv_reader:
                row_count += 1
                # find correct row from output
                ed = ExperimentData.objects.get(name=row['name'])
                d_id = ed.id
                match_experiment_data = data.filter(experiment_data_id=d_id, analyte_id=int(row['analyte']))
                if row['series']:
                    match_series = match_experiment_data.filter(series=row['series'])
                else:
                    match_series = match_experiment_data.filter(series__isnull=True)
                if row['level']:
                    match_level = match_series.filter(level=row['level'])
                else:
                    match_level = match_series.filter(level__isnull=True)
                if row['parallel']:
                    match_parallel = match_level.filter(parallel=int(row['parallel']))
                else:
                    match_parallel = match_level.filter(parallel__isnull=True)
                if row['marker']:
                    match_marker = match_parallel.filter(marker=row['marker'])
                else:
                    match_marker = match_parallel.filter(parallel__isnull=True)
                if row['row']:
                    match = match_marker.filter(row=int(row['row']))
                else:
                    match = match_marker.filter(row__isnull=True)
                if DEBUG:
                    print('Row: {}'.format(row))
                    for m in match.all():
                        print('Match: {}: {} series: {}, level: {}, parallel: {}, row: {}'.format(ed.name, m.value, m.series, m.level, m.parallel, m.row))
                # assert
                self.assertEqual(len(match), 1, 'No match for row: {}'.format(row))
                if ed.name not in ['linear regression graph', 'residuals graph', 'stability graph']:
                    self.assertTrue(self.almostEqual(float(row['value']), float(match[0].value), DIGITS), \
                        'Name: {}, Value: {}, Series: {}, Level: {}, Parallel: {}, Marker: {}, Row: {}, Analyte: {} \n  Did not match row: {}'\
                        .format(match[0].experiment_data.name, match[0].value, match[0].series, match[0].level, match[0].parallel, match[0].marker, match[0].row, match[0].analyte_id, row))

        self.assertEqual(len(data), row_count)

    def almostEqual(self, a, b, digits):
        epsilon = 10 ** -digits
        if b == 0.0:
            return abs(a) < epsilon
        else:
            return abs(a / b - 1) < epsilon

    @classmethod
    def setUpClass(cls):

        '''
        Insert relevant data to DB, to enable data upload and math module running.
        Data is in DB_INIT_DIR
        * Guideline.json
        * AssesmentMethodForG{1-4}.json
        * CriteriaForG1.json
        * DataTypes.json
        * user
        * analytes 'ACE', 'THC'
        * ValidationPlanTemplate
        * ExperimentalPlan
        * ExperimentDataset
        '''
        with open(os.path.join(DB_INIT_DIR, 'Guideline.json'), 'r') as file:
            guidelines = json.load(file)
            serializer = GuidelineSerializer(data=guidelines, many=True)
            if serializer.is_valid():
                serializer.save()
            else:
                print('Errors in serializer: {}'.format(serializer.errors))

        for val in range(1,5):
            am_file_name = 'AssesmentMethodForG{}.json'.format(val)
            with open(os.path.join(DB_INIT_DIR, am_file_name), 'r') as file:
                assmets = json.load(file)
                for am in assmets:
                    serializer = AssessmentMethodSerializer(data=am)
                    if serializer.is_valid():
                        serializer.save()
                    else:
                        print('Errors in serializer: {}'.format(serializer.errors))

        with open(os.path.join(DB_INIT_DIR, 'CriteriaForG1.json'), 'r') as file:
            criterias = json.load(file)
            for c in criterias:
                serializer = CriteriaSerializer(data=c)
                if serializer.is_valid():
                    serializer.save()
                else:
                    print('Errors in serializer: {}'.format(serializer.errors))

        with open(os.path.join(DB_INIT_DIR, 'DataTypes.json'), 'r') as file:
            datatypes = json.load(file) # file.read()
            for dt in datatypes:
                serializer = ExperimentDataSerializer(data=dt)
                if serializer.is_valid():
                    serializer.save()
                else:
                    print('Errors in serializer: {}'.format(serializer.errors))

        guid = Guideline.objects.get(id =1)

        user = User(email = 'dummy@dummy.dummy')
        user.save()

        method = Method(method_description = '',
            method_file = '',
            method_description_file = '',
            concentration_unit = 'cU',
            area_unit = 'aU',
            owner=user
            )
        method.save()

        acetamiprid = Analyte(
            short_name = 'acetamiprid',
            official_name = 'acetamiprid',
            abbreviation = 'ACE',
            target_concentration = 0.1,
            method = method)
        acetamiprid.save()

        thiacloprid = Analyte(
            short_name = 'thiacloprid',
            official_name = 'thiacloprid',
            abbreviation = 'THC',
            target_concentration = 0.1,
            method = method)
        thiacloprid.save()

        # ima = Analyte(
        #     short_name = 'ima',
        #     official_name = 'ima',
        #     abbreviation = 'IMA',
        #     target_concentration = 0.1,
        #     method = method)
        # ima.save()
        #
        # bup = Analyte(
        #     short_name = 'bup',
        #     official_name = 'bup',
        #     abbreviation = 'BUP',
        #     target_concentration = 0.1,
        #     method = method)
        # bup.save()

        lod_residuals = AssessmentMethod.objects.get(id=6)

        t = ValidationPlanTemplate(    comment = '',
            guideline = guid)
        t.save()
        t.assessment_methods.add(lod_residuals)

        ep = ExperimentalPlan(method = method,
            template = t)
        ep.save()

        ds = ExperimentDataset(experimental_plan = ep)
        ds.save()

        return 0

    @classmethod
    def tearDownClass(cls):
        # ExperimentDataValue.objects.all().delete()
        Guideline.objects.all().delete()
        AssessmentMethod.objects.all().delete()
        # Criteria.objects.all().delete()
        ExperimentData.objects.all().delete()
        Method.objects.all().delete()
        Analyte.objects.all().delete()
        ValidationPlanTemplate.objects.all().delete()
        ExperimentalPlan.objects.all().delete()
        ExperimentDataset.objects.all().delete()
        return 0

    def setUp(self):
        return 0

    def tearDown(self):
        ExperimentDataValue.objects.all().delete()
        return 0
