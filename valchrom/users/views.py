from rest_framework import viewsets, permissions
from . import models
from . import serializers

from allauth.socialaccount.providers.facebook.views import FacebookOAuth2Adapter
from rest_auth.registration.views import SocialLoginView

class UserViewSet(viewsets.ModelViewSet):
    queryset = models.User.objects.all().order_by('-date_joined')
    serializer_class = serializers.UserSerializer
    permission_classes = (permissions.IsAuthenticated, )

class FacebookLogin(SocialLoginView):
    adapter_class = FacebookOAuth2Adapter
