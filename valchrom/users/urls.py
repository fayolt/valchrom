from django.urls import include, path
from rest_framework.routers import DefaultRouter
from . import views

class CustomRouter(DefaultRouter):
    def __init__(self, *args, **kwargs): 
        super(DefaultRouter, self).__init__(*args, **kwargs) 
        self.trailing_slash = '/?' 

router = CustomRouter()
router.register(r'users', views.UserViewSet)

urlpatterns = [
    path('account/signup/facebook/', views.FacebookLogin.as_view(), name='fb_login'),
    path('', include(router.urls)),
]