from django.apps import AppConfig


class ExperimentalPlanConfig(AppConfig):
    name = 'experiment_validation'
