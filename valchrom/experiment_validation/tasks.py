from celery import task, shared_task
from celery.utils.log import get_task_logger
from django.core.cache import cache
from django.conf import settings
from am_code import assessment_method_checks, assessment_method_calculations
from .models import ExperimentData, ExperimentDataValue, AssessmentMethod
from . import serializers
from collections import defaultdict
import os
import pendulum
import copy


logger = get_task_logger(__name__)

@shared_task
def invalidate_cache(key):
    """Invalidates cached experiment data value"""
    logger.info("Invalidates cached experiment data value")
    try:
        cache.get(key)
    except:
        pass
    else:
        cache.delete(key)


@shared_task
def execute_math_module(dataset, assessment_method):
    """Runs math module after experiment data is validated"""
    logger.info("Execute math module")
    values = cache.get(f'dataset-{dataset}:assessment-{assessment_method}:output', None)
    if values is None:
        am = AssessmentMethod.objects.get(id=assessment_method)
        if am.code_name in assessment_method_calculations:
            analytes = ExperimentDataValue.objects.values_list(
                "analyte", flat=True
            ).filter(
                experiment_dataset=dataset, assessment_method=assessment_method, experiment_data__type=ExperimentData.INPUT
            ).distinct()
            for analyte in analytes:
                assessment_method_calculations[am.code_name](dataset, assessment_method, analyte)
            data = ExperimentDataValue.objects.filter(experiment_dataset=dataset, assessment_method=assessment_method
                ).prefetch_related('experiment_data').order_by('row')
            values = defaultdict(list)
            # TODO: extract this into a helper lib
            def internals(a_dict, b_dict):
                target_keys = ['id', 'value', 'experiment_data', 'status', 'rounded_value']
                for key in target_keys:
                    b_dict.pop(key)
                return {**a_dict, **b_dict}
            rows_data = data.exclude(row__isnull=True).exclude(row=0)
            rows = rows_data.values_list("row", flat=True).distinct()
            for row in rows:
                row_dict = {x.experiment_data.abbreviation if x.experiment_data.abbreviation else x.experiment_data.official_name: serializers.ExperimentDataValueSerializer(x).data for x in rows_data if x.row == row}
                row_dict = internals(row_dict, copy.deepcopy(row_dict[next(iter(row_dict))]))

                values[row].append(row_dict)
            
            chromatograms_data = data.filter(row=0)
            if chromatograms_data:
                for chromatogram in chromatograms_data:
                    values[0].append(serializers.ExperimentDataValueSerializer(chromatogram).data)

            series_data = data.filter(row__isnull=True)
            if data[0].assessment_method.validation_parameter in [AssessmentMethod.PRECISION, AssessmentMethod.ACCURACY] or "CCα and CCβ" in data[0].assessment_method.name:
                analytes_series_levels = series_data.values_list("analyte", "series", "level").distinct()
                for analyte, serie, level in analytes_series_levels:
                    analyte_serie_dict = {x.experiment_data.abbreviation if x.experiment_data.abbreviation else x.experiment_data.official_name: serializers.ExperimentDataValueSerializer(x).data for x in series_data 
                        if x.analyte.id == analyte and x.series == serie and x.level == level}
                    analyte_serie_dict['analyte'] = analyte
                    analyte_serie_dict['series'] = serie
                    analyte_serie_dict['level'] = level
                    analyte_serie_dict['assessment_method'] = assessment_method
                    values[str(assessment_method)+'~'+str(analyte)+'~'+serie if serie else str(assessment_method)+'~'+str(analyte)+'~conclusion'].append(analyte_serie_dict)
            else:
                analytes_series = series_data.values_list("analyte", "series").distinct()
                for analyte, serie in analytes_series:
                    analyte_serie_dict = {x.experiment_data.abbreviation if x.experiment_data.abbreviation else x.experiment_data.official_name: serializers.ExperimentDataValueSerializer(x).data for x in series_data 
                        if x.analyte.id == analyte and x.series == serie}
                    analyte_serie_dict['analyte'] = analyte
                    analyte_serie_dict['series'] = serie
                    analyte_serie_dict['assessment_method'] = assessment_method
                    values[str(assessment_method)+'~'+str(analyte)+'~'+serie if serie else str(assessment_method)+'~'+str(analyte)+'~conclusion'].append(analyte_serie_dict)
            cache.set(f'dataset-{dataset}:assessment-{assessment_method}:output', values, timeout=None) 


@task
def backup_database():
    """
    Backs up the database
    """
    ## call function here
    # BUCKET = "myproject-backups"
    # S3_DIRECTORY = socket.gethostname()
    backup_file = "backup_{time}.sql".format(time=pendulum.now(tz=settings.TIME_ZONE))
    
    backup_path = os.path.join(settings.MEDIA_ROOT, backup_file)
    # DB_S3_DIRECTORY = "{directory}/postgresql".format(directory=S3_DIRECTORY)

    # s3 = boto3.resource('s3')

    os.system("docker exec -u postgres db pg_dumpall > {path}".format(path=backup_path))
    # os.system("PGPASSWORD='postgres' pg_dump -U postgres -d valchrom -Fp > {path}".format(path=backup_path))

    # backup = open(DB_BACKUP_PATH, "rb")
    # s3.Object(BUCKET, "{directory}/{filename}".format(directory=DB_S3_DIRECTORY, filename=backup_file)).put(Body=backup)
    logger.info("Backup database")