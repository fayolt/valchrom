from ..models import Method, ValidationPlanTemplate, ExperimentalPlan, ExperimentDataset, ReportTemplate
import django_filters as filters
class ElementFilter(filters.FilterSet):
    archived = filters.BooleanFilter(field_name='archived')
    exclude = filters.CharFilter(method='exclude_status')

    def exclude_status(self, queryset, name, value):
        queryset = queryset.exclude(status=value)
        return queryset

class MethodFilter(ElementFilter):
    class Meta:
        model = Method
        fields = ('status', 'archived', )

class TemplateFilter(ElementFilter):
    class Meta:
        model = ValidationPlanTemplate
        fields = ('status', 'archived', )

class PlanFilter(ElementFilter):
    class Meta:
        model = ExperimentalPlan
        fields = ('status', 'archived', )

class ExperimentDatasetFilter(ElementFilter):
    class Meta:
        model = ExperimentDataset
        fields = ('status', 'archived', )

class ReportTemplateFilter(ElementFilter):
    class Meta:
        model = ReportTemplate
        fields = ('status', 'archived', )