from django.contrib import admin
from .models import *

admin.site.register(Guideline)
admin.site.register(ExperimentData)
admin.site.register(AssessmentMethod)
admin.site.register(Criteria)
admin.site.register(Analyte)
admin.site.register(Method)
# admin.site.register(TemplateCriteria)
admin.site.register(ValidationPlanTemplate)
admin.site.register(ExperimentalPlan)
admin.site.register(ExperimentDataset)
admin.site.register(ExperimentDataValue)
admin.site.register(ReportTemplate)
# admin.site.register(AnalyteAssessmentReport)
admin.site.register(Report)

