from django.urls import path, include
from rest_framework.routers import DefaultRouter
# from django.views.decorators.cache import cache_page
# from django.core.cache.backends.base import DEFAULT_TIMEOUT
from django.conf import settings
from . import views

# CACHE_TTL = getattr(settings, 'CACHE_TTL', DEFAULT_TIMEOUT)

class CustomRouter(DefaultRouter):
    def __init__(self, *args, **kwargs): 
        super(DefaultRouter, self).__init__(*args, **kwargs) 
        self.trailing_slash = '/?' 

router = CustomRouter()
router.register(r'plans', views.ExperimentalPlanViewSet)
router.register(r'analytes', views.AnalyteViewSet)
router.register(r'assessments', views.AssessmentMethodViewSet)
router.register(r'criteria', views.CriteriaViewSet)
router.register(r'guidelines', views.GuidelineViewSet)
router.register(r'methods', views.MethodViewSet)
router.register(r'templates', views.ValidationPlanTemplateViewSet)
router.register(r'data', views.ExperimentDataViewSet)
router.register(r'datasets', views.ExperimentDatasetViewSet)
router.register(r'data-value', views.ExperimentDataValueViewSet)
router.register(r'report-templates', views.ReportTemplateViewSet)
router.register(r'reports', views.ReportViewSet)


urlpatterns = [
    path('plans/<int:pk>/pdf/', views.PDFHandlerView.as_view()),
    path('datasets/<int:set_id>/assessments/<int:am_id>/input/', views.ExperimentDataUploaderView.as_view()),
    path('datasets/<int:set_id>/assessments/<int:am_id>/output/', views.ExperimentResultView.as_view()),
    path('', include(router.urls)),
]