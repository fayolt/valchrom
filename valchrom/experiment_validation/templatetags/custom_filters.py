from django import template
register = template.Library()

@register.filter
def get(mapping, key):
    return mapping.get(key, '')

@register.filter
def extract_value(queryset, key):
    return queryset.filter(criteria=key)[0].value