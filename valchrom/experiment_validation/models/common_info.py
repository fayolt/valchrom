from django.db import models

class CommonInfo(models.Model):
    COMPLETED = 'COMPLETED'
    PROGRESS = 'IN PROGRESS'
    LOCKED = 'LOCKED'
    STATUS = (
        (COMPLETED, 'Completed'),
        (PROGRESS, 'In Progress'),
        (LOCKED, 'Locked'),
    )
    name = models.CharField(max_length=255)
    status = models.CharField(max_length=20, choices=STATUS, default=PROGRESS)
    archived = models.BooleanField(default=False)
    archivation_date = models.DateTimeField(null=True, blank=True)
    last_saved = models.DateTimeField(auto_now=True)
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        abstract = True