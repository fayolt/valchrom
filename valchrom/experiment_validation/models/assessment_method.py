from django.db import models
from . import Guideline
from django.conf import settings

class AssessmentMethod(models.Model):

    SELECTIVITY = 1
    LOD_AND_LOQ = 2
    LINEAR_RANGE = 3
    PRECISION = 4
    ACCURACY = 5
    ROBUSTNESS = 6
    RUGGEDNESS = 7
    SYSTEM_SUITABILITY_TESTING = 8

    VALIDATION_PARAMETERS = (
        (SELECTIVITY, 'Selectivity'),
        (LOD_AND_LOQ, 'LoD and LoQ'),
        (LINEAR_RANGE, 'Linear Range'),
        (PRECISION, 'Precision'),
        (ACCURACY, 'Accuracy'),
        (ROBUSTNESS, 'Robustness'),
        (RUGGEDNESS, 'Ruggedness'),
        (SYSTEM_SUITABILITY_TESTING, 'System Suitability Testing'),
    )
    name = models.CharField(max_length=255)
    validation_parameter = models.IntegerField(choices=VALIDATION_PARAMETERS)
    description = models.TextField(default='')
    code_name = models.CharField(max_length=255, default='VP:GUI:date:name')
    input_template = models.FileField(default='input_templates/input.csv')
    guideline = models.ForeignKey(Guideline, related_name='assessment_methods', on_delete=models.CASCADE)

    class Meta:
        app_label = 'experiment_validation'
        unique_together = ('name', 'guideline')
        ordering = ['validation_parameter']
    
    def __str__(self):
        return f"{self.code_name}: {self.name}"