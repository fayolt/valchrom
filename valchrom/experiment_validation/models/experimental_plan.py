from django.db import models
from . import CommonInfo, Method, ValidationPlanTemplate

class ExperimentalPlan(CommonInfo):
    method = models.ForeignKey(Method, on_delete=models.CASCADE)
    template = models.ForeignKey(ValidationPlanTemplate, on_delete=models.CASCADE)
    owner = models.ForeignKey('users.User', related_name='plans', on_delete=models.CASCADE, default=1)

    class Meta:
        app_label = 'experiment_validation'
        unique_together = ('name', 'owner')

    def __str__(self):
        return f"{self.name}"