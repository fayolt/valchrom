from django.db import models

class Guideline(models.Model):
    name = models.CharField(max_length=255, unique=True)
    category = models.CharField(max_length=10)
    description_file = models.FileField(blank=True)

    class Meta:
        app_label = 'experiment_validation'
    
    def __str__(self):
        return f"{self.name}"