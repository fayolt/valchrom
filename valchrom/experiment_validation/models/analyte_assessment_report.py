from django.db import models
from . import Analyte, AssessmentMethod, Report

class AnalyteAssessmentReport(models.Model):
    report = models.ForeignKey(Report, on_delete=models.CASCADE)
    assessment_method = models.ForeignKey(AssessmentMethod, on_delete=models.CASCADE)
    analyte = models.ForeignKey(Analyte, on_delete=models.CASCADE)
    
    introduction = models.TextField(null=True, blank=True)
    body = models.TextField(null=True, blank=True)
    conclusion = models.TextField(null=True, blank=True)

    class Meta:
        app_label = 'experiment_validation'