from django.db import models
from . import Method

class Analyte(models.Model):
    short_name = models.CharField(max_length=256)
    official_name = models.CharField(max_length=256)
    abbreviation = models.CharField(max_length=5)
    target_concentration = models.FloatField()
    created_at = models.DateTimeField(auto_now_add=True)
    method = models.ForeignKey(Method, related_name='analytes', on_delete=models.CASCADE)

    class Meta:
        app_label = 'experiment_validation'
        unique_together = ('abbreviation', 'method')
    
    def __str__(self):
        return f"{self.method.name}: {self.abbreviation}"