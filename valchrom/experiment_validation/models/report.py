from django.db import models

from . import CommonInfo, Analyte, ExperimentDataset, ReportTemplate

class Report(CommonInfo):
    report_file = models.CharField(max_length=255, blank=True)
    experiment_dataset = models.ForeignKey(ExperimentDataset, on_delete=models.CASCADE)
    report_template = models.ForeignKey(ReportTemplate, on_delete=models.CASCADE)
    analytes = models.ManyToManyField(Analyte, through='AnalyteAssessmentReport')
    owner = models.ForeignKey('users.User', related_name='reports', on_delete=models.CASCADE, default=1)

    class Meta:
        app_label = 'experiment_validation'
        unique_together = ('name', 'owner')
    
    def __str__(self):
        return f"{self.name}"