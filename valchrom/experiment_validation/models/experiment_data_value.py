from django.db import models
from . import Analyte, ExperimentData, ExperimentDataset, AssessmentMethod
import builtins

class ExperimentDataValue(models.Model):
    value = models.CharField(max_length=255, null=True)
    series = models.CharField(max_length=255, null=True)
    level = models.CharField(max_length=255, null=True)
    parallel = models.IntegerField(null=True)
    marker = models.CharField(max_length=255, null=True)
    row = models.IntegerField(null=True) # row=0 -> data is graph; row>=1 -> row number in INPUT csv
    experiment_data = models.ForeignKey(ExperimentData, on_delete=models.CASCADE)
    experiment_dataset = models.ForeignKey(ExperimentDataset, on_delete=models.CASCADE)
    assessment_method = models.ForeignKey(AssessmentMethod, on_delete=models.CASCADE)
    analyte = models.ForeignKey(Analyte, on_delete=models.CASCADE, null=True)

    class Meta:
        app_label = 'experiment_validation'
    
    def __str__(self):
        return f"{self.experiment_dataset.name}-{self.assessment_method.code_name}-{self.experiment_data.name}: {self.value}"

    def status(self):
        try:
            getattr(builtins, self.experiment_data.data_type)(self.value)
        except:
            return 'invalid'
        else:
            return 'valid'
