from django.db import models
from . import CommonInfo, ExperimentalPlan

class ExperimentDataset(CommonInfo):
    experimental_plan = models.ForeignKey(ExperimentalPlan, related_name='datasets', on_delete=models.CASCADE)
    owner = models.ForeignKey('users.User', related_name='datasets', on_delete=models.CASCADE, default=1)

    class Meta:
        app_label = 'experiment_validation'
        unique_together = ('name', 'owner')
    
    def __str__(self):
        return f"{self.name}"