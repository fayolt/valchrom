from django.db import models
from . import AssessmentMethod

class Criteria(models.Model):
    name = models.CharField(max_length=255)
    value = models.CharField(max_length=255)
    operator = models.CharField(max_length=10)
    assessment_method = models.ForeignKey(AssessmentMethod, related_name='criteria', on_delete=models.CASCADE)

    class Meta:
        app_label = 'experiment_validation'
    
    def __str__(self):
        return f"{self.assessment_method.code_name}: {self.name}"