from django.db import models
from . import ExperimentDataset, AssessmentMethod

class ExperimentDataUploader(models.Model):
    data = models.TextField()
    experiment_dataset = models.ForeignKey(ExperimentDataset, on_delete=models.CASCADE)
    assessment_method = models.ForeignKey(AssessmentMethod, on_delete=models.CASCADE)

    class Meta:
        abstract = True