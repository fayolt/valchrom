from django.db import models
from . import CommonInfo

class Method(CommonInfo):
    concentration_unit = models.CharField(max_length=255, null=True)
    area_unit = models.CharField(max_length=255, null=True)
    retention_time_unit = models.CharField(max_length=255, null=True)
    method_description = models.TextField(blank=True)
    method_file = models.FileField(blank=True)
    method_description_file = models.FileField(blank=True)
    owner = models.ForeignKey('users.User', related_name='methods', on_delete=models.CASCADE, default=1)

    class Meta:
        app_label = 'experiment_validation'
        unique_together = ('name', 'owner')
    
    def __str__(self):
        return f"{self.name}"