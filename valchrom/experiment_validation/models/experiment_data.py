from django.db import models

class ExperimentData(models.Model):
    INPUT = 0
    OUTPUT = 1
    TYPE = (
        (INPUT, 'INPUT'),
        (OUTPUT, 'OUTPUT'),
    )
    name = models.CharField(max_length=255)
    official_name = models.CharField(max_length=255, null=True)
    abbreviation = models.CharField(max_length=255, null=True)
    data_type = models.CharField(max_length=255)
    type = models.IntegerField(choices=TYPE)

    class Meta:
        app_label = 'experiment_validation'
    
    def __str__(self):
        return f"{self.name}"