from django.db import models
from . import ValidationPlanTemplate, Criteria

class TemplateCriteria(models.Model):
    template = models.ForeignKey(ValidationPlanTemplate, on_delete=models.CASCADE)
    criteria = models.ForeignKey(Criteria, on_delete=models.CASCADE)
    value = models.CharField(max_length=255)

    class Meta:
        app_label = 'experiment_validation'