from django.db import models

from . import CommonInfo, Guideline, AssessmentMethod, Criteria

class ValidationPlanTemplate(CommonInfo):
    comment = models.TextField(blank=True)
    guideline = models.ForeignKey(Guideline, on_delete=models.CASCADE)
    assessment_methods = models.ManyToManyField(AssessmentMethod)
    criteria = models.ManyToManyField(Criteria, through='TemplateCriteria')
    owner = models.ForeignKey('users.User', related_name='templates', on_delete=models.CASCADE, default=1)

    class Meta:
        app_label = 'experiment_validation'
        unique_together = ('name', 'owner')
    
    def __str__(self):
        return f"{self.name}"