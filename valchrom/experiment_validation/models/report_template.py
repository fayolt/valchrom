from django.db import models

from . import CommonInfo

class ReportTemplate(CommonInfo):
    template_file = models.FileField(blank=True)
    
    class Meta:
        app_label = 'experiment_validation'
    
    def __str__(self):
        return f"{self.name}"