import magic
from drf_extra_fields.fields import Base64FileField

class CustomBase64File(Base64FileField):
    ALLOWED_TYPES = ('pdf', 'zip', 'rar', '7z', 'csv', 'html')
    INVALID_FILE_MESSAGE = ("Please upload a valid file.")
    INVALID_TYPE_MESSAGE = ("The type of the file couldn't be determined.")


    def get_file_extension(self, filename, decoded_file):
        mime = magic.from_buffer(decoded_file, mime=True)
        for ext in self.ALLOWED_TYPES:
            if ext in mime:
                return ext
        return 'csv'