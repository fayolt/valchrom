import math 
def round_to_n(x, n): 
    """Returns x rounded to n significant figures"""
    if x == 0.0:
        return x 
    return round(x, int(n - math.ceil(math.log10(abs(x))))) 

import hashlib
def generate_digest(string):
    string = string.encode('utf-8')
    return hashlib.md5(string).hexdigest()