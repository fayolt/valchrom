from django.template.loader import get_template
from django.http.response import HttpResponse
from rest_framework import status
from django.conf import settings
from io import BytesIO
import xhtml2pdf.pisa as pisa
import uuid
import os

MEDIA_ROOT = settings.MEDIA_ROOT
MEDIA_URL = settings.MEDIA_URL

class PDFRenderer:
    @staticmethod
    def link_callback(uri, rel):
        """
        Convert HTML URIs to absolute system paths so xhtml2pdf can access those
        resources
        """
        mUrl = settings.MEDIA_URL       # Typically /media/
        mRoot = settings.MEDIA_ROOT     # Typically /home/userX/project_static/media/

        # convert URIs to absolute system paths
        if uri.startswith(mUrl):
            path = os.path.join(mRoot, uri.replace(mUrl, ""))
        else:
            return uri  # handle absolute uri (ie: http://some.tld/foo.png)

        # make sure that file exists
        if not os.path.isfile(path):
            raise Exception(
                'media URI must start with %s' % (mUrl)
            )
        return path
    
    @staticmethod
    def render(path, params):
        template = get_template(path)
        html = template.render(params)
        response = BytesIO()
        pdf = pisa.pisaDocument(BytesIO(html.encode('utf-8')), response)
        if not pdf.err:
            return HttpResponse(response.getvalue(), content_type='application/pdf', status=200)
            # return Response(base64.b64encode(response.getvalue()), status=status.HTTP_200_OK)
        else:
            return HttpResponse("Error Rendering PDF", status=400)
    
    @staticmethod
    def render_to_file(path, params):
        template = get_template(path)
        html = template.render(params)
        file_name = str(uuid.uuid4())[:12]+'.pdf'
        with open(os.path.join(MEDIA_ROOT, file_name), 'wb') as pdf:
            pisa.pisaDocument(BytesIO(html.encode("UTF-8")), pdf, link_callback=PDFRenderer.link_callback)
        url = params['request'].scheme + '://' + params['request'].get_host() + MEDIA_URL + file_name
        return url
