from rest_framework import serializers
from ..models import Analyte, ExperimentDataset, AssessmentMethod

class ExperimentDataUploaderSerializer(serializers.Serializer):
    data = serializers.ListField(
        child=serializers.CharField()
    )
    analyte = serializers.PrimaryKeyRelatedField(queryset=Analyte.objects, allow_null=True)
    experiment_dataset = serializers.PrimaryKeyRelatedField(queryset=ExperimentDataset.objects)
    assessment_method = serializers.PrimaryKeyRelatedField(queryset=AssessmentMethod.objects)
    class Meta:
        fields = ('data', 'experiment_dataset', 'assessment_method', )
    
    def validate_experiment_dataset(self, experiment_dataset):
        if experiment_dataset.status == ExperimentDataset.LOCKED:
            raise serializers.ValidationError("Forbidden Update, Downstream Element Available")
        return experiment_dataset