from rest_framework import serializers
from . import AnalyteSerializer
from ..models import Method, Analyte
from ..utils import CustomBase64File

class MethodSerializer(serializers.ModelSerializer):
    analytes = AnalyteSerializer(many=True)
    method_file = CustomBase64File(required=False)
    method_description_file = CustomBase64File(required=False)
    owner = serializers.ReadOnlyField(source='owner.fullname')
    class Meta:
        model = Method
        fields = ('id', 'name', 'status', 'archived', 'archivation_date',
        'last_saved', 'method_description', 'method_file', 'method_description_file', 
        'concentration_unit', 'area_unit', 'retention_time_unit', 'analytes' ,'created_at', 'owner',)

    def create(self, validated_data):
        analytes = validated_data.pop('analytes', None)
        method_file = validated_data.pop('method_file', None)
        method_description_file = validated_data.pop('method_description_file', None)
        method = Method.objects.create(method_file=method_file, 
            method_description_file=method_description_file, **validated_data)
        if analytes:
            for analyte in analytes:
                Analyte.objects.create(method=method, **analyte)
        return method

    def update(self, instance, validated_data):
        analytes = validated_data.pop('analytes', None)
        instance.name = validated_data.get('name', instance.name)
        instance.status = validated_data.get('status', instance.status)
        instance.concentration_unit = validated_data.get('concentration_unit', instance.concentration_unit)
        instance.area_unit = validated_data.get('area_unit', instance.area_unit)
        instance.retention_time_unit = validated_data.get('retention_time_unit', instance.retention_time_unit)
        instance.method_description = validated_data.get('method_description', instance.method_description)
        instance.method_file = validated_data.get('method_file', instance.method_file)
        instance.method_description_file = validated_data.get('method_description_file', 
            instance.method_description_file)
        instance.save()
        kept_analytes = []
        if analytes:
            for analyte in analytes:
                if "id" in analyte.keys():
                    if Analyte.objects.filter(id=analyte.get('id')).exists:
                        a = Analyte.objects.get(id=analyte.get('id'))
                        a.short_name = analyte.get('short_name', a.short_name)
                        a.official_name = analyte.get('official_name', a.official_name)
                        a.abbreviation = analyte.get('abbreviation', a.abbreviation)
                        a.target_concentration = analyte.get('target_concentration', a.target_concentration)
                        a.method = instance
                        a.save()
                        kept_analytes.append(a.id)
                else:
                    a = Analyte.objects.create(method=instance, **analyte)
                    kept_analytes.append(a.id)
            for analyte in Analyte.objects.filter(method=instance).all():
                if analyte.id not in kept_analytes:
                    analyte.delete()
        return instance
    
    def validate_status(self, status):
        if status == Method.LOCKED:
            raise serializers.ValidationError("Forbidden Update, Downstream Element Available")
        return status
    
    def validate_analytes(self, analytes):
        abbreviations = [analyte.get('abbreviation') for analyte in analytes]
        if len(abbreviations) != len(set(abbreviations)):
            raise serializers.ValidationError("Duplicated Analytes Detected")
        return analytes
