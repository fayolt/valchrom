from rest_framework import serializers
from ..models import AssessmentMethod, Criteria

class CriteriaSerializer(serializers.ModelSerializer):
    assessment_method = serializers.SlugRelatedField(slug_field='code_name', queryset=AssessmentMethod.objects)
    class Meta:
        model = Criteria
        fields = ('id', 'name', 'value', 'operator', 'assessment_method', )