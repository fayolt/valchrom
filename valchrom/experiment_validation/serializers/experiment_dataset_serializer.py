from rest_framework import serializers
from ..models import ExperimentalPlan, ExperimentDataset

class ExperimentDatasetSerializer(serializers.ModelSerializer):
    owner = serializers.ReadOnlyField(source='owner.fullname')
    class Meta:
        model = ExperimentDataset
        fields = ('id', 'name', 'status', 'archived', 'archivation_date', 'last_saved', "experimental_plan",
        'created_at', 'owner', )
    
    def create(self, validated_data):
        experimental_plan = validated_data.pop('experimental_plan', None)
        experimental_plan.status = ExperimentalPlan.LOCKED
        experimental_plan.save()
        return ExperimentDataset.objects.create(experimental_plan=experimental_plan, **validated_data)
    
    def validate_status(self, status):
        if status == ExperimentDataset.LOCKED:
            raise serializers.ValidationError("Forbidden Update, Downstream Element Available")
        return status
    
    def to_representation(self, value):
        representation = super(ExperimentDatasetSerializer, self).to_representation(value)
        representation.pop('experimental_plan')
        representation['experimental_plan'] = representation.get('experimental_plan', 
            {'id': value.experimental_plan.id, 'name': value.experimental_plan.name})
        return representation