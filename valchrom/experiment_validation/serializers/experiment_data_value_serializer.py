from rest_framework import serializers
from ..models import Analyte, ExperimentDataValue, ExperimentData, AssessmentMethod, ExperimentDataset
from ..serializers import ExperimentDataSerializer
from ..utils import round_to_n
from .. import tasks

class ExperimentDataValueSerializer(serializers.ModelSerializer):
    
    # experiment_data = serializers.SlugRelatedField(slug_field='name', queryset=ExperimentData.objects)
    experiment_data = ExperimentDataSerializer()
    experiment_dataset = serializers.PrimaryKeyRelatedField(queryset=ExperimentDataset.objects)
    assessment_method = serializers.PrimaryKeyRelatedField(queryset=AssessmentMethod.objects)
    analyte = serializers.PrimaryKeyRelatedField(queryset=Analyte.objects, allow_null=True)
    rounded_value = serializers.SerializerMethodField()
    
    class Meta:
        model = ExperimentDataValue
        fields = ('id', 'experiment_data', 'value', 'rounded_value', 'series', 'level', 'parallel', 'marker', 
            'row', 'experiment_dataset', 'analyte', 'assessment_method', 'status', )
    
    def get_rounded_value(self, obj):
        '''Round all float values but area to three significant figures'''
        if(obj.experiment_data.name != 'area'):
            try:
                initial_value = float(obj.value)
            except:
                return obj.value
            else:
                return str(round_to_n(initial_value, 3))
        return obj.value

    def create(self, validated_data):
        experiment_data_dict = validated_data.pop('experiment_data', None)
        experiment_data = ExperimentData.objects.get(name=experiment_data_dict.get('name'))
        experiment_data_value = ExperimentDataValue.objects.create(experiment_data=experiment_data, 
           **validated_data)
        tasks.invalidate_cache(f'dataset-{experiment_data_value.experiment_dataset.id}:assessment-{experiment_data_value.assessment_method.id}:input')
        tasks.invalidate_cache(f'dataset-{experiment_data_value.experiment_dataset.id}:assessment-{experiment_data_value.assessment_method.id}:output')
        return experiment_data_value
    
    def update(self, instance, validated_data):
        validated_data.pop('experiment_data')
        instance.value = validated_data.get('value', instance.value)
        instance.series = validated_data.get('series', instance.series)
        instance.level = validated_data.get('level', instance.level)
        instance.parallel = validated_data.get('parallel', instance.parallel)
        instance.marker = validated_data.get('marker', instance.marker)
        instance.row = validated_data.get('row', instance.row)
        instance.experiment_dataset = validated_data.get('experiment_dataset', instance.experiment_dataset)
        instance.analyte = validated_data.get('analyte', instance.analyte)
        instance.assessment_method = validated_data.get('assessment_method', instance.assessment_method)
        instance.save()
        tasks.invalidate_cache(f'dataset-{instance.experiment_dataset.id}:assessment-{instance.assessment_method.id}:input')
        tasks.invalidate_cache(f'dataset-{instance.experiment_dataset.id}:assessment-{instance.assessment_method.id}:output')
        return instance