from rest_framework import serializers
from ..models import Analyte

class AnalyteSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required=False)
    method = serializers.PrimaryKeyRelatedField(read_only=True)
    class Meta:
        model = Analyte
        fields = ('id', 'short_name', 'official_name', 'abbreviation', 
            'target_concentration', 'created_at', 'method', )