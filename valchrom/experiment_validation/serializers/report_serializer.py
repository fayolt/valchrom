from rest_framework import serializers
from . import AnalyteAssessmentReportSerializer, ExperimentDataValueSerializer
from ..models import Analyte, AssessmentMethod, ExperimentData, ExperimentDataset, ExperimentDataValue, \
ReportTemplate, Report, AnalyteAssessmentReport
from ..utils import CustomBase64File, PDFRenderer
from collections import defaultdict

class ReportSerializer(serializers.ModelSerializer):
    report_file = serializers.CharField(required=False)
    experiment_dataset = serializers.PrimaryKeyRelatedField(queryset=ExperimentDataset.objects)
    report_template = serializers.PrimaryKeyRelatedField(queryset=ReportTemplate.objects)
    analytes = AnalyteAssessmentReportSerializer(source='analyteassessmentreport_set', many=True) # , 
    
    class Meta:
        model = Report
        fields = ('id', 'name', 'status', 'archived', 'archivation_date', 'last_saved', 'report_file',
            'experiment_dataset', 'report_template', 'analytes', 'created_at', )
    
    def create(self, validated_data):
        analyte_method_set = validated_data.pop('analyteassessmentreport_set', None)
        report_template = validated_data.pop('report_template', None)
        report_template.status = ReportTemplate.LOCKED
        experiment_dataset = validated_data.pop('experiment_dataset', None)
        experiment_dataset.status = ExperimentDataset.LOCKED
        report_template.save()
        experiment_dataset.save()
        report = Report.objects.create(report_template=report_template, 
            experiment_dataset=experiment_dataset,  **validated_data) # save report object
        
        # params = dict()
        # params['request'] = self.context.get("request")
        # params['data'] = dict()
        # target_keys = ['introduction', 'body', 'conclusion']
        # output = ['name', 'expected concentration', 'area', 'residual', 'slope', 'intercept', 'sd residuals', 
        #     'linear regression graph', 'residuals graph', 'lod', 'loq', 'rsquarred', 'rss', 'sd slope', 'sd intercept']
        if analyte_method_set:
            for data in analyte_method_set:
                analyte = Analyte.objects.get(id=data['analyte']['id'])
                assessment_method = data['assessment_method']
                # params['data'][analyte.short_name] = defaultdict(list)
                amr = AnalyteAssessmentReport(report=report, analyte=analyte, 
                    assessment_method=assessment_method)
                amr.introduction = data.pop('introduction', None) 
                amr.body = data.pop('body', None) 
                amr.conclusion = data.pop('conclusion', None)
                amr.save() # save analyte assessment report  record
        return report

    def to_representation(self, value):
        representation = super(ReportSerializer, self).to_representation(value)
        representation.pop('report_template')
        representation['report_template'] = representation.get('report_template', 
           {'id': value.report_template.id, 'name': value.report_template.name})
        representation.pop('experiment_dataset')
        representation['experiment_dataset'] = representation.get('experiment_dataset', 
            {'id': value.experiment_dataset.id, 'name': value.experiment_dataset.name})
        return representation