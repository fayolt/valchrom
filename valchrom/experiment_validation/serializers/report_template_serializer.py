from rest_framework import serializers
from ..models import ReportTemplate
from ..utils import CustomBase64File

class ReportTemplateSerializer(serializers.ModelSerializer):
    template_file = CustomBase64File(required=False)
    
    class Meta:
        model = ReportTemplate
        fields = ('id', 'name', 'status', 'archived', 'archivation_date', 
            'last_saved', 'template_file', 'created_at', )
    
    def validate_status(self, status):
        if status == ReportTemplate.LOCKED:
            raise serializers.ValidationError("Forbidden Update, Downstream Element Available")
        return status