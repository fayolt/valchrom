from rest_framework import serializers
from ..models import ExperimentalPlan, Method, ValidationPlanTemplate, ExperimentDataset
from ..serializers import ExperimentDatasetSerializer
from ..utils import generate_digest

from django.conf import settings

class ExperimentalPlanSerializer(serializers.ModelSerializer):
    method = serializers.PrimaryKeyRelatedField(queryset=Method.objects)
    template = serializers.PrimaryKeyRelatedField(queryset=ValidationPlanTemplate.objects) # could be SlugRelatedField
    dataset = serializers.PrimaryKeyRelatedField(read_only=True)
    owner = serializers.ReadOnlyField(source='owner.fullname')
    plan_file = serializers.SerializerMethodField()
    class Meta:
        model = ExperimentalPlan
        fields = ('id', 'name', 'status', 'archived', 'archivation_date', 'last_saved', 'method', 
        'template', 'dataset', 'plan_file', 'created_at', 'owner', )
    
    def get_plan_file(self, obj):
        '''Return experimental plan file name based on the list of assessment methods
        present in the validation plan template'''
        codes_list = obj.template.assessment_methods.order_by('code_name').values_list("code_name", flat=True)
        digest = generate_digest(''.join(codes_list))
        request = self.context.get('request')
        url = request.scheme + '://' + request.get_host() + settings.MEDIA_URL + digest + '.pdf'
        return url
        
    def create(self, validated_data):
        method = validated_data.pop('method', None)
        method.status = Method.LOCKED
        method.save()
        template = validated_data.pop('template', None)
        template.status = ValidationPlanTemplate.LOCKED
        template.save()
        return ExperimentalPlan.objects.create(method=method, template=template, **validated_data)
    
    def validate_status(self, status):
        if status == ExperimentalPlan.LOCKED:
            raise serializers.ValidationError("Forbidden Update, Downstream Element Available")
        return status

    def to_representation(self, value):
        representation = super(ExperimentalPlanSerializer, self).to_representation(value)
        representation.pop('method')
        representation['method'] = representation.get('method', {'id': value.method.id, 'name': value.method.name})
        representation.pop('template')
        representation['template'] = representation.get('template', {'id': value.template.id, 'name': value.template.name})
        return representation