from rest_framework import serializers
from ..models import Guideline, AssessmentMethod, Criteria, TemplateCriteria, ValidationPlanTemplate
from . import TemplateCriteriaSerializer

class ValidationPlanTemplateSerializer(serializers.ModelSerializer):
    guideline = serializers.SlugRelatedField(slug_field='name', queryset=Guideline.objects)
    assessment_methods = serializers.PrimaryKeyRelatedField(many=True, 
        queryset=AssessmentMethod.objects) # could be SlugRelatedField
    criteria = TemplateCriteriaSerializer(source='templatecriteria_set', many=True)
    owner = serializers.ReadOnlyField(source='owner.fullname')
    
    class Meta:
        model = ValidationPlanTemplate
        fields = ('id', 'name', 'status', 'archived', 'archivation_date',
        'last_saved', 'comment', 'guideline', 'assessment_methods', 'criteria', 'created_at', 'owner',) 

    def create(self, validated_data):
        template_criteria = validated_data.pop('templatecriteria_set', None)
        methods = validated_data.pop('assessment_methods', None)
        template = ValidationPlanTemplate.objects.create(**validated_data)
        if methods:
            for method in methods:
                template.assessment_methods.add(method)
        template.save()
        if template_criteria:
            for criterion in template_criteria:
                value = criterion.get('value')
                criterion = Criteria.objects.get(id=criterion['criteria']['id'])
                tc = TemplateCriteria(template=template, criteria=criterion)
                tc.value = value
                tc.save()
        return template
    
    def update(self, instance, validated_data):
        assessment_methods = validated_data.pop('assessment_methods', None)
        criteria = validated_data.pop('templatecriteria_set', None)
        # guideline = validated_data.pop('guideline', None)
        instance.name = validated_data.get('name', instance.name)
        instance.status = validated_data.get('status', instance.status)
        instance.comment = validated_data.get('comment', instance.comment)
        instance.save()
        am_to_be_kept = []
        cr_to_be_kept = []
        if assessment_methods:
            for assessment_method in assessment_methods:
                am_to_be_kept.append(assessment_method)
            
            # remove assessment methods if any
            for assessment_method in instance.assessment_methods.all():
                if assessment_method not in am_to_be_kept:
                    instance.assessment_methods.remove(assessment_method)
            # add assessment methods if any
            for assessment_method in am_to_be_kept:
                if assessment_method not in instance.assessment_methods.all():
                    instance.assessment_methods.add(assessment_method)
        if criteria:
            for criterion in criteria:
                if "id" in criterion['criteria'].keys() and Criteria.objects.filter(id=criterion['criteria']['id']).exists():
                    cr = Criteria.objects.get(id=criterion['criteria']['id'])
                    if TemplateCriteria.objects.filter(template=instance, criteria=cr).exists():
                        tc = TemplateCriteria.objects.get(template=instance, criteria=cr)
                        tc.value = criterion.get('value', tc.value)
                        tc.save()
                        cr_to_be_kept.append(cr)
                    else:
                        tc = TemplateCriteria(template=instance, criteria=cr, value=criterion.get('value'))
                        tc.save()
                        cr_to_be_kept.append(cr)

            for criterion in instance.criteria.all():
                if criterion not in cr_to_be_kept:
                    TemplateCriteria.objects.filter(criteria=criterion, template=instance).delete()
            
        return instance
    
    def validate_status(self, status):
        if status == ValidationPlanTemplate.LOCKED:
            raise serializers.ValidationError("Update Forbidden, Existing Downstream Element")
        return status