from rest_framework import serializers
from ..models import TemplateCriteria, Criteria


class TemplateCriteriaSerializer(serializers.HyperlinkedModelSerializer):
    id = serializers.IntegerField(source='criteria.id')
    name = serializers.CharField(source='criteria.name')
    operator = serializers.CharField(source='criteria.operator')
    value = serializers.CharField()

    class Meta:
        model = TemplateCriteria
        fields = ('id', 'name', 'operator', 'value', )
    
    def to_representation(self, value):
        representation = super(TemplateCriteriaSerializer, self).to_representation(value)
        representation['assessment_method'] = value.criteria.assessment_method.id
        return representation