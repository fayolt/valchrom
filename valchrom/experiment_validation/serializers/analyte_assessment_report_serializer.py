from rest_framework import serializers
from ..models import Analyte, AssessmentMethod, AnalyteAssessmentReport


class AnalyteAssessmentReportSerializer(serializers.HyperlinkedModelSerializer):
    id = serializers.IntegerField(source='analyte.id')
    assessment_method = serializers.PrimaryKeyRelatedField(queryset=AssessmentMethod.objects)
    class Meta:
        model = AnalyteAssessmentReport
        fields = ('id', 'assessment_method', 'introduction', 'body', 'conclusion', )