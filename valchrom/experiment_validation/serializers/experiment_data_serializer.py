from rest_framework import serializers
from ..models import ExperimentData

class ExperimentDataSerializer(serializers.ModelSerializer):
    type_display = serializers.CharField(source='get_type_display', read_only=True)
    name_display = serializers.SerializerMethodField()

    class Meta:
        model = ExperimentData
        fields = ('id', 'name', 'official_name', 'abbreviation', 'name_display',
            'data_type', 'type', 'type_display')
    
    def get_name_display(self, obj):
        '''Returns the display (abbreviation or official name) name for the data type'''
        return obj.abbreviation if obj.abbreviation else obj.official_name