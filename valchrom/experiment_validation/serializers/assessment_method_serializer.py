from rest_framework import serializers
from . import CriteriaSerializer
from ..models import AssessmentMethod, Guideline
from ..utils import CustomBase64File

class AssessmentMethodSerializer(serializers.ModelSerializer):
    input_template = CustomBase64File(required=False)
    validation_parameter_display = serializers.CharField(source='get_validation_parameter_display', read_only=True)
    guideline = serializers.PrimaryKeyRelatedField(queryset=Guideline.objects)
    criteria = CriteriaSerializer(many=True, read_only=True)
    class Meta:
        model = AssessmentMethod
        fields = ('id', 'name', 'validation_parameter', 'validation_parameter_display', 'description', 
            'guideline','criteria', 'code_name', 'input_template')