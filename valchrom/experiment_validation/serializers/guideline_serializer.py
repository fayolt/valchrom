from rest_framework import serializers
from . import AssessmentMethodSerializer
from ..models import Guideline
from ..utils import CustomBase64File


class GuidelineSerializer(serializers.ModelSerializer):
    assessment_methods = AssessmentMethodSerializer(many=True, read_only=True)
    guideline_file = CustomBase64File(required=False)
    class Meta:
        model = Guideline
        fields = ('id', 'name', 'category', 'guideline_file', 'assessment_methods')