from rest_framework import viewsets, permissions
from ..utils import CreateListMixin
from ..models import Guideline
from ..serializers import GuidelineSerializer

class GuidelineViewSet(CreateListMixin, viewsets.ModelViewSet):
    queryset = Guideline.objects.all()
    serializer_class = GuidelineSerializer
    permission_classes = (permissions.IsAuthenticated, )