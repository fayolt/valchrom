from rest_framework import viewsets, permissions
from ..utils import CreateListMixin
from ..models import AssessmentMethod
from ..serializers import AssessmentMethodSerializer

class AssessmentMethodViewSet(CreateListMixin, viewsets.ModelViewSet):
    queryset = AssessmentMethod.objects.all()
    serializer_class = AssessmentMethodSerializer
    ordering = ('validation_parameter', )
    permission_classes = (permissions.IsAuthenticated, )