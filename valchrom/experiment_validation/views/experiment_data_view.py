from rest_framework import viewsets, permissions
from ..utils import CreateListMixin
from ..models import ExperimentData
from ..serializers import ExperimentDataSerializer

class ExperimentDataViewSet(CreateListMixin, viewsets.ModelViewSet):
    queryset = ExperimentData.objects.all()
    serializer_class = ExperimentDataSerializer
    permission_classes = (permissions.IsAuthenticated, )