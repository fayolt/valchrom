from rest_framework import viewsets, permissions
from ..utils import CreateListMixin
from ..models import ExperimentDataValue
from ..serializers import ExperimentDataValueSerializer

class ExperimentDataValueViewSet(CreateListMixin, viewsets.ModelViewSet):
    queryset = ExperimentDataValue.objects.all()
    serializer_class = ExperimentDataValueSerializer
    permission_classes = (permissions.IsAuthenticated, )