from rest_framework import viewsets, filters, permissions
from ..models import Analyte
from ..serializers import AnalyteSerializer

class AnalyteViewSet(viewsets.ModelViewSet):
    queryset = Analyte.objects.all()
    serializer_class = AnalyteSerializer
    filter_backends = (filters.SearchFilter, )
    search_fields = ('^short_name', '=abbreviation')
    permission_classes = (permissions.IsAuthenticated, )