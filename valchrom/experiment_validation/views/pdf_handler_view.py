from io import BytesIO
from rest_framework import status
from rest_framework.views import APIView
from rest_framework.decorators import action
from drf_pdf.response import PDFResponse
from drf_pdf.renderer import PDFRenderer
from PyPDF2 import PdfFileWriter, PdfFileReader
from reportlab.pdfgen import canvas
from reportlab.lib.pagesizes import letter
from ..models import ExperimentalPlan, Method, ValidationPlanTemplate

class PDFHandlerView(APIView):
    renderer_classes = (PDFRenderer, )

    def get(self, request, pk, formart=None):
        experimental_plan = ExperimentalPlan.objects.get(id=pk)

        pdf = BytesIO()
        assessment_methods = BytesIO()

        gen_pdf = canvas.Canvas(assessment_methods, letter)
        y = 700
        x = 75
        gen_pdf.setFont('Helvetica-Bold', 12)
        gen_pdf.drawString(x, y, 'Assessment Methods')
        gen_pdf.setFont('Times-Roman', 12)
        for assessement_method in experimental_plan.template.assessment_methods.all():
            y -= 15
            gen_pdf.drawString(x, y, assessement_method.description)

        gen_pdf.showPage()
        gen_pdf.save()

        writer = PdfFileWriter()
        temp_reader = PdfFileReader(assessment_methods)
        for page in range(temp_reader.getNumPages()):
           writer.addPage(temp_reader.getPage(page))
        reader = PdfFileReader(open(experimental_plan.method.method_description_file.path, 'rb'))
        for page in range(reader.getNumPages()):
           writer.addPage(reader.getPage(page))

        writer.write(pdf)

        return PDFResponse(
            pdf=pdf.getvalue(),
            file_name=experimental_plan.name,
            status=status.HTTP_200_OK
        )
