from rest_framework import viewsets, filters, status, permissions
from rest_framework.decorators import action
from rest_framework.response import Response
from django.core.cache import cache
from ..permissions import IsOwner as IsOwnerPerm
from ..filters import IsOwner, ExperimentDatasetFilter
from ..models import ExperimentDataset, ExperimentDataValue, ExperimentData, AssessmentMethod, \
ValidationPlanTemplate, ExperimentalPlan
from ..serializers import ExperimentDatasetSerializer, ExperimentDataValueSerializer, \
AnalyteSerializer, ValidationPlanTemplateSerializer, MethodSerializer
from am_code import assessment_method_checks, assessment_method_calculations
from collections import defaultdict
from django.conf import settings
import django_filters.rest_framework
import pendulum
import copy

class ExperimentDatasetViewSet(viewsets.ModelViewSet):
    queryset = ExperimentDataset.objects.all()
    serializer_class = ExperimentDatasetSerializer
    permission_classes = (permissions.IsAuthenticated, IsOwnerPerm)
    filter_backends = (django_filters.rest_framework.DjangoFilterBackend, IsOwner, filters.OrderingFilter, filters.SearchFilter)
    filter_class = ExperimentDatasetFilter
    search_fields = ('^name', 'created_at', 'status', )
    ordering_fields = ('name', 'created_at', 'status', )

    def perform_create(self, serializer):
        """
        This method associates the currently authenticated user 
        to the new analytical method to be created
        """
        serializer.save(owner=self.request.user)

    @action(detail=True, methods=['post'])
    def duplicate(self, request, pk=None):
        dataset = self.get_object()
        data = ExperimentDataValue.objects.filter(experiment_dataset=dataset.id, experiment_data__type=ExperimentData.INPUT)
        new_dataset_dict = {}
        new_dataset_dict['name'] = request.data['name']
        new_dataset_dict['experimental_plan'] = dataset.experimental_plan.id
        new_dataset_dict['status'] = ExperimentDataset.COMPLETED
        serializer = ExperimentDatasetSerializer(data=new_dataset_dict)
        if serializer.is_valid():
            new_dataset = serializer.save()
            experiment_data_values = []
            for data_value in list(data):
                experiment_data_values.append(
                    ExperimentDataValue(
                        value=data_value.value, 
                        experiment_data=data_value.experiment_data, 
                        experiment_dataset=new_dataset, 
                        assessment_method=data_value.assessment_method, 
                        row=data_value.row, 
                        series=data_value.series,
                        level=data_value.level,
                        parallel=data_value.parallel,
                        analyte=data_value.analyte
                    )
                )
            ExperimentDataValue.objects.bulk_create(experiment_data_values)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @action(detail=True, methods=['patch', 'put'])
    def archive(self, request, pk=None):
        dataset = self.get_object()
        if dataset.status != ExperimentDataset.LOCKED:
            dataset.archived = True
            dataset.archivation_date = pendulum.now(tz=settings.TIME_ZONE)
            dataset.status = ExperimentDataset.LOCKED
            dataset.save()
            # unlock experimental plan when possible
            if ExperimentDataset.objects.filter(experimental_plan=dataset.experimental_plan, archived=False).count() == 0:
                dataset.experimental_plan.status = ExperimentalPlan.COMPLETED
                dataset.experimental_plan.save()
            serializer = ExperimentDatasetSerializer(dataset)
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response("Update Forbidden, Existing Downstream Element", status=status.HTTP_403_FORBIDDEN)

    @action(detail=True, methods=['get'])
    def template(self, request, pk=None):
        dataset = self.get_object()
        template = dataset.experimental_plan.template
        serializer = ValidationPlanTemplateSerializer(template)
        return Response(serializer.data, status=status.HTTP_200_OK)
    
    @action(detail=True, methods=['get'])
    def method(self, request, pk=None):
        dataset = self.get_object()
        method = dataset.experimental_plan.method
        serializer = MethodSerializer(method)
        return Response(serializer.data, status=status.HTTP_200_OK)

    @action(detail=True, methods=['get'])
    def analytes(self, request, pk=None):
        dataset = self.get_object()
        analytes = dataset.experimental_plan.method.analytes
        serializer = AnalyteSerializer(analytes, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


    @action(detail=True, methods=['get'])
    def result(self, request, pk=None):
        dataset = self.get_object()
        assessment_methods = ExperimentDataValue.objects.values_list(
            "assessment_method", flat=True
            ).filter(
                experiment_dataset=dataset.id, experiment_data__type=ExperimentData.INPUT
            ).distinct()
        values = defaultdict(list)
        for assessment_method in assessment_methods.all():
            inner_values = cache.get(f'dataset-{dataset.id}:assessment-{assessment_method}:output', None)

            if inner_values is None:
                am_code_name = AssessmentMethod.objects.get(id=assessment_method).code_name
                if am_code_name in assessment_method_calculations:
                    analytes = ExperimentDataValue.objects.values_list(
                        "analyte", flat=True
                    ).filter(
                        experiment_dataset=dataset.id, assessment_method=assessment_method, experiment_data__type=ExperimentData.INPUT
                    ).exclude(
                        analyte__isnull=True
                    ).distinct()
                    for analyte in analytes:
                        try:
                            assessment_method_calculations[am_code_name](dataset.id, assessment_method, analyte)
                        except :
                            # send error message to a webhook
                            pass
                    data = ExperimentDataValue.objects.filter(experiment_dataset=dataset.id, assessment_method=assessment_method
                    ).prefetch_related('experiment_data').order_by('row')
                    inner_values = defaultdict(list)
                    # TODO: extract this into a helper lib
                    def internals(a_dict, b_dict):
                        target_keys = ['id', 'value', 'experiment_data', 'status', 'rounded_value']
                        for key in target_keys:
                            b_dict.pop(key)
                        return {**a_dict, **b_dict}
                    rows_data = data.exclude(row__isnull=True).exclude(row=0)
                    rows = rows_data.values_list("row", flat=True).distinct()
                    for row in rows:
                        row_dict = {x.experiment_data.abbreviation if x.experiment_data.abbreviation else x.experiment_data.official_name: ExperimentDataValueSerializer(x).data for x in rows_data if x.row == row}
                        row_dict = internals(row_dict, copy.deepcopy(row_dict[next(iter(row_dict))]))

                        inner_values[row].append(row_dict)
                    
                    chromatograms_data = data.filter(row=0)
                    if chromatograms_data:
                        for chromatogram in chromatograms_data:
                            inner_values[0].append(ExperimentDataValueSerializer(chromatogram).data)

                    series_data = data.filter(row__isnull=True)
                    if data[0].assessment_method.validation_parameter in [AssessmentMethod.PRECISION, AssessmentMethod.ACCURACY] or "CCα and CCβ" in data[0].assessment_method.name:
                        analytes_series_levels = series_data.values_list("analyte", "series", "level").distinct()
                        for analyte, serie, level in analytes_series_levels:
                            analyte_serie_dict = {x.experiment_data.abbreviation if x.experiment_data.abbreviation else x.experiment_data.official_name: ExperimentDataValueSerializer(x).data for x in series_data 
                                if x.analyte.id == analyte and x.series == serie and x.level == level}
                            analyte_serie_dict['analyte'] = analyte
                            analyte_serie_dict['series'] = serie
                            analyte_serie_dict['level'] = level
                            analyte_serie_dict['assessment_method'] = assessment_method
                            inner_values[str(assessment_method)+'~'+str(analyte)+'~'+serie if serie else str(assessment_method)+'~'+str(analyte)+'~conclusion'].append(analyte_serie_dict)
                    else:
                        analytes_series = series_data.values_list("analyte", "series").distinct()
                        for analyte, serie in analytes_series:
                            analyte_serie_dict = {x.experiment_data.abbreviation if x.experiment_data.abbreviation else x.experiment_data.official_name: ExperimentDataValueSerializer(x).data for x in series_data 
                                if x.analyte.id == analyte and x.series == serie}
                            analyte_serie_dict['analyte'] = analyte
                            analyte_serie_dict['series'] = serie
                            analyte_serie_dict['assessment_method'] = assessment_method
                            inner_values[str(assessment_method)+'~'+str(analyte)+'~'+serie if serie else str(assessment_method)+'~'+str(analyte)+'~conclusion'].append(analyte_serie_dict)
                    cache.set(f'dataset-{dataset.id}:assessment-{assessment_method}:output', inner_values, timeout=None) 
                else:
                    continue
            # print(inner_values)
            # read results from database
            # else:
            #     pass
            for key in inner_values:
                values[key] = values.get(key, []) + inner_values[key]
            
        return Response(values, status=status.HTTP_200_OK)
