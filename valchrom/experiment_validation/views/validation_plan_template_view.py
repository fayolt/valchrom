from rest_framework import viewsets, filters, status, permissions
from rest_framework.decorators import action
from rest_framework.response import Response
from ..permissions import IsOwner as IsOwnerPerm
from ..filters import IsOwner, TemplateFilter
from ..models import ValidationPlanTemplate
from ..serializers import ValidationPlanTemplateSerializer
from django.conf import settings
import django_filters.rest_framework
import pendulum

TIME_ZONE = settings.TIME_ZONE

class ValidationPlanTemplateViewSet(viewsets.ModelViewSet):
    queryset = ValidationPlanTemplate.objects.all()
    serializer_class = ValidationPlanTemplateSerializer
    permission_classes = (permissions.IsAuthenticated, IsOwnerPerm)
    filter_backends = (django_filters.rest_framework.DjangoFilterBackend, IsOwner, filters.OrderingFilter, filters.SearchFilter)
    filter_class = TemplateFilter
    search_fields = ('^name', 'created_at', 'status', )
    ordering_fields = ('name', 'created_at', 'status', )

    def perform_create(self, serializer):
        """
        This method associates the currently authenticated user 
        to the new analytical method to be created
        """
        serializer.save(owner=self.request.user)

    @action(detail=True, methods=['patch', 'put'])
    def archive(self, request, pk=None):
        template = self.get_object()
        if template.status != ValidationPlanTemplate.LOCKED:
            template.archived = True
            template.archivation_date = pendulum.now(tz=TIME_ZONE)
            template.save()
            serializer = ValidationPlanTemplateSerializer(template)
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response("Update Forbidden, Existing Downstream Element", status=status.HTTP_403_FORBIDDEN)
