from rest_framework import viewsets, filters, status, permissions
from rest_framework.decorators import action
from rest_framework.response import Response
from ..filters import ReportTemplateFilter
from ..utils import CreateListMixin
from ..models import ReportTemplate
from ..serializers import ReportTemplateSerializer
from django.conf import settings
import django_filters.rest_framework
import pendulum

TIME_ZONE = settings.TIME_ZONE

class ReportTemplateViewSet(CreateListMixin, viewsets.ModelViewSet):
    queryset = ReportTemplate.objects.all()
    serializer_class = ReportTemplateSerializer
    permission_classes = (permissions.IsAuthenticated,)
    filter_backends = (django_filters.rest_framework.DjangoFilterBackend, filters.OrderingFilter, filters.SearchFilter)
    filter_class = ReportTemplateFilter
    ordering_fields = ('name', 'created_at', 'status', )
    search_fields = ('^name', 'created_at', 'status', )

    @action(detail=True, methods=['post'])
    def duplicate(self, request, pk=None):
        report_template = self.get_object()
        report_template.id = None
        report_template.status = ReportTemplate.COMPLETED
        report_template.name = request.data['name']
        report_template.save()
        serializer = ReportTemplateSerializer(report_template)
        return Response(serializer.data, status=status.HTTP_201_CREATED)
    
    @action(detail=True, methods=['patch', 'put'])
    def archive(self, request, pk=None):
        report_template = self.get_object()
        if report_template.status != ReportTemplate.LOCKED:
            report_template.archived = True
            report_template.archivation_date = pendulum.now(tz=TIME_ZONE)
            report_template.save()
            serializer = ReportTemplateSerializer(report_template)
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response("Update Forbidden, Existing Downstream Element", status=status.HTTP_403_FORBIDDEN)