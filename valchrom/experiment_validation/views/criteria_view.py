from rest_framework import viewsets, permissions
from ..utils import CreateListMixin
from ..models import Criteria
from ..serializers import CriteriaSerializer

class CriteriaViewSet(CreateListMixin, viewsets.ModelViewSet):
    queryset = Criteria.objects.all()
    serializer_class = CriteriaSerializer
    permission_classes = (permissions.IsAuthenticated, )