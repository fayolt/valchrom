from rest_framework import viewsets, filters, status, permissions
from rest_framework.decorators import action
from rest_framework.response import Response
from ..permissions import IsOwner as IsOwnerPerm
from ..filters import IsOwner, MethodFilter
from ..models import Method, Analyte
from ..serializers import MethodSerializer
from django.conf import settings
import django_filters.rest_framework
import pendulum

TIME_ZONE = settings.TIME_ZONE

class MethodViewSet(viewsets.ModelViewSet):
    queryset = Method.objects.all()
    serializer_class = MethodSerializer
    permission_classes = (permissions.IsAuthenticated, IsOwnerPerm, )
    filter_backends = (django_filters.rest_framework.DjangoFilterBackend, IsOwner, filters.OrderingFilter, filters.SearchFilter)
    filter_class = MethodFilter
    ordering_fields = ('name', 'created_at', 'status', )
    search_fields = ('^name', 'created_at', 'status', )
    
    
    def perform_create(self, serializer):
        """
        This method associates the currently authenticated user 
        to the new analytical method to be created
        """
        serializer.save(owner=self.request.user)

    @action(detail=True, methods=['post'])
    def duplicate(self, request, pk=None):
        method = self.get_object()
        analytes  = Analyte.objects.filter(method=method).all()
        method.id = None
        method.name = request.data['name']
        method.status = Method.COMPLETED
        method.archived = False
        method.archivation_date = None
        method.save()
        for analyte in analytes:
            analyte.id = None
            analyte.method_id = method.id
            analyte.save()
        serializer = MethodSerializer(method)
        return Response(serializer.data, status=status.HTTP_201_CREATED)
    
    @action(detail=True, methods=['patch', 'put'])
    def archive(self, request, pk=None):
        method = self.get_object()
        if method.status != Method.LOCKED:
            method.archived = True
            method.archivation_date = pendulum.now(tz=TIME_ZONE)
            method.save()
            serializer = MethodSerializer(method)
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response("Update Forbidden, Existing Downstream Element", status=status.HTTP_403_FORBIDDEN)
