from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response
from .. import tasks

class ExperimentResultView(APIView):
    
    def post(self, request, set_id, am_id, format=None):
        
        tasks.execute_math_module(set_id, am_id)

        return Response(status=status.HTTP_201_CREATED)
