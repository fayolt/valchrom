from rest_framework import viewsets, filters, status, permissions
from rest_framework.decorators import action
from rest_framework.response import Response
from ..permissions import IsOwner as IsOwnerPerm
from ..filters import IsOwner, PlanFilter
from ..models import ExperimentalPlan, Method, ValidationPlanTemplate
from ..serializers import ExperimentalPlanSerializer, ExperimentDatasetSerializer
from django.conf import settings
import django_filters.rest_framework
import pendulum

TIME_ZONE = settings.TIME_ZONE

class ExperimentalPlanViewSet(viewsets.ModelViewSet):
    queryset = ExperimentalPlan.objects.all()
    serializer_class = ExperimentalPlanSerializer
    permission_classes = (permissions.IsAuthenticated, IsOwnerPerm)
    filter_backends = (django_filters.rest_framework.DjangoFilterBackend, IsOwner, filters.OrderingFilter, filters.SearchFilter)
    filter_class = PlanFilter
    search_fields = ('^name', 'created_at', 'status', )
    ordering_fields = ('name', 'created_at', 'status', )

    def perform_create(self, serializer):
        """
        This method associates the currently authenticated user 
        to the new analytical method to be created
        """
        serializer.save(owner=self.request.user)

    @action(detail=True)
    def datasets(self, request, pk=None):
        plan = self.get_object()
        serializer = ExperimentDatasetSerializer(plan.datasets.all(), many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)
    
    @action(detail=True, methods=['patch', 'put'])
    def archive(self, request, pk=None):
        plan = self.get_object()
        if plan.status != ExperimentalPlan.LOCKED:
            plan.archived = True
            plan.archivation_date = pendulum.now(tz=TIME_ZONE)
            plan.save()
            # unlock template or method when possible
            if ExperimentalPlan.objects.filter(template=plan.template, archived=False).count() == 0:
                plan.template.status = ValidationPlanTemplate.COMPLETED
                plan.template.save()
            if ExperimentalPlan.objects.filter(method=plan.method, archived=False).count() == 0:
                plan.method.status = Method.COMPLETED
                plan.method.save()
            serializer = ExperimentalPlanSerializer(plan, context={"request": request})
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response("Update Forbidden, Existing Downstream Element", status=status.HTTP_403_FORBIDDEN)
        
