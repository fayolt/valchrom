from rest_framework import status, permissions
from rest_framework.views import APIView
from rest_framework.response import Response

from ..models import Analyte, ExperimentData, ExperimentDataValue, ExperimentDataset, AssessmentMethod
from ..serializers import ExperimentDataUploaderSerializer, ExperimentDataValueSerializer
from .. import tasks
from am_code import assessment_method_checks, assessment_method_calculations

from django.core.cache import cache
from django.conf import settings
from collections import defaultdict

import pendulum
import base64
import csv
import io
import os
import uuid
import imghdr
import copy

error_message = {
    1: 'Mandatory column has missing value',
    2: 'Column "Name" is missing for some row',
    4: 'Row has "excpected concentration", but has missing "area"',
    8: 'Marker(Calibration) does not have 2 rows',
    16: 'Marker(Calibration) does not have "area" or "expected concentration"',
    32: 'Marker(Sample) or Marker (Blank) does not have "area"',
    64: 'Dataset is missing correct set of "series", "levels", "parallels", "replicates"',
    128: 'Internal standard data is incorrect'
}

target_keys = ['marker', 'series', 'level', 'parallel']

class ExperimentDataUploaderView(APIView):

    def post(self, request, set_id, am_id, format=None):
        serializer = ExperimentDataUploaderSerializer(data=request.data)
        serializer.initial_data['experiment_dataset'] = set_id
        serializer.initial_data['assessment_method'] = am_id
        if  serializer.is_valid():
            experiment_dataset = ExperimentDataset.objects.get(id=serializer.data['experiment_dataset'])
            assessment_method = AssessmentMethod.objects.get(id=serializer.data['assessment_method'])
            data = serializer.data['data']
            experiment_data_values = []
            if request.data['file_type'] == 'csv' or 'excel' in request.data['file_type']:
                csv_data = base64.b64decode(data[-1])
                csv_reader = csv.DictReader(io.StringIO(csv_data.decode()), delimiter=",")
                index = 0
                for row in csv_reader:
                    target_dict = {target: row.pop(target, None) for target in target_keys}
                    if target_dict.get('series') == None:
                        target_dict['series'] = f"Uploaded on {pendulum.today(tz=settings.TIME_ZONE).to_date_string()}"
                    analyte = Analyte.objects.filter(abbreviation=row.pop('analyte'),
                            method=experiment_dataset.experimental_plan.method)[0]
                    index += 1
                    for key in row:
                        experiment_data = ExperimentData.objects.get(name=key)
                        experiment_data_values.append(
                            ExperimentDataValue(value=row[key],
                                experiment_data=experiment_data,
                                experiment_dataset=experiment_dataset,
                                assessment_method=assessment_method,
                                row=index,
                                analyte=analyte,
                                **target_dict
                            )
                        )
                check_list = assessment_method_checks[assessment_method.code_name](experiment_data_values)
                if len(check_list) == 0:
                    ExperimentDataValue.objects.filter(experiment_dataset=set_id, assessment_method=am_id,
                        experiment_data__type=ExperimentData.INPUT).delete()
                else:
                    return Response([error_message[check] for check in check_list], status=status.HTTP_400_BAD_REQUEST)
            else:
                analyte = serializer.data['analyte']
                if analyte:
                    analyte = Analyte.objects.get(id=analyte)
                index = 0
                for img_data in data:
                    index = index + 1
                    img_data = base64.b64decode(img_data)
                    file_name = str(uuid.uuid4())[:12] 
                    extension = imghdr.what(file_name, img_data)
                    complete_file_name = file_name + "." + extension
                    with open(os.path.join(settings.MEDIA_ROOT, complete_file_name), 'wb') as target:
                        target.write(img_data)
                    url = request.scheme + '://' + request.get_host() + settings.MEDIA_URL + complete_file_name
                    
                    target_dict = {target: None if target != 'parallel' else index for target in target_keys }

                    experiment_data = ExperimentData.objects.get(name='chromatogram')
                    experiment_data_values.append(
                        ExperimentDataValue(value=url, 
                            experiment_data=experiment_data, 
                            experiment_dataset=experiment_dataset, 
                            row=0, 
                            analyte = analyte,
                            assessment_method=assessment_method, 
                            **target_dict
                        )
                    )
                ExperimentDataValue.objects.filter(experiment_dataset=set_id, assessment_method=am_id, 
                    analyte=analyte, experiment_data__type=ExperimentData.INPUT).delete()
            tasks.invalidate_cache(f'dataset-{experiment_dataset.id}:assessment-{assessment_method.id}:input')
            tasks.invalidate_cache(f'dataset-{experiment_dataset.id}:assessment-{assessment_method.id}:output')
            ExperimentDataValue.objects.bulk_create(experiment_data_values) 
            return Response(status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST) 
    
    def get(self, request, set_id, am_id, format=None):
        
        def internals(a_dict, b_dict):
            target_keys = ['id', 'value', 'experiment_data', 'status', 'rounded_value']
            for key in target_keys:
                b_dict.pop(key)
            return {**a_dict, **b_dict}
        
        values = cache.get(f'dataset-{set_id}:assessment-{am_id}:input', None)
        if values is None:
            data = ExperimentDataValue.objects.filter(
                experiment_dataset=set_id, assessment_method=am_id, experiment_data__type=ExperimentData.INPUT
            ).prefetch_related('experiment_data').order_by('row')
            values = defaultdict(list)
            
            rows_data = data.exclude(row__isnull=True).exclude(row=0)
            rows = rows_data.values_list("row", flat=True).distinct()
            for row in rows:
                row_dict = {x.experiment_data.abbreviation if x.experiment_data.abbreviation else x.experiment_data.official_name: ExperimentDataValueSerializer(x).data for x in rows_data if x.row == row}
                row_dict = internals(row_dict, copy.deepcopy(row_dict[next(iter(row_dict))]))

                values[row] = row_dict
            
            chromatograms_data = data.filter(row=0)
            if chromatograms_data:
                analytes_rows = chromatograms_data.values_list("analyte", "row").distinct()
                chrom_dict = dict()
                chrom_dict["chromatogram"] = list()
                for analyte, row in analytes_rows:
                    for x in chromatograms_data:
                        if (analyte and x.analyte and analyte == x.analyte.id) or (not analyte and not x.analyte and analyte == x.analyte):
                            chrom_dict["chromatogram"].append(ExperimentDataValueSerializer(x).data)
                values[0] = chrom_dict
            
            cache.set(f'dataset-{set_id}:assessment-{am_id}:input', values, timeout=None)
        return Response(values, status=status.HTTP_200_OK)
