from rest_framework import viewsets, filters, status, permissions
from rest_framework.decorators import action
from rest_framework.response import Response
from ..permissions import IsOwner as IsOwnerPerm
from ..filters import IsOwner
from ..utils import CreateListMixin
from ..models import Report, ExperimentDataset, ReportTemplate
from ..serializers import ReportSerializer
from django.conf import settings
import django_filters.rest_framework
import pendulum

TIME_ZONE = settings.TIME_ZONE

class ReportViewSet(CreateListMixin, viewsets.ModelViewSet):
    queryset = Report.objects.all()
    serializer_class = ReportSerializer
    permission_classes = (permissions.IsAuthenticated, IsOwnerPerm)
    filter_backends = (django_filters.rest_framework.DjangoFilterBackend, IsOwner, filters.OrderingFilter, filters.SearchFilter)
    filter_fields = ('archived', 'status', )
    ordering_fields = ('name', 'created_at', 'status', )
    search_fields = ('^name', 'created_at', 'status', )

    def perform_create(self, serializer):
        """
        This method associates the currently authenticated user 
        to the new analytical method to be created
        """
        serializer.save(owner=self.request.user)


    @action(detail=True, methods=['patch', 'put'])
    def archive(self, request, pk=None):
        report = self.get_object()
        if report.status != Report.LOCKED:
            report.archived = True
            report.archivation_date = pendulum.now(tz=TIME_ZONE)
            report.save()
            # unlock report template or dataset when possible
            if Report.objects.filter(report_template=report.report_template, archived=False).count() == 0:
                report.report_template.status = ReportTemplate.COMPLETED
                report.report_template.save()
            if Report.objects.filter(experiment_dataset=report.experiment_dataset, archived=False).count() == 0:
                report.experiment_dataset.status = ExperimentDataset.COMPLETED
                report.experiment_dataset.save()
            serializer = ReportSerializer(report)
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response("Update Forbidden, Existing Downstream Element", status=status.HTTP_403_FORBIDDEN)
    
    @action(detail=True, methods=['post'])
    def duplicate(self, request, pk=None):
        report = self.get_object()
        report.id = None
        report.name = request.data['name']
        report.status = Report.COMPLETED
        report.save()
        serializer = ReportSerializer(report)
        return Response(serializer.data, status=status.HTTP_201_CREATED)