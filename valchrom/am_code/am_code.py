from functools import reduce
from datetime import datetime
import pandas as pd
import math
import operator

from experiment_validation.models import ExperimentData, ExperimentDataValue, ExperimentDataset, AssessmentMethod, Analyte, Method
from am_code import common

'''
am_code module consists of checks and calculations used in all guidelines
Each method is named after assessment_method_code that uses given function.

Naming convention: Function names come from document 'Official and with-in program names'
- all small caps
- all none letter/number characters are underscores.

1. How to use:
from am_code import assessment_method_checks, assessment_method_calculations

1.1. Data checking
assessment_method_checks[assessment_method.code_name](experiment_data_values)
where experiment_data_values is list of Experiment_data_values
For return values check function general_checks in am_code/common.py

1.2. Calculations
assessment_method_calculations[assessment_method.code_name](dataset_id, assessment_method.id, analyte_id)
Underlying functions will query Experiment_data_values from DB, calculate results and store the result to DB.

2. How to further develop new assessment methods?
2.1. Create function for data checking. Check naming in naming convention above. Add "_check" to the end of the name.
2.2. Add binding between assessment_method.code_name and function into dictionary "assessment_method_checks" in am_code/bindings.py
2.3. Create function for calculation. Check naming in naming convention above.
2.4. Add binding between assessment_method.code_name and function into dictionary "assessment_method_calculations" in am_code/bindings.py

'''
def sel_all_2019_03_28_upload_pic_check(data_value):
    return []

def sel_all_2019_03_28_m_z_check(data_value):
    headers = ['mass', 'analyte', 'precursor ion']
    chk_par = common.check_parameters_type(marker=None, headers=headers)

    return common.general_checks(data_value, [chk_par])

def sel_all_2019_03_28_rs_check(data_value):
    headers = ['rs', 'analyte']
    chk_par = common.check_parameters_type(marker=None, headers=headers)

    return common.general_checks(data_value, [chk_par])

def lod_all_2019_03_28_cal(dataset_id, assessment_method_id, analyte_id, sd_type):
    """
    Calculates LoD and LoQ for assessment methods:
    - Slope and standard deviation of blanks
    - Slope and standard deviation of y-intercept
    - Slope and standard deviation of residuals
    Results (slope, intercept, "sd blanks"/"sd y-intercept"/"sd residuals", [residuals,] lod, loq) will be saved to
    experiment_validation_experimentdatavalue

    @param dataset_id
    @param assessment_method_id
    @param analyte_id
    @param sd_type - 1 - for blanks, 2 - for y - intercept, 3 - residuals
    """

    # Query database for needed data

    in_keys = {}
    out_keys = {}
    for key in ['area', 'expected concentration', 'is area', 'is expected concentration']:
        in_keys[key] = ExperimentData.objects.get(name=key, type=ExperimentData.INPUT).id
    for key in ['slope', 'intercept', 'is area ratio', 'is expected concentration ratio', 'sd blanks', 'sd y-intercept', 'sd residuals', 'residual', 'lod', 'loq']:
        out_keys[key] = ExperimentData.objects.get(name=key, type=ExperimentData.OUTPUT).id

    all_data_values = ExperimentDataValue.objects.filter(
        experiment_dataset=dataset_id,
        assessment_method=assessment_method_id,
        analyte=analyte_id,
        experiment_data__in=[in_keys[k] for k in in_keys]
    ).order_by('row')

    def append_output(lst, value, experiment_data, series, level, parallel, marker, row):
        common.append_output_core(dataset_id, assessment_method_id, analyte_id,\
            lst, value, experiment_data, series, level, parallel, marker, row)

    def series_results(series, data_value):

        data_value = list(data_value)
        series_output = []

        # Separate Calibration data and blanks
        if sd_type == 1:
            cal_data = list(filter(lambda x: x.marker=='Calibration', data_value))
            samp_data = list(filter(lambda x: x.marker=='Blank', data_value))
            [blanks, is_area], samp_row_ids, _, samp_level, samp_parallel, _ = \
                common.extract_columns(samp_data, [in_keys['area'], in_keys['is area']])
            [cal_area, cal_expected_concentration, cal_is_area, cal_is_expected_concentration], row_ids, _, level, parallel, marker = \
                common.extract_columns(cal_data, [in_keys['area'], in_keys['expected concentration'], in_keys['is area'], in_keys['is expected concentration']])
        else:
            cal_data = data_value
            [cal_area, cal_expected_concentration, cal_is_area, cal_is_expected_concentration], row_ids, _, level, parallel, marker = \
                common.extract_columns(cal_data, [in_keys['area'], in_keys['expected concentration'], in_keys['is area'], in_keys['is expected concentration']])

        slope, intercept, _, _, _, sd_intercept, is_area_ratio_cal, is_expected_conc_ratio_cal = \
            common.calibration(cal_area, cal_expected_concentration, cal_is_area, cal_is_expected_concentration)

        # If IS is present then
        # 1. Save IS slope andintercept ratios
        # 2. Calculate and save IS area ratio for datarows
        if cal_is_area[0]:
            for value, r, l, p, m in zip(is_area_ratio_cal, row_ids, level, parallel, marker):
                append_output(series_output, value, out_keys['is area ratio'], series, l, p, m, r)
            for value, r, l, p, m in zip(is_expected_conc_ratio_cal, row_ids, level, parallel, marker):
                append_output(series_output, value, out_keys['is expected concentration ratio'], series, l, p, m, r)

            if sd_type == 1:
                is_area_ratio_blank = [b / i_a for b, i_a in zip(blanks, is_area)]
                for value, r, l, p in zip(is_area_ratio_blank, samp_row_ids, samp_level, samp_parallel):
                    append_output(series_output, value, out_keys['is area ratio'], series, l, p, 'Blank', r)

        # Calculate SD
        if sd_type == 1:
            _, sd, _ = common.mean_sd_rsd(blanks)# np.std(blanks, ddof=1)
            keys = ['slope', 'intercept', 'sd blanks', 'lod', 'loq']
        elif sd_type == 2:
            sd = sd_intercept
            keys = ['slope', 'intercept', 'sd y-intercept', 'lod', 'loq']
        else:
            residuals = common.residuals(cal_area, cal_expected_concentration, slope, intercept)
            # sd = np.std(residuals, ddof=1)
            _, sd, _ = common.mean_sd_rsd(residuals)
            keys = ['slope', 'intercept', 'sd residuals', 'lod', 'loq']
            # add residuals to
            for value, r, l, p, m in zip(residuals, row_ids, level, parallel, marker):
                append_output(series_output, value, out_keys['residual'], series, l, p, m, r)

        lod = 3.3 * sd / slope
        loq = 10 * sd / slope

        values = [slope, intercept, sd, lod, loq]
        for key, value in zip(keys, values):
            append_output(series_output, value, out_keys[key], series, None, None, None, None)

        return series_output

    output = []
    dv_by_series = common.groupby_column(all_data_values, lambda x: x.series)

    for series, data_value in dv_by_series:
        output.extend(series_results(series, data_value))

    # Add conclusion of lod, loq to results
    if output:
        lods = list(filter(lambda x: x.experiment_data_id == out_keys['lod'], output))
        loqs = list(filter(lambda x: x.experiment_data_id == out_keys['loq'], output))
        max_lod = max([x.value for x in lods])
        max_loq = max([x.value for x in loqs])
        append_output(output, max_lod, out_keys['lod'], None, None, None, None, None)
        append_output(output, max_loq, out_keys['loq'], None, None, None, None, None)

    # Delete previous results
    common.clear_output(dataset_id, assessment_method_id, analyte_id)

    # Write into DB
    ExperimentDataValue.objects.bulk_create(output)

def lod_all_2019_03_28_cal_blanks_check(data_value):
    cal_headers = ['name', 'area', 'expected concentration', 'level', 'series', 'marker', 'analyte']
    cal = common.check_parameters_type(marker='Calibration', headers=cal_headers)

    blank_headers = ['name', 'area', 'analyte']
    blanks = common.check_parameters_type(marker='Blank', headers=blank_headers)

    result = common.general_checks(data_value, [cal, blanks])
    return result

def lod_all_2019_03_28_cal_blanks(dataset_id, assessment_method_id, analyte_id):
    lod_all_2019_03_28_cal(dataset_id, assessment_method_id, analyte_id, 1)

def lod_all_2019_03_28_cal_y_check(data_value):
    cal_headers = ['name', 'area', 'expected concentration', 'level', 'series', 'marker', 'analyte']
    cal = common.check_parameters_type(marker='Calibration', headers=cal_headers)

    result = common.general_checks(data_value, [cal])
    return result

def lod_all_2019_03_28_cal_y(dataset_id, assessment_method_id, analyte_id):
    lod_all_2019_03_28_cal(dataset_id, assessment_method_id, analyte_id, 2)

def lod_all_2019_03_28_cal_res_check(data_value):
    cal_headers = ['name', 'area', 'expected concentration', 'level', 'series', 'marker', 'analyte']
    cal = common.check_parameters_type(marker='Calibration', headers=cal_headers)

    result = common.general_checks(data_value, [cal])
    return result

def lod_all_2019_03_28_cal_res(dataset_id, assessment_method_id, analyte_id):
    lod_all_2019_03_28_cal(dataset_id, assessment_method_id, analyte_id, 3)

def lod_all_2019_03_28_upload_vis(data_value):
    headers = ['experimental concentration', 'analyte']
    chk_par = common.check_parameters_type(marker=None, headers=headers)

    result = common.general_checks(data_value, [chk_par])
    return result

def lod_all_2019_03_28_upload_s_n(data_value):
    headers = ['experimental concentration', 'analyte', 'Signal-to-noise ratio (S/N)']
    chk_par = common.check_parameters_type(marker=None, headers=headers)

    return common.general_checks(data_value, [chk_par])
    # return common.general_checks_by_marker(data_value, headers, 0, 0, 0, 0)

def lod_all_2019_03_28_sd_6spike_check(data_value):
    # cal_data = [x for x in data_value if x.marker=='Calibration']
    # blank_data = [x for x in data_value if x.marker=='Blank']

    cal_headers = ['name', 'area', 'expected concentration', 'level', 'series', 'parallel', 'marker', 'analyte']
    blank_headers = ['name', 'area', 'analyte']

    cal = common.check_parameters_type(marker='Calibration', headers=cal_headers)
    blank = common.check_parameters_type(marker='Blank', headers=blank_headers)

    return common.general_checks(data_value, [cal, blank])
    # result = common.general_checks_by_marker(cal_data,cal_headers, 0, 0, 0, 0)
    # result.extend(common.general_checks_by_marker(blank_data, blank_headers, 0, 0, 0, 0))
    #
    # return result

def lod_all_2019_03_28_sd_6spike(dataset_id, assessment_method_id, analyte_id):
    """
    Calculates LoD and LoQ for assessment method:
    Calculate standard deviation from 6 (minimum) spikings (Eurachem)
    Results (slope, intercept, "experimental concentration" s0, s0prime lod, loq) will be saved to
    experiment_validation_experimentdatavalue

    @param dataset_id
    @param assessment_method_id
    @param analyte_id
    """

    # Query database for needed data

    in_keys = {}
    out_keys = {}
    for key in ['area', 'expected concentration', 'is area', 'is expected concentration']:
        in_keys[key] = ExperimentData.objects.get(name=key, type=ExperimentData.INPUT).id
    for key in ['slope', 'intercept', 'is area ratio', 'is expected concentration ratio',\
        'experimental concentration', 's0', 's0prime', 'lod', 'loq']:
        out_keys[key] = ExperimentData.objects.get(name=key, type=ExperimentData.OUTPUT).id

    all_data_values = ExperimentDataValue.objects.filter(
        experiment_dataset=dataset_id,
        assessment_method=assessment_method_id,
        analyte=analyte_id,
        experiment_data__in=[in_keys[k] for k in in_keys]
    ).order_by('row')

    n = 1
    nb = 1
    kq = 10

    def append_output(lst, value, experiment_data, series, level, parallel, marker, row):
        common.append_output_core(dataset_id, assessment_method_id, analyte_id,\
            lst, value, experiment_data, series, level, parallel, marker, row)

    def series_results(series, data_value):

        data_value = list(data_value)
        series_output = []

        # Separate Calibration data and blanks
        cal_data = list(filter(lambda x: x.marker=='Calibration', data_value))
        samp_data = list(filter(lambda x: x.marker=='Blank', data_value))

        [blanks, samp_is_area, samp_is_expected_concentration], row_ids, _, level, parallel, marker = \
            common.extract_columns(samp_data, [in_keys['area'], in_keys['is area'], in_keys['is expected concentration']])
        [cal_area, cal_expected_concentration, cal_is_area, cal_is_expected_concentration], cal_row_ids, _, cal_level, cal_parallel, _ = \
            common.extract_columns(cal_data, [in_keys['area'], in_keys['expected concentration'], in_keys['is area'], in_keys['is expected concentration']])

        slope, intercept, _, _, _, _, is_area_ratio_cal, is_expected_conc_ratio_cal = \
            common.calibration(cal_area, cal_expected_concentration, cal_is_area, cal_is_expected_concentration)

        exper_conc = common.experimental_concentration(blanks, samp_is_area, samp_is_expected_concentration, slope, intercept)

        for value, r, l, p, m in zip(exper_conc, row_ids, level, parallel, marker):
            append_output(series_output, value, out_keys['experimental concentration'], series, l, p, m, r)

        keys = ['slope', 'intercept']
        values = [slope, intercept]
        for key, value in zip(keys, values):
            append_output(series_output, value, out_keys[key], series, None, None, None, None)

        # If IS is present then
        # 1. Save IS slope andintercept ratios
        # 2. Calculate and save IS area ratio for datarows
        if cal_is_area[0]:
            for value, r, l, p in zip(is_area_ratio_cal, cal_row_ids, cal_level, cal_parallel):
                append_output(series_output, value, out_keys['is area ratio'], series, l, p, 'Calibration', r)
            for value, r, l, p in zip(is_expected_conc_ratio_cal, cal_row_ids, cal_level, cal_parallel):
                append_output(series_output, value, out_keys['is expected concentration ratio'], series, l, p, 'Calibration', r)

            is_area_ratio_blank = [b / i_a for b, i_a in zip(blanks, samp_is_area)]
            for value, r, l, p in zip(is_area_ratio_blank, row_ids, level, parallel):
                append_output(series_output, value, out_keys['is area ratio'], series, l, p, 'Blank', r)


        return series_output

    output = []
    dv_by_series = common.groupby_column(all_data_values, lambda x: x.series)

    for series, data_value in dv_by_series:
        output.extend(series_results(series, data_value))

    # Calculate s0, s0prime, lod, loq
    if output:
        exper_conc = [x.value for x in filter(lambda x: x.experiment_data_id == out_keys['experimental concentration'], output)]

        s0 = common.s0(exper_conc)
        s0prime = common.s0prime(s0, n, nb)

        lod = 3 * s0prime
        loq = kq * s0prime

        keys = ['s0', 's0prime', 'lod', 'loq']
        values = [s0, s0prime, lod, loq]
        for key, value in zip(keys, values):
            append_output(output, value, out_keys[key], None, None, None, None, None)

    # Delete previous results
    common.clear_output(dataset_id, assessment_method_id, analyte_id)

    # Write into DB
    ExperimentDataValue.objects.bulk_create(output)

def lod_all_2019_03_28_cca_ccb_homo_check(data_value):

    par_rep_match = True
    alfa_eq_beta = True

    dv_by_series = common.groupby_column(data_value, lambda x: x.series)
    for series, dv_series in dv_by_series:

        def count_match_check(dv, grp_func):
            counts = [len(common.groupby_column(dv_grp, grp_func)) for l, dv_grp in dv]
            counts_checked = [a == b for a, b in zip(counts, counts[1:])]
            if len(counts_checked) >= 1:
                return reduce(lambda a, b: a and b, counts_checked, True)
            else:
                return True

        dv_by_level = common.groupby_column(dv_series, lambda x: x.level)

        if not count_match_check(dv_by_level, lambda x: x.parallel):
            par_rep_match = False

        dv_by_parallel = common.groupby_column(dv_series, lambda x: '{};{}'.format(x.level, x.parallel))

        if not count_match_check(dv_by_parallel, lambda x: x.row):
            par_rep_match = False

    if not par_rep_match:
        result = [64]
    else:
        result = []

    # alfa == beta
    alfa_key = ExperimentData.objects.get(name='alfa', type=ExperimentData.INPUT).id
    beta_key = ExperimentData.objects.get(name='beta', type=ExperimentData.INPUT).id
    [alfa, beta], _, _, _, _, _ = \
        common.extract_columns(data_value, [alfa_key, beta_key])

    if alfa[0] != beta[0]:
        alfa_eq_beta = False

    headers = ['area', 'expected concentration', 'series', 'level', 'analyte', 'parallel']
    chk_par = common.check_parameters_type(marker=None, headers=headers, series=1, levels=3, parallels=2, datarows=0)

    result.extend(common.general_checks(data_value, [chk_par]))
    return result
    # result.extend(common.general_checks_by_marker(data_value, headers, 1, 3, 2, 0))
    #
    # return result

def lod_all_2019_03_28_cca_ccb_homo(dataset_id, assessment_method_id, analyte_id):
    """
    Calculates CCα and CCβ homoscedastic case for assessment methods
    Results ('slope', 'intercept', 'ccα', 'ccβ', 'y critical',
        'sum sq dev', 'deg freedom', 't coef', 'noncent coef',
        'sd residuals', 'linear regression graph', 'residuals graph') will be saved to
    experiment_validation_experimentdatavalue

    @param dataset_id
    @param assessment_method_id
    @analyte_id
    """

    # Query database for needed data
    analyte = Analyte.objects.get(id=analyte_id)
    method = Method.objects.get(id=analyte.method_id)

    def query_overall_param(name):
        id = ExperimentData.objects.get(name=name, type=ExperimentData.INPUT).id
        return float(ExperimentDataValue.objects.get(
            experiment_dataset=dataset_id,
            assessment_method=assessment_method_id,
            # analyte=analyte_id,
            experiment_data=id).value)

    alfa = query_overall_param('alfa')
    beta = query_overall_param('beta')
    K = query_overall_param('K')

    in_keys = {}
    out_keys = {}
    for key in ['area', 'expected concentration', 'is area', 'is expected concentration']:
        in_keys[key] = ExperimentData.objects.get(name=key, type=ExperimentData.INPUT).id
    for key in ['slope', 'intercept', 'is area ratio', 'is expected concentration ratio', 'ccα', 'ccβ', 'y critical', \
        'sum sq dev', 'deg freedom', 't coef', 'noncent coef',
        'sd residuals', 'linear regression graph', 'residuals graph']:
        out_keys[key] = ExperimentData.objects.get(name=key, type=ExperimentData.OUTPUT).id

    all_data_values = list(ExperimentDataValue.objects.filter(
        experiment_dataset=dataset_id,
        assessment_method=assessment_method_id,
        analyte=analyte_id,
        experiment_data__in=[in_keys[k] for k in in_keys]
        ).order_by('row'))

    def append_output(lst, value, experiment_data, series, level, parallel, marker, row):
        common.append_output_core(dataset_id, assessment_method_id, analyte_id,\
            lst, value, experiment_data, series, level, parallel, marker, row)

    def series_results(series, data_value):
        series_output = []

        data_value = list(data_value)

        dv_by_levels = common.groupby_column(data_value, lambda x: x.level)
        I = len(dv_by_levels)

        dv_by_parallel = common.groupby_column(dv_by_levels[0][1], lambda x: x.parallel)
        J = len(dv_by_parallel)

        dv_by_relpicate = common.groupby_column(dv_by_parallel[0][1], lambda x: x.row)
        L = len(dv_by_relpicate)

        # extract input lists
        [ys, xs], samp_row, samp_series, samp_level, samp_parallel, samp_marker = \
            common.extract_columns(data_value, [in_keys['area'], in_keys['expected concentration']])

        xs_by_level = {}
        for l, dvs in dv_by_levels:
            [level_xs], _, _, _, _, _ = common.extract_columns(dvs, [in_keys['expected concentration']])
            level_xs = [d for d in level_xs if d is not None]
            mean, _, _ = common.mean_sd_rsd(level_xs)
            xs_by_level[l] = mean

        x_mean, _, _ = common.mean_sd_rsd(xs)
        y_mean, _, _ = common.mean_sd_rsd(ys)

        ys_level_par = {}# mean over replicates for specific level and parallel
        for l, dv_l in dv_by_levels:
            dv_by_parallel = common.groupby_column(dv_l, lambda x: x.parallel)
            ys_level_par[l] = {}
            for p, dv_p in dv_by_parallel:
                [parallel_ys], _, _, _, _, _ = common.extract_columns(dv_p, [in_keys['area']])
                parallel_ys = [d for d in parallel_ys if d is not None]
                ys_level_par[l][p], _, _ = common.mean_sd_rsd(parallel_ys)

        sxx = J * sum([(xs_by_level[l] - x_mean) * (xs_by_level[l] - x_mean) for l in xs_by_level])

        b = sum([(xs_by_level[l] - x_mean) * (ys_level_par[l][p] - y_mean)
            for l in ys_level_par for p in ys_level_par[l]]) / sxx

        a = y_mean - b * x_mean

        d2 = 1.0 / (I * J -2) * sum([(ys_level_par[l][p] - a - b * xs_by_level[l]) * (ys_level_par[l][p] - a - b * xs_by_level[l]) \
            for l in ys_level_par for p in ys_level_par[l]])
        d = math.sqrt(d2)

        v = I * J - 2

        t1_a = common.find_t1_a_homo(v, alfa)

        yc = a + t1_a * d * math.sqrt(1/K + 1 / (I * J) + x_mean * x_mean / sxx)
        xc = t1_a * d / b * math.sqrt(1/K + 1 / (I * J) + x_mean * x_mean / sxx)
        ccalfa = xc

        deeta = common.deeta005[str(v)]

        if alfa == 0.05:
            xd = deeta * d / b * math.sqrt(1/K + 1 / (I * J) + x_mean * x_mean / sxx)
        elif v > 3:
            xd = 2 * xc

        ccbeeta = xd

        residuals = common.residuals(ys, xs, a, b)

        if method.concentration_unit:
            res_x_label = lin_x_label = 'c({}, {})'.format(analyte.short_name, method.concentration_unit)
        else:
            res_x_label = lin_x_label = 'c({})'.format(analyte.short_name)

        if method.area_unit:
            lin_y_label = 'Area, {}'.format(method.area_unit)
            res_y_label = 'Residuals, {}'.format(method.area_unit)
        else:
            lin_y_label = 'Area'
            res_y_label = 'Residuals'

        lin_reg_graph_url = common.draw_lmplot([xs, ys], [lin_x_label, lin_y_label], 'CCα and CCβ series' + series)
        residuals_graph_url = common.draw_lmplot([xs, residuals], [res_x_label, res_y_label], 'Residuals for series ' + series)

        append_output(series_output, xc, out_keys['ccα'], series, None, None, None, None)
        append_output(series_output, xd, out_keys['ccβ'], series, None, None, None, None)
        append_output(series_output, yc, out_keys['y critical'], series, None, None, None, None)
        append_output(series_output, sxx, out_keys['sum sq dev'], series, None, None, None, None)
        append_output(series_output, v, out_keys['deg freedom'], series, None, None, None, None)
        append_output(series_output, a, out_keys['slope'], series, None, None, None, None)
        append_output(series_output, b, out_keys['intercept'], series, None, None, None, None)
        append_output(series_output, d, out_keys['sd residuals'], series, None, None, None, None)
        append_output(series_output, t1_a, out_keys['t coef'], series, None, None, None, None)
        append_output(series_output, deeta, out_keys['noncent coef'], series, None, None, None, None)
        append_output(series_output, lin_reg_graph_url, out_keys['linear regression graph'], series, None, None, None, 0)
        append_output(series_output, residuals_graph_url, out_keys['residuals graph'], series, None, None, None, 0)

        return series_output

    dv_by_series = common.groupby_column(all_data_values, lambda x: x.series)

    output = []

    for series, data_value in dv_by_series:
        output.extend(series_results(series, data_value))

    # Delete previous results
    common.clear_output(dataset_id, assessment_method_id, analyte_id)

    # Write into DB
    ExperimentDataValue.objects.bulk_create(output)

def lod_all_2019_03_28_cca_ccb_hetero_check(data_value):
    return lod_all_2019_03_28_cca_ccb_homo_check(data_value)

def lod_all_2019_03_28_cca_ccb_hetero(dataset_id, assessment_method_id, analyte_id):
    """
    Calculates CCα and CCβ homoscedastic case for assessment methods
    Results ('slope', 'intercept', 'ccα', 'ccβ', 'y critical',
        'sum sq dev', 'deg freedom', 't coef', 'noncent coef',
        's', 'linear regression graph', 'sd residuals',
        'residuals graph', 'xd[0]', 'xd[1]', 'xd[2]', 'xd[3]', 'weight sum sq dev',
        'sd xd[0]', 'sd xd[1]', 'sd xd[2]', 'sd xd[3]',
        'sigma[1]', 'sigma[2]', 'sigma[3]',) will be saved to
    experiment_validation_experimentdatavalue

    @param dataset_id
    @param assessment_method_id
    @analyte_id
    """

    # Query database for needed data
    analyte = Analyte.objects.get(id=analyte_id)
    method = Method.objects.get(id=analyte.method_id)

    def query_overall_param(name):
        id = ExperimentData.objects.get(name=name, type=ExperimentData.INPUT).id
        return float(ExperimentDataValue.objects.get(
            experiment_dataset=dataset_id,
            assessment_method=assessment_method_id,
            # analyte=analyte_id,
            experiment_data=id).value)

    alfa = query_overall_param('alfa')
    beta = query_overall_param('beta')
    K = query_overall_param('K')

    in_keys = {}
    out_keys = {}
    for key in ['area', 'expected concentration', 'is area', 'is expected concentration']:
        in_keys[key] = ExperimentData.objects.get(name=key, type=ExperimentData.INPUT).id
    for key in ['slope', 'intercept', 'is area ratio', 'is expected concentration ratio', 'ccα', 'ccβ', 'y critical', \
        'sum sq dev', 'deg freedom', 't coef', 'noncent coef', 's', 'linear regression graph', 'sd residuals',\
        'residuals graph', 'xd[0]', 'xd[1]', 'xd[2]', 'xd[3]', 'weight sum sq dev', \
        'sd xd[0]', 'sd xd[1]', 'sd xd[2]', 'sd xd[3]',\
        'sigma[1]', 'sigma[2]', 'sigma[3]', 'sigma0', 'd'\
        ]:
        out_keys[key] = ExperimentData.objects.get(name=key, type=ExperimentData.OUTPUT).id

    all_data_values = list(ExperimentDataValue.objects.filter(
        experiment_dataset=dataset_id,
        assessment_method=assessment_method_id,
        analyte=analyte_id,
        experiment_data__in=[in_keys[k] for k in in_keys]
        ).order_by('row'))

    def append_output(lst, value, experiment_data, series, level, parallel, marker, row):
        common.append_output_core(dataset_id, assessment_method_id, analyte_id,\
            lst, value, experiment_data, series, level, parallel, marker, row)

    def series_results(series, data_value):
        series_output = []

        data_value = list(data_value)

        dv_by_levels = common.groupby_column(data_value, lambda x: x.level)
        I = len(dv_by_levels)

        dv_by_parallel = common.groupby_column(dv_by_levels[0][1], lambda x: x.parallel)
        J = len(dv_by_parallel)

        dv_by_relpicate = common.groupby_column(dv_by_parallel[0][1], lambda x: x.row)
        L = len(dv_by_relpicate)

        # extract input lists
        [ys, xs], samp_row, samp_series, samp_level, samp_parallel, samp_marker = \
            common.extract_columns(data_value, [in_keys['area'], in_keys['expected concentration']])

        xs_by_level = {}
        for l, dvs in dv_by_levels:
            [level_xs], _, _, _, _, _ = common.extract_columns(dvs, [in_keys['expected concentration']])
            level_xs = [d for d in level_xs if d is not None]
            mean, _, _ = common.mean_sd_rsd(level_xs)
            xs_by_level[l] = mean

        x_mean, _, _ = common.mean_sd_rsd(xs)
        y_mean, _, _ = common.mean_sd_rsd(ys)

        ys_level = {}# mean over replicates and parallels for specific level
        for l, dv_l in dv_by_levels:
            [level_ys], _, _, _, _, _ = common.extract_columns(dv_l, [in_keys['area']])
            level_ys = [d for d in level_ys if d is not None]
            ys_level[l], _, _ = common.mean_sd_rsd(level_ys)

        ys_level_par = {} # mean over replicates for specific level and parallel
        for l, dv_l in dv_by_levels:
            dv_by_parallel = common.groupby_column(dv_l, lambda x: x.parallel)
            ys_level_par[l] = {}
            for p, dv_p in dv_by_parallel:
                [parallel_ys], _, _, _, _, _ = common.extract_columns(dv_p, [in_keys['area']])
                parallel_ys = [d for d in parallel_ys if d is not None]
                ys_level_par[l][p], _, _ = common.mean_sd_rsd(parallel_ys)

        si = {}
        for l in ys_level:
            sub = [(ys_level_par[l][p] - ys_level[l]) * (ys_level_par[l][p] - ys_level[l]) for p in ys_level_par[l]]
            si[l] = math.sqrt(1 / (J - 1) * sum(sub))
            append_output(series_output, si[l], out_keys['s'], series, l, None, None, None)

        def estimate_c_d(q = 0, c_in = None, d_in = None):

            if q == 0:
                sigma = { l: si[l] for l in si }
            else:
                sigma = { l: c_in + d_in * xs_by_level[l] for l in xs_by_level }
                for l in sigma:
                    label = 'sigma[{}]'.format(q)
                    append_output(series_output, sigma[l], out_keys[label], series, l, None, None, None)

            w = { l: 1 / (sigma[l] * sigma[l]) for l in sigma}

            t1 = sum([w[l] for l in w])
            t1_list = [w[l] for l in w]
            t2 = sum([w[l] * xs_by_level[l] for l in w])
            t2_list = [w[l] * xs_by_level[l] for l in w]
            t3 = sum([w[l] * xs_by_level[l] * xs_by_level[l] for l in w])
            t3_list = [w[l] * xs_by_level[l] * xs_by_level[l] for l in w]
            t4 = sum([w[l] * si[l] for l in w])
            t5 = sum([w[l] * xs_by_level[l] * si[l] for l in w])

            c = (t3 * t4 - t2 * t5) / (t1 * t3 - t2 * t2)
            d = (t1 * t5 - t2 * t4) / (t1 * t3 - t2 * t2)

            if q == 2:
                return c, d
            else:
                return estimate_c_d(q + 1, c, d)

        c, d = estimate_c_d()
        sigma0 = c

        # Calculate and store sigma for iteration q = 3
        sigma = { l: c + d * xs_by_level[l] for l in xs_by_level }
        q = 3
        for l in sigma:
            label = 'sigma[{}]'.format(q)
            append_output(series_output, sigma[l], out_keys[label], series, l, None, None, None)


        weight = {l: 1 / ((c + d * xs_by_level[l]) * (c + d * xs_by_level[l])) for l in xs_by_level}

        t1 = J * sum([weight[l] for l in weight])
        t2 = J * sum([weight[l] * xs_by_level[l] for l in weight])
        t3 = J * sum([weight[l] * xs_by_level[l] * xs_by_level[l] for l in weight])
        t4 = sum([weight[l] * ys_level_par[l][p] for l in ys_level_par for p in ys_level_par[l]])
        t5 = sum([weight[l] * xs_by_level[l] * ys_level_par[l][p] for l in ys_level_par for p in ys_level_par[l]])

        a = (t3 * t4 - t2 * t5) / (t1 * t3 - t2 * t2)
        b = (t1 * t5 - t2 * t4) / (t1 * t3 - t2 * t2)

        xw = t2 / t1

        sxxw = t3 - t2 * t2 / t1

        v = I * J - 2

        t1_a = common.find_t1_a_homo(v, alfa)

        d2 = 1.0 / v * sum([weight[l] * (ys_level_par[l][p] - a - b * xs_by_level[l]) ** 2 \
            for l in ys_level_par for p in ys_level_par[l]])

        yc = a + t1_a * math.sqrt(d2) * math.sqrt(sigma0 * sigma0 / K + (1 / t1 + xw * xw / sxxw) * d2)
        xc = t1_a / b * math.sqrt(sigma0 * sigma0 / K + (1 / t1 + xw * xw / sxxw) * d2)
        ccalfa = xc

        # TODO xd
        deeta = common.deeta005[str(v)]

        def xd_func(q, sigma_xd):
            if alfa == 0.05:
                xd_in = deeta / b * math.sqrt(sigma_xd * sigma_xd / K + (1 / t1 + xw * xw / sxxw) * d2)
            elif v > 3:
                xd_in = 2 * t1_a / b * math.sqrt(sigma_xd * sigma_xd / K + (1 / t1 + xw * xw / sxxw) * d2)

            if q < 3:
                sigma_xd1 = c + d * xd_in
                addition = xd_func(q + 1, sigma_xd1)
                result = [(xd_in, sigma_xd)]
                result.extend(addition)
                return result
            else:
                return [(xd_in, sigma_xd)]

        xd = xd_func(0, sigma0)

        g_slope, g_intercept, _, _, _, _, is_area_ratio_cal, is_expected_conc_ratio_cal = common.calibration(ys, xs)
        residuals = common.residuals(ys, xs, g_slope, g_intercept)

        if method.concentration_unit:
            res_x_label = lin_x_label = 'c({}, {})'.format(analyte.short_name, method.concentration_unit)
        else:
            res_x_label = lin_x_label = 'c({})'.format(analyte.short_name)

        if method.area_unit:
            lin_y_label = 'Area, {}'.format(method.area_unit)
            res_y_label = 'Residuals, {}'.format(method.area_unit)
        else:
            lin_y_label = 'Area'
            res_y_label = 'Residuals'

        lin_reg_graph_url = common.draw_lmplot([xs, ys], [lin_x_label, lin_y_label], 'CCα and CCβ series' + series)
        residuals_graph_url = common.draw_lmplot([xs, residuals], [res_x_label, res_y_label], 'Residuals for series ' + series)

        append_output(series_output, xc, out_keys['ccα'], series, None, None, None, None)
        append_output(series_output, xd[3][0], out_keys['ccβ'], series, None, None, None, None)
        append_output(series_output, yc, out_keys['y critical'], series, None, None, None, None)

        append_output(series_output, xd[0][0], out_keys['xd[0]'], series, None, None, None, None)
        append_output(series_output, xd[1][0], out_keys['xd[1]'], series, None, None, None, None)
        append_output(series_output, xd[2][0], out_keys['xd[2]'], series, None, None, None, None)
        append_output(series_output, xd[3][0], out_keys['xd[3]'], series, None, None, None, None)

        append_output(series_output, xd[0][1], out_keys['sd xd[0]'], series, None, None, None, None)
        append_output(series_output, xd[1][1], out_keys['sd xd[1]'], series, None, None, None, None)
        append_output(series_output, xd[2][1], out_keys['sd xd[2]'], series, None, None, None, None)
        append_output(series_output, xd[3][1], out_keys['sd xd[3]'], series, None, None, None, None)

        append_output(series_output, sigma0, out_keys['sigma0'], series, None, None, None, None)
        append_output(series_output, d, out_keys['d'], series, None, None, None, None)
        append_output(series_output, sxxw, out_keys['weight sum sq dev'], series, None, None, None, None)
        append_output(series_output, v, out_keys['deg freedom'], series, None, None, None, None)
        append_output(series_output, a, out_keys['slope'], series, None, None, None, None)
        append_output(series_output, b, out_keys['intercept'], series, None, None, None, None)
        append_output(series_output, math.sqrt(d2), out_keys['sd residuals'], series, None, None, None, None)
        append_output(series_output, t1_a, out_keys['t coef'], series, None, None, None, None)
        append_output(series_output, deeta, out_keys['noncent coef'], series, None, None, None, None)
        append_output(series_output, lin_reg_graph_url, out_keys['linear regression graph'], series, None, None, None, 0)
        append_output(series_output, residuals_graph_url, out_keys['residuals graph'], series, None, None, None, 0)

        return series_output

    dv_by_series = common.groupby_column(all_data_values, lambda x: x.series)

    output = []

    for series, data_value in dv_by_series:
        output.extend(series_results(series, data_value))

    # Delete previous results
    common.clear_output(dataset_id, assessment_method_id, analyte_id)

    # Write into DB
    ExperimentDataValue.objects.bulk_create(output)

def lin_all_2019_03_28(dataset_id, assessment_method_id, analyte_id, back_calculated):
    """
    Calculates Linearity for assessment methods:
    - "Linearity 3 series / 5 concentration levels / 1 parallel per level"

    Results (slope, intercept, rsquared, residual, rss, "sd slope", "sd intercept") will be saved to
    experiment_validation_experimentdatavalue

    @param dataset_id
    @param assessment_method_id
    @analyte_id
    """

    # Query database for needed data
    analyte = Analyte.objects.get(id=analyte_id)
    method = Method.objects.get(id=analyte.method_id)

    in_keys = {}
    out_keys = {}
    for key in ['area', 'expected concentration', 'is area', 'is expected concentration']:
        in_keys[key] = ExperimentData.objects.get(name=key, type=ExperimentData.INPUT).id
    for key in ['slope', 'intercept', 'is area ratio', 'is expected concentration ratio', 'rsquared', 'rss', 'sd slope', 'sd intercept', \
        'linear regression graph', 'residuals graph', 'residual', 'bc concentration', 'bc concentration error']:
        out_keys[key] = ExperimentData.objects.get(name=key, type=ExperimentData.OUTPUT).id

    all_data_values = list(ExperimentDataValue.objects.filter(
        experiment_dataset=dataset_id,
        assessment_method=assessment_method_id,
        analyte=analyte_id,
        experiment_data__in=[in_keys[k] for k in in_keys]
        ).order_by('row'))

    def append_output(lst, value, experiment_data, series, level, parallel, marker, row):
        common.append_output_core(dataset_id, assessment_method_id, analyte_id,\
            lst, value, experiment_data, series, level, parallel, marker, row)

    def series_results(series, data_value):

        data_value = list(data_value)
        series_output = []

        # load inputs
        [area, expected_concentration, is_area, is_expected_concentration], row_ids, _, level, parallel, marker = \
            common.extract_columns(data_value, [in_keys['area'], in_keys['expected concentration'], in_keys['is area'], in_keys['is expected concentration']])

        # calculate
        slope, intercept, rsquared, rss, sd_slope, sd_intercept, is_area_ratio, is_expected_conc_ratio = \
            common.calibration(area, expected_concentration, is_area, is_expected_concentration)

        # If IS is present then
        # 1. Save IS slope and intercept ratios
        # 2. Calculate and save IS area ratio for datarows
        if is_area[0]:
            for value, r, l, p, m in zip(is_area_ratio, row_ids, level, parallel, marker):
                append_output(series_output, value, out_keys['is area ratio'], series, l, p, m, r)
            for value, r, l, p, m in zip(is_expected_conc_ratio, row_ids, level, parallel, marker):
                append_output(series_output, value, out_keys['is expected concentration ratio'], series, l, p, m, r)

        residuals = common.residuals(area, expected_concentration, slope, intercept)

        if method.concentration_unit:
            res_x_label = lin_x_label = 'c({}, {})'.format(analyte.short_name, method.concentration_unit)
        else:
            res_x_label = lin_x_label = 'c({})'.format(analyte.short_name)

        if method.area_unit:
            lin_y_label = 'Area, {}'.format(method.area_unit)
            res_y_label = 'Residuals, {}'.format(method.area_unit)
        else:
            lin_y_label = 'Area'
            res_y_label = 'Residuals'

        # formula = 'y = {0:.settings.SIGNIFICANT_DIGITSg}x + {0:.1g}'.format(slope, intercept)
        # formula = 'y = {0:.3g}x + {1:.3g}'.format(slope, intercept)

        lin_reg_graph_url = common.draw_lmplot([expected_concentration, area], [lin_x_label, lin_y_label], 'Linearity series ' + series)# , formula)
        residuals_graph_url = common.draw_lmplot([expected_concentration, residuals], [res_x_label, res_y_label], 'Residuals for series ' + series)

        if back_calculated:
            # calculate back-calculated concentration
            bc_conc = [(a - intercept) / slope for a in area]
            bc_error = [(bc - ec) / ec * 100 for bc, ec in zip(bc_conc, expected_concentration)]
            bc_packed = list(zip(row_ids, bc_conc, level, parallel, marker))
            for r, val, l, p, m in bc_packed:
                append_output(series_output, val, out_keys['bc concentration'], series, l, p, m, r)

            bce_packed = list(zip(row_ids, bc_error, level, parallel, marker))
            for r, val, l, p, m in bce_packed:
                append_output(series_output, val, out_keys['bc concentration error'], series, l, p, m, r)

        # write outputs
        residuals_packed = list(zip(row_ids, residuals, level, parallel, marker))
        for r, val, l, p, m in residuals_packed:
            append_output(series_output, val, out_keys['residual'], series, l, p, m, r)

        keys = ['slope', 'intercept', 'rsquared', 'rss', 'sd slope', 'sd intercept']
        values = [slope, intercept, rsquared, rss, sd_slope, sd_intercept]
        for key, value in zip(keys, values):
            append_output(series_output, value, out_keys[key], series, None, None, None, None)

        # NB! graphs have to be saved with row=0
        append_output(series_output, lin_reg_graph_url, out_keys['linear regression graph'], series, None, None, None, 0)
        append_output(series_output, residuals_graph_url, out_keys['residuals graph'], series, None, None, None, 0)

        return series_output

    output = []
    dv_by_series = common.groupby_column(all_data_values, lambda x: x.series)

    for series, data_value in dv_by_series:
        output.extend(series_results(series, data_value))

    # Delete previous results
    common.clear_output(dataset_id, assessment_method_id, analyte_id)

    # Write into DB
    ExperimentDataValue.objects.bulk_create(output)

def lin_all_2019_03_28_check(data_value, min_series, min_levels, min_parallels):
    # cal_data = [x for x in data_value if x.marker=='Calibration']

    cal_headers = ['name', 'area', 'expected concentration', 'level', 'series', 'parallel', 'marker', 'analyte']
    cal = common.check_parameters_type(marker='Calibration', headers=cal_headers, series=min_series, levels=min_levels, parallels=min_parallels)

    return common.general_checks(data_value, [cal])

    #result = common.general_checks_by_marker(cal_data,cal_headers, min_series, min_levels, min_parallels, 0)
    #
    # return result

def lin_all_2019_03_28_1_5_1_check(data_value):
    return lin_all_2019_03_28_check(data_value, 1, 5, 1)

def lin_all_2019_03_28_1_5_1(dataset_id, assessment_method_id, analyte_id):
    lin_all_2019_03_28(dataset_id, assessment_method_id, analyte_id, False)

def lin_all_2019_03_28_1_5_3_check(data_value):
    return lin_all_2019_03_28_check(data_value, 1, 5, 3)

def lin_all_2019_03_28_1_5_3(dataset_id, assessment_method_id, analyte_id):
    lin_all_2019_03_28(dataset_id, assessment_method_id, analyte_id, False)

def lin_all_2019_03_28_3_5_1_check(data_value):
    return lin_all_2019_03_28_check(data_value, 3, 5, 1)

def lin_all_2019_03_28_3_5_1(dataset_id, assessment_method_id, analyte_id):
    lin_all_2019_03_28(dataset_id, assessment_method_id, analyte_id, False)

def lin_all_2019_03_28_1_6_1_check(data_value):
    return lin_all_2019_03_28_check(data_value, 1, 6, 1)

def lin_all_2019_03_28_1_6_1(dataset_id, assessment_method_id, analyte_id):
    lin_all_2019_03_28(dataset_id, assessment_method_id, analyte_id, False)

def lin_all_2019_03_28_2_6_1_check(data_value):
    return lin_all_2019_03_28_check(data_value, 2, 6, 1)

def lin_all_2019_03_28_2_6_1(dataset_id, assessment_method_id, analyte_id):
    lin_all_2019_03_28(dataset_id, assessment_method_id, analyte_id, False)

def lin_all_2019_03_28_3_6_1_check(data_value):
    return lin_all_2019_03_28_check(data_value, 3, 6, 1)

def lin_all_2019_03_28_3_6_1(dataset_id, assessment_method_id, analyte_id):
    lin_all_2019_03_28(dataset_id, assessment_method_id, analyte_id, True)

def prec_all_2019_03_28_1_1_6_check(data_value):

    # cal_data = [x for x in data_value if x.marker=='Calibration']
    # samp_data = [x for x in data_value if x.marker=='Sample']

    cal_headers = ['name', 'area', 'expected concentration', 'level', 'parallel', 'marker', 'analyte']
    samp_headers = ['name', 'area', 'analyte']

    cal = common.check_parameters_type(marker='Calibration', headers=cal_headers, series=1, series_op=operator.eq)
    samp = common.check_parameters_type(marker='Sample', headers=samp_headers, series=1, levels=1, datarows=6, series_op=operator.eq, levels_op=operator.eq)

    return common.general_checks(data_value, [cal, samp])

    # result = common.general_checks_by_marker(cal_data, cal_headers, 1, 0, 0, 0, operator.eq, operator.ge, operator.ge, operator.ge)
    # result.extend(common.general_checks_by_marker(samp_data, samp_headers, 1, 1, 0, 6,
    #     operator.eq, operator.eq, operator.ge, operator.ge))
    # return result

def prec_all_2019_03_28_1_1_6(dataset_id, assessment_method_id, analyte_id):
    """
    Calculates Repeatability for assessment method
    "Repeatability | 6 injections at 100%"
    Results (slope, intercept, experimental_concentration, trueness, mean_trueness, sd_trueness, rsd_trueness") will be saved to
    experiment_validation_experimentdatavalue

    @param dataset_id
    @param assessment_method_id
    @analyte_id
    """

    # Query database for needed data
    am_name = 'Repeatability | 6 injections at 100%'

    in_keys = {}
    out_keys = {}
    for key in ['area', 'expected concentration', 'is area', 'is expected concentration']:
        in_keys[key] = ExperimentData.objects.get(name=key, type=ExperimentData.INPUT).id
    for key in ['slope', 'intercept', 'is area ratio', 'is expected concentration ratio', 'experimental concentration', 'trueness', \
        'mean trueness', 'sd trueness', 'rsd trueness',\
        'mean experimental concentration', 'sd experimental concentration', 'rsd experimental concentration',\
        'mean area', 'sd area', 'rsd area']:
        out_keys[key] = ExperimentData.objects.get(name=key, type=ExperimentData.OUTPUT).id

    all_data_values = list(ExperimentDataValue.objects.filter(
        experiment_dataset=dataset_id,
        assessment_method=assessment_method_id,
        analyte=analyte_id,
        experiment_data__in=[in_keys[k] for k in in_keys]
        ).order_by('row'))

    def append_output(lst, value, experiment_data, series, level, parallel, marker, row):
        common.append_output_core(dataset_id, assessment_method_id, analyte_id,\
            lst, value, experiment_data, series, level, parallel, marker, row)

    source = common.trueness_source(all_data_values, in_keys['expected concentration'])

    def series_results(series, data_value):
        series_output = []

        data_value = list(data_value)

        cal_data = list(filter(lambda x: x.marker=='Calibration', data_value))
        samp_data = list(filter(lambda x: x.marker=='Sample', data_value))

        [cal_area, cal_expected_concentration, cal_is_area, cal_is_expected_concentration], cal_row, _, cal_level, cal_parallel, cal_marker = \
            common.extract_columns(cal_data, [in_keys['area'], in_keys['expected concentration'], in_keys['is area'], in_keys['is expected concentration']])

        if source in ('trueness', 'experimental concentration'):
            slope, intercept, _, _, _, _, is_area_ratio_cal, is_expected_conc_ratio_cal =\
                common.calibration(cal_area, cal_expected_concentration, cal_is_area, cal_is_expected_concentration)

            keys = ['slope', 'intercept']
            values = [slope, intercept]
            for key, val in zip(keys, values):
                append_output(series_output, val, out_keys[key], series, None, None, 'Calibration', None)

        else:
            slope = None
            intercept = None
            cal_results = list()

        # If IS is present then
        # 1. Save IS slope andintercept ratios
        # 2. Calculate and save IS area ratio for datarows
        if cal_is_area[0]:
            for value, r, l, p, m in zip(is_area_ratio_cal, cal_row, cal_level, cal_parallel, cal_marker):
                append_output(series_output, value, out_keys['is area ratio'], series, l, p, m, r)
            for value, r, l, p, m in zip(is_expected_conc_ratio_cal, cal_row, cal_level, cal_parallel, cal_marker):
                append_output(series_output, value, out_keys['is expected concentration ratio'], series, l, p, m, r)

        def level_results(series, marker, level, data_by_level):

            level_output = []

            # extract input lists
            [samp_area, samp_expec_conc, samp_is_area, samp_is_expected_concentration], samp_row, _, samp_level, samp_parallel, samp_marker = \
                common.extract_columns(data_by_level, [in_keys['area'], in_keys['expected concentration'], in_keys['is area'], in_keys['is expected concentration']])

            # calculate
            experimental_concentration, trueness, mean_trueness, sd_trueness, rsd_trueness =\
                common.trueness_mean(slope, intercept, samp_area, samp_expec_conc, samp_is_area, samp_is_expected_concentration, source)

            # compose output
            exper_conc_packed = list(zip(samp_row, experimental_concentration))
            trueness_packed = list(zip(samp_row, trueness))

            for row, val in exper_conc_packed:
                append_output(level_output, val, out_keys['experimental concentration'], series, level, None, marker, row)

            for row, val in trueness_packed:
                append_output(level_output, val, out_keys['trueness'], series, level, None, marker, row)

            keys = ['mean ' + source, 'sd ' + source, 'rsd ' + source]
            values = [mean_trueness, sd_trueness, rsd_trueness]
            for key, value in zip(keys, values):
                append_output(level_output, value, out_keys[key], series, level, None, marker, None)

            if samp_is_area[0]:
                is_area_ratio_samp = [b / i_a for b, i_a in zip(samp_area, samp_is_area)]
                for value, r, l, p, m in zip(is_area_ratio_samp, samp_row, samp_level, samp_parallel, samp_marker):
                    append_output(series_output, value, out_keys['is area ratio'], series, l, p, m, r)

            return level_output

        sd_by_level = common.groupby_column(samp_data, lambda x: x.level)
        for level, data_by_level in sd_by_level:
            series_output.extend(level_results(series, 'Sample', level, data_by_level))

        return series_output

    dv_by_series = common.groupby_column(all_data_values, lambda x: x.series)
    output = []
    for series, data_value in dv_by_series:
        output.extend(series_results(series, data_value))

    # Delete previous results
    common.clear_output(dataset_id, assessment_method_id, analyte_id)

    # Write into DB
    ExperimentDataValue.objects.bulk_create(output)

def prec_all_2019_03_28_1(dataset_id, assessment_method_id, analyte_id):
    """
    Calculates Repeatability for assessment methods
    - "9 injections at 3 levels with 3 parallels per level"
    - "4 concentration levels with 5 parallels using LoQ, low, medium and high QC"
    Results (slope, intercept, experimental_concentration, trueness, mean_trueness, sd_trueness, rsd_trueness") will be saved to
    experiment_validation_experimentdatavalue

    @param dataset_id
    @param assessment_method_id
    @analyte_id
    """

    # Query database for needed data

    in_keys = {}
    out_keys = {}
    for key in ['area', 'expected concentration', 'is area', 'is expected concentration']:
        in_keys[key] = ExperimentData.objects.get(name=key, type=ExperimentData.INPUT).id
    for key in ['slope', 'intercept', 'is area ratio', 'is expected concentration ratio', 'experimental concentration', 'trueness', \
        'mean trueness', 'sd trueness', 'rsd trueness',\
        'mean experimental concentration', 'sd experimental concentration', 'rsd experimental concentration',\
        'mean area', 'sd area', 'rsd area']:
        out_keys[key] = ExperimentData.objects.get(name=key, type=ExperimentData.OUTPUT).id

    all_data_values = list(ExperimentDataValue.objects.filter(
        experiment_dataset=dataset_id,
        assessment_method=assessment_method_id,
        analyte=analyte_id,
        experiment_data__in=[in_keys[k] for k in in_keys]
        ).order_by('row'))

    def append_output(lst, value, experiment_data, series, level, parallel, marker, row):
        common.append_output_core(dataset_id, assessment_method_id, analyte_id,\
            lst, value, experiment_data, series, level, parallel, marker, row)

    source = common.trueness_source(all_data_values, in_keys['expected concentration'])

    def series_results(series, data_value):
        series_output = []

        data_value = list(data_value)

        cal_data = list(filter(lambda x: x.marker=='Calibration', data_value))
        samp_data = list(filter(lambda x: x.marker=='Sample', data_value))

        [cal_area, cal_expec_conc, cal_is_area, cal_is_expected_concentration], cal_row, _, cal_level, cal_parallel, cal_marker = \
            common.extract_columns(cal_data, [in_keys['area'], in_keys['expected concentration'], in_keys['is area'], in_keys['is expected concentration']])

        if source in ('trueness', 'experimental concentration'):
            slope, intercept, _, _, _, _, is_area_ratio_cal, is_expected_conc_ratio_cal =\
                common.calibration(cal_area, cal_expec_conc, cal_is_area, cal_is_expected_concentration)

            keys = ['slope', 'intercept']
            values = [slope, intercept]
            for key, val in zip(keys, values):
                append_output(series_output, val, out_keys[key], series, None, None, 'Calibration', None)

            # If IS is present then
            # 1. Save IS slope andintercept ratios
            # 2. Calculate and save IS area ratio for datarows
            if cal_is_area[0]:
                for value, r, l, p, m in zip(is_area_ratio_cal, cal_row, cal_level, cal_parallel, cal_marker):
                    append_output(series_output, value, out_keys['is area ratio'], series, l, p, m, r)
                for value, r, l, p, m in zip(is_expected_conc_ratio_cal, cal_row, cal_level, cal_parallel, cal_marker):
                    append_output(series_output, value, out_keys['is expected concentration ratio'], series, l, p, m, r)


        else:
            slope = None
            intercept = None
            cal_results = list()

        def level_results(series, marker, level, data_by_level):

            level_output = []

            # extract input lists
            [samp_area, samp_expec_conc, samp_is_area, samp_is_expected_concentration], samp_row, _, samp_level, samp_parallel, samp_marker = \
                common.extract_columns(data_by_level, [in_keys['area'], in_keys['expected concentration'], in_keys['is area'], in_keys['is expected concentration']])

            # calculate
            experimental_concentration, trueness, mean_trueness, sd_trueness, rsd_trueness =\
                common.trueness_mean(slope, intercept, samp_area, samp_expec_conc, samp_is_area, samp_is_expected_concentration, source)

            # compose output
            exper_conc_packed = list(zip(samp_row, experimental_concentration))
            trueness_packed = list(zip(samp_row, trueness))

            for row, val in exper_conc_packed:
                append_output(level_output, val, out_keys['experimental concentration'], series, level, None, marker, row)

            for row, val in trueness_packed:
                append_output(level_output, val, out_keys['trueness'], series, level, None, marker, row)

            keys = ['mean ' + source, 'sd ' + source, 'rsd ' + source]
            values = [mean_trueness, sd_trueness, rsd_trueness]
            for key, value in zip(keys, values):
                append_output(level_output, value, out_keys[key], series, level, None, marker, None)

            if samp_is_area[0]:
                is_area_ratio_samp = [b / i_a for b, i_a in zip(samp_area, samp_is_area)]
                for value, r, l, p, m in zip(is_area_ratio_samp, samp_row, samp_level, samp_parallel, samp_marker):
                    append_output(series_output, value, out_keys['is area ratio'], series, l, p, m, r)

            return level_output

        sd_by_level = common.groupby_column(samp_data, lambda x: x.level)
        for level, data_by_level in sd_by_level:
            series_output.extend(level_results(series, 'Sample', level, data_by_level))

        return series_output

    dv_by_series = common.groupby_column(all_data_values, lambda x: x.series)
    output = []
    for series, data_value in dv_by_series:
        output.extend(series_results(series, data_value))

    # Delete previous results
    common.clear_output(dataset_id, assessment_method_id, analyte_id)

    # Write into DB
    ExperimentDataValue.objects.bulk_create(output)

def prec_all_2019_03_28_1_3_3_check(data_value):
    # cal_data = [x for x in data_value if x.marker=='Calibration']
    # samp_data = [x for x in data_value if x.marker=='Sample']

    cal_headers = ['name', 'area', 'expected concentration', 'level', 'parallel', 'marker', 'analyte']
    samp_headers = ['name', 'area', 'level', 'analyte']

    cal = common.check_parameters_type(marker='Calibration', headers=cal_headers, series=1, series_op=operator.eq)
    samp = common.check_parameters_type(marker='Sample', headers=samp_headers, series=1, levels=3, datarows=3, series_op=operator.eq)

    return common.general_checks(data_value, [cal, samp])

    # result = common.general_checks_by_marker(cal_data, cal_headers, 1, 0, 0, 0, operator.eq, operator.ge, operator.ge, operator.ge)
    # result.extend(common.general_checks_by_marker(samp_data, samp_headers, 1, 3, 0, 3,
    #     operator.eq, operator.ge, operator.ge, operator.ge))
    # return result

def prec_all_2019_03_28_1_3_3(dataset_id, assessment_method_id, analyte_id):
    prec_all_2019_03_28_1(dataset_id, assessment_method_id, analyte_id)

def prec_all_2019_03_28_1_4_5_check(data_value):
    # cal_data = [x for x in data_value if x.marker=='Calibration']
    # samp_data = [x for x in data_value if x.marker=='Sample']
    #
    # cal_headers = ['name', 'area', 'expected concentration', 'level', 'parallel', 'marker', 'analyte']
    # samp_headers = ['name', 'area', 'level', 'analyte']
    #
    # result = common.general_checks_by_marker(cal_data, cal_headers, 1, 0, 0, 0, operator.eq, operator.ge, operator.ge, operator.ge)
    # result.extend(common.general_checks_by_marker(samp_data, samp_headers, 1, 4, 0, 5,
    #     operator.eq, operator.ge, operator.ge, operator.ge))
    # return result

    cal_headers = ['name', 'area', 'expected concentration', 'level', 'parallel', 'marker', 'analyte']
    samp_headers = ['name', 'area', 'level', 'analyte']

    cal = common.check_parameters_type(marker='Calibration', headers=cal_headers, series=1, series_op=operator.eq)
    samp = common.check_parameters_type(marker='Sample', headers=samp_headers, series=1, levels=4, datarows=5, series_op=operator.eq)

    return common.general_checks(data_value, [cal, samp])

def prec_all_2019_03_28_1_4_5(dataset_id, assessment_method_id, analyte_id):
    prec_all_2019_03_28_1(dataset_id, assessment_method_id, analyte_id)

def prec_all_2019_03_28_2_1_6(dataset_id, assessment_method_id, analyte_id):
    """
    Calculates Intermediate precision for assessment methods
    - "6 injections at target concentration"
    Results (slope, intercept, experimental_concentration, trueness, mean_trueness, sd_trueness, rsd_trueness") will be saved to
    experiment_validation_experimentdatavalue

    @param dataset_id
    @param assessment_method_id
    @analyte_id
    """

    # Query database for needed data

    in_keys = {}
    out_keys = {}
    for key in ['area', 'expected concentration', 'is area', 'is expected concentration']:
        in_keys[key] = ExperimentData.objects.get(name=key, type=ExperimentData.INPUT).id
    for key in ['slope', 'intercept', 'is area ratio', 'is expected concentration ratio', 'experimental concentration', 'trueness', \
        'mean trueness', 'sd trueness', 'rsd trueness',\
        'mean experimental concentration', 'sd experimental concentration', 'rsd experimental concentration',\
        'mean area', 'sd area', 'rsd area']:
        out_keys[key] = ExperimentData.objects.get(name=key, type=ExperimentData.OUTPUT).id

    all_data_values = list(ExperimentDataValue.objects.filter(
        experiment_dataset=dataset_id,
        assessment_method=assessment_method_id,
        analyte=analyte_id,
        experiment_data__in=[in_keys[k] for k in in_keys]
        ).order_by('row'))

    def append_output(lst, value, experiment_data, series, level, parallel, marker, row):
        common.append_output_core(dataset_id, assessment_method_id, analyte_id,\
            lst, value, experiment_data, series, level, parallel, marker, row)

    source = common.trueness_source(all_data_values, in_keys['expected concentration'])

    def series_results(series, data_value):
        series_output = []

        data_value = list(data_value)

        cal_data = list(filter(lambda x: x.marker=='Calibration', data_value))
        samp_data = list(filter(lambda x: x.marker=='Sample', data_value))

        [cal_area, cal_expec_conc, cal_is_area, cal_is_expected_concentration], cal_row, cal_series, cal_level, cal_parallel, cal_marker = \
            common.extract_columns(cal_data, [in_keys['area'], in_keys['expected concentration'], in_keys['is area'], in_keys['is expected concentration']])

        if source in ('trueness', 'experimental concentration'):
            slope, intercept, _, _, _, _, is_area_ratio_cal, is_expected_conc_ratio_cal = common.calibration(cal_area, cal_expec_conc, cal_is_area, cal_is_expected_concentration)

            keys = ['slope', 'intercept']
            values = [slope, intercept]
            for key, val in zip(keys, values):
                append_output(series_output, val, out_keys[key], series, None, None, 'Calibration', None)

            # If IS is present then
            # 1. Save IS slope andintercept ratios
            # 2. Calculate and save IS area ratio for datarows
            if cal_is_area[0]:
                for value, r, l, p, m in zip(is_area_ratio_cal, cal_row, cal_level, cal_parallel, cal_marker):
                    append_output(series_output, value, out_keys['is area ratio'], series, l, p, m, r)
                for value, r, l, p, m in zip(is_expected_conc_ratio_cal, cal_row, cal_level, cal_parallel, cal_marker):
                    append_output(series_output, value, out_keys['is expected concentration ratio'], series, l, p, m, r)

        else:
            slope = None
            intercept = None
            cal_results = list()

        # extract input lists
        [samp_area, samp_expec_conc, samp_is_area, samp_is_expected_concentration], samp_row, _, samp_level, samp_parallel, samp_marker = \
            common.extract_columns(samp_data, [in_keys['area'], in_keys['expected concentration'], in_keys['is area'], in_keys['is expected concentration']])

        # calculate
        experimental_concentration, trueness, mean_trueness, sd_trueness, rsd_trueness =\
            common.trueness_mean(slope, intercept, samp_area, samp_expec_conc, samp_is_area, samp_is_expected_concentration, source)

        # compose output
        exper_conc_packed = list(zip(experimental_concentration, samp_row, samp_level, samp_parallel, samp_marker))
        trueness_packed = list(zip(trueness, samp_row, samp_level, samp_parallel, samp_marker))

        for val, row, level, parallel, marker in exper_conc_packed:
            append_output(series_output, val, out_keys['experimental concentration'], series, level, parallel, marker, row)

        for val, row, level, parallel, marker in trueness_packed:
            append_output(series_output, val, out_keys['trueness'], series, level, parallel, marker, row)

        keys = ['mean ' + source, 'sd ' + source, 'rsd ' + source]
        values = [mean_trueness, sd_trueness, rsd_trueness]
        for key, value in zip(keys, values):
            append_output(series_output, value, out_keys[key], series, None, None, 'Sample', None)

        if samp_is_area[0]:
            is_area_ratio_samp = [b / i_a for b, i_a in zip(samp_area, samp_is_area)]
            for value, r, l, p, m in zip(is_area_ratio_samp, samp_row, samp_level, samp_parallel, samp_marker):
                append_output(series_output, value, out_keys['is area ratio'], series, l, p, m, r)

        return series_output

    dv_by_series = common.groupby_column(all_data_values, lambda x: x.series)
    output = []
    for series, data_value in dv_by_series:
        output.extend(series_results(series, data_value))

    # Calculations over level
    if source in ('trueness', 'experimental concentration'):
        level_input_dv = filter(lambda x: x.experiment_data_id == out_keys[source], output)
    else:
        samp_data = list(filter(lambda x: x.marker=='Sample', all_data_values))
        level_input_dv = filter(lambda x: x.experiment_data_id == in_keys[source], samp_data)

    # level
    def level_results(level, data_value):
        level_output = []
        # extract
        if source in ('trueness', 'experimental concentration'):
            [values], _, _, _, _, _ = common.extract_columns(data_value, [out_keys[source]])
        else:
            [values], _, _, _, _, _ = common.extract_columns(data_value, [in_keys[source]])

        mean, sd, rsd = common.mean_sd_rsd(values)

        keys = ['mean ' + source, 'sd ' + source, 'rsd ' + source]
        values = [mean, sd, rsd]
        for key, value in zip(keys, values):
            append_output(level_output, value, out_keys[key], None, level, None, 'Sample', None)

        return level_output


    dv_by_level = common.groupby_column(level_input_dv, lambda x: x.level)
    for level, data_value in dv_by_level:
        output.extend(level_results(level, data_value))

    # Delete previous results
    common.clear_output(dataset_id, assessment_method_id, analyte_id)

    # Write into DB
    ExperimentDataValue.objects.bulk_create(output)

def prec_all_2019_03_28_2_1_6_check(data_value):
    # cal_data = [x for x in data_value if x.marker=='Calibration']
    # samp_data = [x for x in data_value if x.marker=='Sample']
    #
    # cal_headers = ['name', 'area', 'expected concentration', 'series', 'level', 'parallel', 'marker', 'analyte']
    # samp_headers = ['name', 'area', 'series', 'analyte']
    #
    # result = common.general_checks_by_marker(cal_data, cal_headers, 2, 0, 0, 0, operator.ge, operator.ge, operator.ge, operator.ge)
    # result.extend(common.general_checks_by_marker(samp_data, samp_headers, 2, 1, 0, 6,
    #     operator.ge, operator.eq, operator.ge, operator.ge))
    # return result

    cal_headers = ['name', 'area', 'expected concentration', 'series', 'level', 'parallel', 'marker', 'analyte']
    samp_headers = ['name', 'area', 'series', 'analyte']

    cal = common.check_parameters_type(marker='Calibration', headers=cal_headers, series=2)
    samp = common.check_parameters_type(marker='Sample', headers=samp_headers, series=2, levels=1, datarows=6, levels_op=operator.eq)

    return common.general_checks(data_value, [cal, samp])

def prec_all_2019_03_28_2(dataset_id, assessment_method_id, analyte_id):
    """
    Calculates Intermediate precision for assessment methods
    - "9 injections at 3 levels with 3 parallels per level"
    Results (slope, intercept, experimental_concentration, trueness, mean_trueness, sd_trueness, rsd_trueness) will be saved to
    experiment_validation_experimentdatavalue

    @param dataset_id
    @param assessment_method_id
    @analyte_id
    """

    # Query database for needed data

    in_keys = {}
    out_keys = {}
    for key in ['area', 'expected concentration', 'is area', 'is expected concentration']:
        in_keys[key] = ExperimentData.objects.get(name=key, type=ExperimentData.INPUT).id
    for key in ['slope', 'intercept', 'is area ratio', 'is expected concentration ratio', 'experimental concentration', 'trueness', \
        'mean trueness', 'sd trueness', 'rsd trueness',\
        'mean experimental concentration', 'sd experimental concentration', 'rsd experimental concentration',\
        'mean area', 'sd area', 'rsd area'
        ]:
        out_keys[key] = ExperimentData.objects.get(name=key, type=ExperimentData.OUTPUT).id

    all_data_values = list(ExperimentDataValue.objects.filter(
        experiment_dataset=dataset_id,
        assessment_method=assessment_method_id,
        analyte=analyte_id,
        experiment_data__in=[in_keys[k] for k in in_keys]
        ).order_by('row'))

    def append_output(lst, value, experiment_data, series, level, parallel, marker, row):
        common.append_output_core(dataset_id, assessment_method_id, analyte_id,\
            lst, value, experiment_data, series, level, parallel, marker, row)

    source = common.trueness_source(all_data_values, in_keys['expected concentration'])

    def series_results(series, data_value):
        series_output = []

        data_value = list(data_value)

        cal_data = list(filter(lambda x: x.marker=='Calibration', data_value))
        samp_data = list(filter(lambda x: x.marker=='Sample', data_value))

        [cal_area, cal_expec_conc, cal_is_area, cal_is_expected_concentration], cal_row, cal_series, cal_level, cal_parallel, cal_marker = \
            common.extract_columns(cal_data, [in_keys['area'], in_keys['expected concentration'], in_keys['is area'], in_keys['is expected concentration']])

        if source in ('trueness', 'experimental concentration'):
            slope, intercept, _, _, _, _, is_area_ratio_cal, is_expected_conc_ratio_cal = common.calibration(cal_area, cal_expec_conc, cal_is_area, cal_is_expected_concentration)

            keys = ['slope', 'intercept']
            values = [slope, intercept]
            for key, val in zip(keys, values):
                append_output(series_output, val, out_keys[key], series, None, None, 'Calibration', None)

            # If IS is present then
            # 1. Save IS slope andintercept ratios
            # 2. Calculate and save IS area ratio for datarows
            if cal_is_area[0]:
                for value, r, l, p, m in zip(is_area_ratio_cal, cal_row, cal_level, cal_parallel, cal_marker):
                    append_output(series_output, value, out_keys['is area ratio'], series, l, p, m, r)
                for value, r, l, p, m in zip(is_expected_conc_ratio_cal, cal_row, cal_level, cal_parallel, cal_marker):
                    append_output(series_output, value, out_keys['is expected concentration ratio'], series, l, p, m, r)

        else:
            slope = None
            intercept = None
            cal_results = list()

        def level_results(level, data_by_level):
            level_output = []

            # extract input lists
            [samp_area, samp_expec_conc, samp_is_area, samp_is_expected_concentration], samp_row, _, samp_level, samp_parallel, samp_marker = \
                common.extract_columns(data_by_level, [in_keys['area'], in_keys['expected concentration'], in_keys['is area'], in_keys['is expected concentration']])

            # calculate
            experimental_concentration, trueness, mean_trueness, sd_trueness, rsd_trueness =\
                common.trueness_mean(slope, intercept, samp_area, samp_expec_conc, samp_is_area, samp_is_expected_concentration, source)

            # compose output
            exper_conc_packed = list(zip(experimental_concentration, samp_row, samp_level, samp_parallel, samp_marker))
            trueness_packed = list(zip(trueness, samp_row, samp_level, samp_parallel, samp_marker))

            for val, r, l, p, m in exper_conc_packed:
                append_output(level_output, val, out_keys['experimental concentration'], series, l, p, m, r)

            for val, r, l, p, m in trueness_packed:
                append_output(level_output, val, out_keys['trueness'], series, l, p, m, r)

            if len(samp_area) > 1:
                keys = ['mean ' + source, 'sd ' + source, 'rsd ' + source]
                values = [mean_trueness, sd_trueness, rsd_trueness]
                for key, value in zip(keys, values):
                    append_output(level_output, value, out_keys[key], series, level, None, 'Sample', None)

            return level_output

        dv_by_level = common.groupby_column(samp_data, lambda x: x.level)
        for l, dv in dv_by_level:
            series_output.extend(level_results(l, dv))

        return series_output

    dv_by_series = common.groupby_column(all_data_values, lambda x: x.series)
    output = []
    for series, data_value in dv_by_series:
        output.extend(series_results(series, data_value))

    # Calculations over level
    if source in ('trueness', 'experimental concentration'):
        level_input_dv = filter(lambda x: x.experiment_data_id == out_keys[source], output)
    else:
        samp_data = list(filter(lambda x: x.marker=='Sample', all_data_values))
        level_input_dv = filter(lambda x: x.experiment_data_id == in_keys[source], samp_data)

    # level
    def level_results(level, data_value):
        level_output = []
        # extract

        # TODO do we need to filter out "Calibration"
        if source in ('trueness', 'experimental concentration'):
            [values], _, _, _, _, _ = common.extract_columns(data_value, [out_keys[source]])
        else:
            [values], _, _, _, _, _ = common.extract_columns(data_value, [in_keys[source]])

        mean, sd, rsd = common.mean_sd_rsd(values)

        keys = ['mean ' + source, 'sd ' + source, 'rsd ' + source]
        values = [mean, sd, rsd]
        for key, value in zip(keys, values):
            append_output(level_output, value, out_keys[key], None, level, None, 'Sample', None)

        return level_output


    dv_by_level = common.groupby_column(level_input_dv, lambda x: x.level)
    for level, data_value in dv_by_level:
        output.extend(level_results(level, data_value))

    # Delete previous results
    common.clear_output(dataset_id, assessment_method_id, analyte_id)

    # Write into DB
    ExperimentDataValue.objects.bulk_create(output)

def prec_all_2019_03_28_2_3_3_check(data_value):
    # headers = ['name', 'tr', 'area', 'series', 'level', 'analyte']
    # return common.general_checks_by_marker(data_value, headers, 2, 3, 3, 1)

    # cal_data = [x for x in data_value if x.marker=='Calibration']
    # samp_data = [x for x in data_value if x.marker=='Sample']

    cal_headers = ['name', 'area', 'expected concentration', 'series', 'level', 'parallel', 'marker', 'analyte']
    samp_headers = ['name', 'area', 'series', 'level', 'parallel', 'analyte']

    cal = common.check_parameters_type(marker='Calibration', headers=cal_headers, series=2)
    samp = common.check_parameters_type(marker='Sample', headers=samp_headers, series=2, levels=3, datarows=3)

    return common.general_checks(data_value, [cal, samp])

    # result = common.general_checks_by_marker(cal_data, cal_headers, 2, 0, 0, 0, operator.ge, operator.ge, operator.ge, operator.ge)
    # result.extend(common.general_checks_by_marker(samp_data, samp_headers, 2, 3, 0, 3,
    #     operator.ge, operator.ge, operator.ge, operator.ge))
    # return result

def prec_all_2019_03_28_2_3_3(dataset_id, assessment_method_id, analyte_id):
    prec_all_2019_03_28_2(dataset_id, assessment_method_id, analyte_id)

def prec_all_2019_03_28_3_4_1_check(data_value):
    # headers = ['name', 'area', 'series', 'level', 'analyte']
    # return common.general_checks_by_marker(data_value, headers, 3, 4, 1, 1)

    # cal_data = [x for x in data_value if x.marker=='Calibration']
    # samp_data = [x for x in data_value if x.marker=='Sample']

    cal_headers = ['name', 'area', 'expected concentration', 'series', 'level', 'parallel', 'marker', 'analyte']
    samp_headers = ['name', 'area', 'series', 'level', 'analyte']

    # result = common.general_checks_by_marker(cal_data, cal_headers, 3, 0, 0, 0, operator.ge, operator.ge, operator.ge, operator.ge)
    # result.extend(common.general_checks_by_marker(samp_data, samp_headers, 3, 4, 0, 1,
    #     operator.ge, operator.ge, operator.ge, operator.ge))
    # return result

    cal = common.check_parameters_type(marker='Calibration', headers=cal_headers, series=3)
    samp = common.check_parameters_type(marker='Sample', headers=samp_headers, series=3, levels=4, datarows=1)

    return common.general_checks(data_value, [cal, samp])

def prec_all_2019_03_28_3_4_1(dataset_id, assessment_method_id, analyte_id):
    prec_all_2019_03_28_2(dataset_id, assessment_method_id, analyte_id)

def acc_all_2019_03_28_1_3_3_check(data_value):
    # cal_data = [x for x in data_value if x.marker=='Calibration']
    # samp_data = [x for x in data_value if x.marker=='Sample']

    cal_headers = ['name', 'area', 'expected concentration', 'series', 'level', 'parallel', 'marker', 'analyte']
    samp_headers = ['name', 'area', 'expected concentration', 'series', 'level', 'parallel', 'marker', 'analyte']

    # result = common.general_checks_by_marker(cal_data, cal_headers, 1, 0, 0, 0, operator.ge, operator.ge, operator.ge, operator.ge)
    # result.extend(common.general_checks_by_marker(samp_data, samp_headers, 1, 2, 3, 0,
    #     operator.ge, operator.ge, operator.ge, operator.ge))
    # return result
    #
    cal = common.check_parameters_type(marker='Calibration', headers=cal_headers, series=1)
    samp = common.check_parameters_type(marker='Sample', headers=samp_headers, series=1, levels=2, parallels=3)

    return common.general_checks(data_value, [cal, samp])

def acc_all_2019_03_28_1_3_3(dataset_id, assessment_method_id, analyte_id):
    prec_all_2019_03_28_2(dataset_id, assessment_method_id, analyte_id)

def acc_all_2019_03_28_ref_samp_check(data_value):
    # cal_data = [x for x in data_value if x.marker=='Calibration']
    # samp_data = [x for x in data_value if x.marker=='Sample']

    cal_headers = ['name', 'area', 'expected concentration', 'series', 'level', 'parallel', 'marker', 'analyte']
    samp_headers = ['name', 'area', 'expected concentration', 'series', 'level', 'parallel', 'marker', 'analyte']

    # result = common.general_checks_by_marker(cal_data, cal_headers, 1, 0, 0, 0, operator.ge, operator.ge, operator.ge, operator.ge)
    # result.extend(common.general_checks_by_marker(samp_data, samp_headers, 1, 1, 10, 0,
    #     operator.ge, operator.ge, operator.ge, operator.ge))
    # return result

    cal = common.check_parameters_type(marker='Calibration', headers=cal_headers, series=1)
    samp = common.check_parameters_type(marker='Sample', headers=samp_headers, series=1, levels=1, parallels=10)

    return common.general_checks(data_value, [cal, samp])

def acc_all_2019_03_28_ref_samp(dataset_id, assessment_method_id, analyte_id):
    prec_all_2019_03_28_2(dataset_id, assessment_method_id, analyte_id)

def acc_all_2019_03_28_ref_method_check(data_value):
    # cal_data = [x for x in data_value if x.marker=='Calibration']
    # samp_data = [x for x in data_value if x.marker=='Sample']

    cal_headers = ['name', 'area', 'expected concentration', 'series', 'level', 'parallel', 'marker', 'analyte']
    samp_headers = ['name', 'area', 'expected concentration', 'series', 'level', 'parallel', 'marker', 'analyte']

    # result = common.general_checks_by_marker(cal_data, cal_headers, 1, 0, 0, 0, operator.ge, operator.ge, operator.ge, operator.ge)
    # result.extend(common.general_checks_by_marker(samp_data, samp_headers, 1, 1, 10, 0,
    #     operator.ge, operator.ge, operator.ge, operator.ge))
    # return result

    cal = common.check_parameters_type(marker='Calibration', headers=cal_headers, series=1)
    samp = common.check_parameters_type(marker='Sample', headers=samp_headers, series=1, levels=1, parallels=10)

    return common.general_checks(data_value, [cal, samp])

def acc_all_2019_03_28_ref_method(dataset_id, assessment_method_id, analyte_id):
    prec_all_2019_03_28_2(dataset_id, assessment_method_id, analyte_id)

def acc_all_2019_03_28_1_1_10_check(data_value):
    # cal_data = [x for x in data_value if x.marker=='Calibration']
    # samp_data = [x for x in data_value if x.marker=='Sample']

    cal_headers = ['name', 'area', 'expected concentration', 'series', 'level', 'parallel', 'marker', 'analyte']
    samp_headers = ['name', 'area', 'expected concentration', 'series', 'level', 'parallel', 'marker', 'analyte']

    # result = common.general_checks_by_marker(cal_data, cal_headers, 1, 0, 0, 0, operator.ge, operator.ge, operator.ge, operator.ge)
    # result.extend(common.general_checks_by_marker(samp_data, samp_headers, 1, 1, 10, 0,
    #     operator.ge, operator.ge, operator.ge, operator.ge))
    # return result

    cal = common.check_parameters_type(marker='Calibration', headers=cal_headers, series=1)
    samp = common.check_parameters_type(marker='Sample', headers=samp_headers, series=1, levels=1, parallels=10)

    return common.general_checks(data_value, [cal, samp])


def acc_all_2019_03_28_1_1_10(dataset_id, assessment_method_id, analyte_id):
    prec_all_2019_03_28_2(dataset_id, assessment_method_id, analyte_id)

def acc_all_2019_03_28_1_4_5_check(data_value):
    # cal_data = [x for x in data_value if x.marker=='Calibration']

    cal_headers = ['name', 'area', 'expected concentration', 'series', 'level', 'parallel', 'marker', 'analyte']
    samp_headers = ['name', 'area', 'expected concentration', 'series', 'level', 'parallel', 'marker', 'analyte']

    result = []
    samp_data = [x for x in data_value if x.marker=='Sample']
    for s, dv in common.groupby_column(samp_data, lambda x: x.series):
        lod_data = [x for x in dv if x.level=='LoQ']
        if len(lod_data) < 1:
            result.append(64)

    # result.extend(common.general_checks_by_marker(cal_data, cal_headers, 1, 0, 0, 0, operator.ge, operator.ge, operator.ge, operator.ge))
    # result.extend(common.general_checks_by_marker(samp_data, samp_headers, 1, 4, 5, 0,
    #     operator.ge, operator.ge, operator.ge, operator.ge))
    # return result

    cal = common.check_parameters_type(marker='Calibration', headers=cal_headers, series=1)
    samp = common.check_parameters_type(marker='Sample', headers=samp_headers, series=1, levels=4, parallels=5)

    result.extend(common.general_checks(data_value, [cal, samp]))
    return result

def acc_all_2019_03_28_1_4_5(dataset_id, assessment_method_id, analyte_id):
    prec_all_2019_03_28_2(dataset_id, assessment_method_id, analyte_id)

def acc_all_2019_03_28_3_4_1_check(data_value):
    # cal_data = [x for x in data_value if x.marker=='Calibration']
    # samp_data = [x for x in data_value if x.marker=='Sample']

    cal_headers = ['name', 'area', 'expected concentration', 'series', 'level', 'parallel', 'marker', 'analyte']
    samp_headers = ['name', 'area', 'expected concentration', 'series', 'level', 'parallel', 'marker', 'analyte']

    result = []
    samp_data = [x for x in data_value if x.marker=='Sample']
    for s, dv in common.groupby_column(samp_data, lambda x: x.series):
        lod_data = [x for x in dv if x.level=='LoQ']
        if len(lod_data) < 1:
            result.append(64)

    # result.extend(common.general_checks_by_marker(cal_data, cal_headers, 3, 0, 0, 0, operator.ge, operator.ge, operator.ge, operator.ge))
    # result.extend(common.general_checks_by_marker(samp_data, samp_headers, 3, 4, 1, 0,
    #     operator.ge, operator.ge, operator.ge, operator.ge))
    # return result

    cal = common.check_parameters_type(marker='Calibration', headers=cal_headers, series=3)
    samp = common.check_parameters_type(marker='Sample', headers=samp_headers, series=3, levels=4, parallels=1)

    result.extend(common.general_checks(data_value, [cal, samp]))
    return result

def acc_all_2019_03_28_3_4_1(dataset_id, assessment_method_id, analyte_id):
    prec_all_2019_03_28_2(dataset_id, assessment_method_id, analyte_id)

def rob_all_2019_03_28_check(data_value):
    # TODO integrate common.general_checks for IS logic

    default_present = False
    robust_param_present = False

    dv_by_marker = common.groupby_column(data_value, lambda x: x.marker)

    response = []

    for m, dv in dv_by_marker:
        if m == 'Default':
            default_present = True
            headers = ['name', 'area', 'Series', 'Level', 'Parallel', 'Analyte']
            response.extend(common.general_checks_by_marker(dv, headers))

        if common.is_robustness_parameter(m):
            robust_param_present = True
            headers = ['name', 'area', 'Series', 'Level', 'Parallel', 'Analyte']
            response.extend(common.general_checks_by_marker(dv, headers))

        if m == 'Calibration':
            headers = ['name', 'area', 'Expected concentration', 'Series', 'Level', 'Parallel', 'Analyte']
            response.extend(common.general_checks_by_marker(dv, headers))

    # TODO
    if not default_present:
        pass
    if not robust_param_present:
        pass

    return response

def rob_all_2019_03_28(dataset_id, assessment_method_id, analyte_id):
    rob_rug_all_2019_03_28_analyst(dataset_id, assessment_method_id, analyte_id, common.is_robustness_parameter)

def rob_rug_all_2019_03_28_analyst(dataset_id, assessment_method_id, analyte_id, marker_function):
    """
    Calculates Robustness for assessment methods
    Results (slope, intercept, experimental_concentration, trueness, mean_trueness, sd_trueness, rsd_trueness,
        mean experimental concentration, sd experimental concentration, rsd experimental concentration,
        mean area, sd area, rsd area,
        mean tr, mean rs, mean n, mean as, mean k,
        rsd tr, rsd rs, rsd n, rsd as, rsd k,
        change tr, change rs, change n, change as, change k,
        mean change tr, mean change rs, mean change n, mean change as, mean change k,
        rsd change tr, rsd change rs, rsd change n, rsd change as, rsd change k) will be saved to
    experiment_validation_experimentdatavalue

    @param dataset_id
    @param assessment_method_id
    @analyte_id
    """

    # Query database for needed data

    in_keys = {}
    out_keys = {}
    for key in ['area', 'expected concentration', 'is area', 'is expected concentration', 'tr', 'rs', 'n', 'as', 'k']:
        in_keys[key] = ExperimentData.objects.get(name=key, type=ExperimentData.INPUT).id
    for key in ['slope', 'intercept', 'is area ratio', 'is expected concentration ratio', 'experimental concentration', 'trueness', \
        'mean trueness', 'sd trueness', 'rsd trueness',\
        'mean experimental concentration', 'sd experimental concentration', 'rsd experimental concentration',\
        'mean area', 'sd area', 'rsd area',\
        'mean tr', 'mean rs', 'mean n', 'mean as', 'mean k',\
        'rsd tr', 'rsd rs', 'rsd n', 'rsd as', 'rsd k',\
        'change tr', 'change rs', 'change n', 'change as', 'change k',\
        'mean change tr', 'mean change rs', 'mean change n', 'mean change as', 'mean change k',\
        'rsd change tr', 'rsd change rs', 'rsd change n', 'rsd change as', 'rsd change k',\
        ]:
        out_keys[key] = ExperimentData.objects.get(name=key, type=ExperimentData.OUTPUT).id

    all_data_values = list(ExperimentDataValue.objects.filter(
        experiment_dataset=dataset_id,
        assessment_method=assessment_method_id,
        analyte=analyte_id,
        experiment_data__in=[in_keys[k] for k in in_keys]
        ).order_by('row'))

    def append_output(lst, value, experiment_data, series, level, parallel, marker, row):
        common.append_output_core(dataset_id, assessment_method_id, analyte_id,\
            lst, value, experiment_data, series, level, parallel, marker, row)

    # 3.1 By series
    # 3.1.1. Marker(Calibration): slope intercept
    # 3.1.2. By area: experimental concentration / data for trueness

    def series_results(series, data_value):
        series_output = []

        data_value = list(data_value)

        source = common.trueness_source(data_value, in_keys['expected concentration'])

        cal_data = list(filter(lambda x: x.marker=='Calibration', data_value))
        samp_data = list(filter(lambda x: x.marker=='Default' or marker_function(x.marker), data_value))

        [cal_area, cal_expec_conc, cal_is_area, cal_is_expected_concentration], cal_row, cal_series, cal_level, cal_parallel, cal_marker = \
            common.extract_columns(cal_data, [in_keys['area'], in_keys['expected concentration'], in_keys['is area'], in_keys['is expected concentration']])

        if source in ('trueness', 'experimental concentration'):
            slope, intercept, _, _, _, _, is_area_ratio_cal, is_expected_conc_ratio_cal =\
                common.calibration(cal_area, cal_expec_conc, cal_is_area, cal_is_expected_concentration)

            keys = ['slope', 'intercept']
            values = [slope, intercept]
            for key, val in zip(keys, values):
                append_output(series_output, val, out_keys[key], series, None, None, 'Calibration', None)

            # If IS is present then
            # 1. Save IS slope andintercept ratios
            # 2. Calculate and save IS area ratio for datarows
            if cal_is_area[0]:
                for value, r, l, p, m in zip(is_area_ratio_cal, cal_row, cal_level, cal_parallel, cal_marker):
                    append_output(series_output, value, out_keys['is area ratio'], series, l, p, m, r)
                for value, r, l, p, m in zip(is_expected_conc_ratio_cal, cal_row, cal_level, cal_parallel, cal_marker):
                    append_output(series_output, value, out_keys['is expected concentration ratio'], series, l, p, m, r)


        else:
            slope = None
            intercept = None
            cal_results = list()

        # extract input lists
        [samp_area, samp_expec_conc, samp_is_area, samp_is_expected_concentration], samp_row, samp_series, samp_level, samp_parallel, samp_marker = \
            common.extract_columns(samp_data, [in_keys['area'], in_keys['expected concentration'], in_keys['is area'], in_keys['is expected concentration']])

        # calculate
        experimental_concentration, trueness, _, _, _ =\
            common.trueness_mean(slope, intercept, samp_area, samp_expec_conc, samp_is_area, samp_is_expected_concentration, source)

        # compose output
        exper_conc_packed = list(zip(experimental_concentration, samp_row, samp_level, samp_parallel, samp_marker))
        trueness_packed = list(zip(trueness, samp_row, samp_level, samp_parallel, samp_marker))

        for val, r, l, p, m in exper_conc_packed:
            append_output(series_output, val, out_keys['experimental concentration'], series, l, p, m, r)

        for val, r, l, p, m in trueness_packed:
            append_output(series_output, val, out_keys['trueness'], series, l, p, m, r)

        return series_output

    dv_by_series = common.groupby_column(all_data_values, lambda x: x.series)
    output = []
    for series, data_value in dv_by_series:
        output.extend(series_results(series, data_value))

    # level
    # 3.2. By level
    # 3.2.1. Marker(Default): Mean(SST parameter)
    # 3.2.2. Marker(Default): Trueness
    # 3.2.3. By Marker(Robustness parameter): By Area: Change (%, SST

    def level_results(level, data_value):
        level_output = []

        sst_headers = ['tr', 'rs', 'n', 'as', 'k']
        sst_parameters = [in_keys[x] for x in sst_headers]

        default_data = list(filter(lambda x: x.marker == 'Default', data_value))
        samp_data = list(filter(lambda x: marker_function(x.marker), data_value))

        # extract input lists
        defaults, _, _, _, _,_ = \
            common.extract_columns(default_data, sst_parameters)

        samp_ssts, samp_row, samp_series, samp_level, samp_parallel, samp_marker = \
            common.extract_columns(samp_data, sst_parameters)

        samp_ssts = {k: v for k, v in zip(sst_headers, samp_ssts)}

        # calculate
        for sst_param, lst in zip(sst_headers, defaults):

            lst = [d for d in lst if d is not None]

            if len(lst) > 1:
                mean, _, rsd = common.mean_sd_rsd(lst)

                keys = ['mean ' + sst_param, 'rsd ' + sst_param]
                values = [mean, rsd]
                for k, v in zip(keys, values):
                    append_output(level_output, v, out_keys[k], None, level, None, 'Default', None)
            elif len(lst) == 1:
                mean = lst[0]
            else:
                mean = None

            if mean:
                values = samp_ssts[sst_param]
                sst_change = [(v - mean) * 100 / mean if v is not None else None for v in values]
                sst_change_packed = list(zip(sst_change, samp_row, samp_series, samp_level, samp_parallel, samp_marker))
                for val, r, s, l, p, m in sst_change_packed:
                    if val:
                        append_output(level_output, val, out_keys['change ' + sst_param], s, l, p, m, r)

        [area, exper_conc, trueness], samp_row, samp_series, samp_level, samp_parallel, samp_marker = \
            common.extract_columns(default_data, [in_keys['area'], out_keys['experimental concentration'], out_keys['trueness']])

        headers = ['area', 'experimental concentration', 'trueness']
        data_values = [area, exper_conc, trueness]
        for param, lst in zip(headers, data_values):

            lst = [d for d in lst if d is not None]

            if len(lst) > 1:
                mean, _, rsd = common.mean_sd_rsd(lst)

                keys = ['mean ' + param, 'rsd ' + param]
                values = [mean, rsd]
                for k, v in zip(keys, values):
                    append_output(level_output, v, out_keys[k], None, level, None, 'Default', None)

        return level_output

    exp_conc_trueness = [ x for x in output if x.experiment_data_id in [out_keys['experimental concentration'], out_keys['trueness']]]

    all_levels_input = [ x for x in all_data_values if x.marker == 'Default']
    all_levels_input.extend([ x for x in all_data_values if marker_function(x.marker)])
    all_levels_input.extend(exp_conc_trueness)

    dv_by_level = common.groupby_column(all_levels_input, lambda x: x.level)

    for level, data_value in dv_by_level:
        output.extend(level_results(level, data_value))

    # 3.3. By Marker(Robustness parameter)
    # 3.3.1. Mean(SST Parameter)
    # 3.3.2. Mean(Change(%, SST parameter))
    # 3.3.3. RSD(SST Parameter)
    # 3.3.4. RSD(Change(%, SST parameter))
    # 3.3.5. Trueness
    # 3.3.6. RSD(Data for trueness)
    def marker_results(marker, data_value):
        marker_output = []

        sst_in_headers = ['tr', 'rs', 'n', 'as', 'k', 'area']
        sst_out_headers = ['change tr', 'change rs', 'change n', 'change as', 'change k', 'experimental concentration', 'trueness']
        sst_headers = sst_in_headers + sst_out_headers

        sst_parameters = [in_keys[x] for x in sst_in_headers]
        sst_parameters.extend([out_keys[x] for x in sst_out_headers])

        samp_ssts, _, _, _, _, _ = \
            common.extract_columns(data_value, sst_parameters)

        for sst_param, lst in zip(sst_headers, samp_ssts):

            lst = [d for d in lst if d is not None]

            if len(lst) > 1:
                mean, _, rsd = common.mean_sd_rsd(lst)

                keys = ['mean ' + sst_param, 'rsd ' + sst_param]
                values = [mean, rsd]
                for k, v in zip(keys, values):
                    append_output(marker_output, v, out_keys[k], None, None, None, marker, None)

        return marker_output


    marker_input_dv = [x for x in all_data_values if marker_function(x.marker)]
    marker_input_dv.extend([x for x in output if marker_function(x.marker)])

    dv_by_marker = common.groupby_column(marker_input_dv, lambda x: x.marker)
    for level, data_value in dv_by_marker:
        output.extend(marker_results(level, data_value))

    # Delete previous results
    common.clear_output(dataset_id, assessment_method_id, analyte_id)

    # Write into DB
    ExperimentDataValue.objects.bulk_create(output)

def rug_all_2019_03_28_stability_check(data_value):
    # TODO integrate common.general_checks for IS logic

    dv_by_marker = common.groupby_column(data_value, lambda x: x.marker)

    response = []

    for m, dv in dv_by_marker:
        if common.is_ruggedness_stability_parameter(m):
            headers = ['name', 'area', 'series', 'level', 'analyte']
            response.extend(common.general_checks_by_marker(dv, headers))

        if m == 'Calibration':
            headers = ['name', 'area', 'expected concentration', 'series', 'level', 'parallel', 'analyte']
            response.extend(common.general_checks_by_marker(dv, headers))

    return response

def rug_all_2019_03_28_stability(dataset_id, assessment_method_id, analyte_id):
    """
    Calculates Rugeddness for assessment method stability
    Results (slope, intercept, experimental_concentration, trueness, mean_trueness, sd_trueness, rsd_trueness,
        mean experimental concentration, sd experimental concentration, rsd experimental concentration,\
        mean area, sd area, rsd area) will be saved to
    experiment_validation_experimentdatavalue

    @param dataset_id
    @param assessment_method_id
    @analyte_id
    """

    # Query database for needed data

    in_keys = {}
    out_keys = {}
    for key in ['area', 'expected concentration', 'is area', 'is expected concentration', 'datetime']:
        in_keys[key] = ExperimentData.objects.get(name=key, type=ExperimentData.INPUT).id
    for key in ['slope', 'intercept', 'is area ratio', 'is expected concentration ratio', 'experimental concentration', 'trueness', \
        'mean trueness', 'sd trueness', 'rsd trueness',\
        'mean experimental concentration', 'sd experimental concentration', 'rsd experimental concentration',\
        'mean area', 'sd area', 'rsd area',\
        'stability graph'
        ]:
        out_keys[key] = ExperimentData.objects.get(name=key, type=ExperimentData.OUTPUT).id

    all_data_values = list(ExperimentDataValue.objects.filter(
        experiment_dataset=dataset_id,
        assessment_method=assessment_method_id,
        analyte=analyte_id,
        experiment_data__in=[in_keys[k] for k in in_keys]
        ).order_by('row'))

    def append_output(lst, value, experiment_data, series, level, parallel, marker, row):
        common.append_output_core(dataset_id, assessment_method_id, analyte_id,\
            lst, value, experiment_data, series, level, parallel, marker, row)

    def series_results(series, data_value):
        series_output = []

        data_value = list(data_value)

        # source = common.trueness_source(data_value, in_keys['expected concentration'])

        cal_data = list(filter(lambda x: x.marker=='Calibration', data_value))
        samp_data = list(filter(lambda x: x.marker=='Default' or common.is_ruggedness_stability_parameter(x.marker), data_value))

        if len(cal_data) > 1:

            [cal_area, cal_expec_conc, cal_is_area, cal_is_expected_concentration], cal_row, cal_series, cal_level, cal_parallel, cal_marker = \
                common.extract_columns(cal_data, [in_keys['area'], in_keys['expected concentration'], in_keys['is area'], in_keys['is expected concentration']])

            slope, intercept, _, _, _, _, is_area_ratio_cal, is_expected_conc_ratio_cal = \
                common.calibration(cal_area, cal_expec_conc, cal_is_area, cal_is_expected_concentration)

            keys = ['slope', 'intercept']
            values = [slope, intercept]
            for key, val in zip(keys, values):
                append_output(series_output, val, out_keys[key], series, None, None, 'Calibration', None)

            # If IS is present then
            # 1. Save IS slope andintercept ratios
            # 2. Calculate and save IS area ratio for datarows
            if cal_is_area[0]:
                for value, r, l, p, m in zip(is_area_ratio_cal, cal_row, cal_level, cal_parallel, cal_marker):
                    append_output(series_output, value, out_keys['is area ratio'], series, l, p, m, r)
                for value, r, l, p, m in zip(is_expected_conc_ratio_cal, cal_row, cal_level, cal_parallel, cal_marker):
                    append_output(series_output, value, out_keys['is expected concentration ratio'], series, l, p, m, r)

            experiment_conc = common.experimental_concentration(cal_area, cal_is_area, cal_is_expected_concentration, slope, intercept)

            trueness = [experiment / expected * 100 if expected else None for experiment, expected in zip(experiment_conc, cal_expec_conc)]

            # compose output
            exper_conc_packed = list(zip(experiment_conc, cal_row, cal_level, cal_parallel, cal_marker))
            trueness_packed = list(zip(trueness, cal_row, cal_level, cal_parallel, cal_marker))

            for val, r, l, p, m in exper_conc_packed:
                append_output(series_output, val, out_keys['experimental concentration'], series, l, p, m, r)

            for val, r, l, p, m in trueness_packed:
                append_output(series_output, val, out_keys['trueness'], series, l, p, m, r)

        return series_output

    dv_by_series = common.groupby_column(all_data_values, lambda x: x.series)
    output = []
    for series, data_value in dv_by_series:
        output.extend(series_results(series, data_value))

    def marker_results(marker, data_value):

        marker_output = []

        sst_parameters = [in_keys['area'], in_keys['is area'], in_keys['is expected concentration'], out_keys['experimental concentration'], out_keys['trueness'], in_keys['datetime']]

        # def to_datetime(x):
        #     try:
        #         return datetime.strptime(x, '%d.%m.%y %H:%M:%S')
        #     except ValueError:
        #         try:
        #             return datetime.strptime(x, '%d.%m.%Y %H:%M:%S')
        #         except ValueError:
        #             try:
        #                 return datetime.strptime(x, '%d.%m.%y %H:%M')
        #             except ValueError:
        #                 return datetime.strptime(x, '%d.%m.%Y %H:%M')

        formats = [float, float, float, float, float, str]

        [area, samp_is_area, samp_is_expected_concentration, exper_conc, trueness, dt], samp_row, samp_series, samp_level, samp_parallel, _ = \
            common.extract_formated_columns(data_value, sst_parameters, formats)

        if samp_is_area[0]:
            is_area_ratio_samp = [b / i_a for b, i_a in zip(area, samp_is_area)]
            for value, r, s, l, p in zip(is_area_ratio_samp, samp_row, samp_series, samp_level, samp_parallel):
                append_output(marker_output, value, out_keys['is area ratio'], s, l, p, marker, r)

        # for d in dt:
        #     print('{} in datetime: {}'.format(d, to_datetime(d)))

        if reduce(lambda a, b: a and b, [x != None for x in trueness]):
            source = 'trueness'
            y_axis = trueness
        elif reduce(lambda a, b: a and b, [x != None for x in exper_conc]):
            source = 'experimental concentration'
            y_axis = exper_conc
        else:
            source = 'area'
            y_axis = area

        x_axis = dt
        header = marker + ' stability'
        y_label = source + ' %'

        # TODO what to do when date is none

        stability_graph_url = common.draw_gridplot([x_axis, y_axis], ['Date', y_label], header)

        append_output(marker_output, stability_graph_url, out_keys['stability graph'], None, None, None, marker, 0)

        return marker_output

    marker_input_dv = [x for x in all_data_values if common.is_ruggedness_stability_parameter(x.marker)]
    marker_input_dv.extend([x for x in output if common.is_ruggedness_stability_parameter(x.marker)])

    dv_by_marker = common.groupby_column(marker_input_dv, lambda x: x.marker)
    for marker, data_value in dv_by_marker:
        output.extend(marker_results(marker, data_value))

    # Delete previous results
    common.clear_output(dataset_id, assessment_method_id, analyte_id)

    # Write into DB
    ExperimentDataValue.objects.bulk_create(output)


def rug_all_2019_03_28_analyst_check(data_value):
    # TODO integrate common.general_checks for IS logic

    default_present = False
    ruggedness_param_present = False

    dv_by_marker = common.groupby_column(data_value, lambda x: x.marker)

    response = []

    for m, dv in dv_by_marker:
        if m == 'Default':
            default_present = True
            headers = ['name', 'area', 'series', 'level', 'analyte']
            response.extend(common.general_checks_by_marker(dv, headers))

        if common.is_ruggedness_analyst_parameter(m):
            ruggedness_param_present = True
            headers = ['name', 'area', 'series', 'level', 'analyte']
            response.extend(common.general_checks_by_marker(dv, headers))

        if m == 'Calibration':
            headers = ['name', 'area', 'expected concentration', 'series', 'level', 'parallel', 'analyte']
            response.extend(common.general_checks_by_marker(dv, headers))

    return response

def rug_all_2019_03_28_analyst(dataset_id, assessment_method_id, analyte_id):
    rob_rug_all_2019_03_28_analyst(dataset_id, assessment_method_id, analyte_id, common.is_ruggedness_analyst_parameter)

def sst_all_2019_03_28_sst_check(data_value):
    # TODO integrate common.general_checks for IS logic

    cal_data = [x for x in data_value if x.marker=='Calibration']
    samp_data = [x for x in data_value if x.marker=='Sample']

    cal_headers = ['name', 'area', 'expected concentration', 'series', 'level', 'parallel', 'marker', 'analyte']
    # TODO At least one of the SST parameters: Theoretical plate number (N), Resolution (Rs), Retention time (tR), Asymmetry of the peak, Retention factor (k)
    samp_headers = ['name', 'area', 'series', 'level', 'analyte']

    result = common.general_checks_by_marker(cal_data, cal_headers)
    result.extend(common.general_checks_by_marker(samp_data, samp_headers))
    return result


def sst_all_2019_03_28_sst(dataset_id, assessment_method_id, analyte_id):
    """
    Calculates SST for assessment methods
    Results (slope, intercept, experimental_concentration, trueness, mean_trueness, sd_trueness, rsd_trueness,
        mean experimental concentration, sd experimental concentration, rsd experimental concentration,
        mean area, sd area, rsd area,
        mean tr, mean rs, mean n, mean as, mean k,
        rsd tr, rsd rs, rsd n, rsd as, rsd k,
        change tr, change rs, change n, change as, change k,
        mean change tr, mean change rs, mean change n, mean change as, mean change k,
        rsd change tr, rsd change rs, rsd change n, rsd change as, rsd change k) will be saved to
    experiment_validation_experimentdatavalue

    @param dataset_id
    @param assessment_method_id
    @analyte_id
    """

    # Query database for needed data

    in_keys = {}
    out_keys = {}
    for key in ['area', 'expected concentration', 'is area', 'is expected concentration', 'tr', 'rs', 'n', 'as', 'k']:
        in_keys[key] = ExperimentData.objects.get(name=key, type=ExperimentData.INPUT).id
    for key in ['slope', 'intercept', 'is area ratio', 'is expected concentration ratio', 'experimental concentration', 'trueness', \
        'mean trueness', 'sd trueness', 'rsd trueness',\
        'mean experimental concentration', 'sd experimental concentration', 'rsd experimental concentration',\
        'mean area', 'sd area', 'rsd area',\
        'mean tr', 'mean rs', 'mean n', 'mean as', 'mean k',\
        'rsd tr', 'rsd rs', 'rsd n', 'rsd as', 'rsd k',\
        'change tr', 'change rs', 'change n', 'change as', 'change k',\
        'mean change tr', 'mean change rs', 'mean change n', 'mean change as', 'mean change k',\
        'rsd change tr', 'rsd change rs', 'rsd change n', 'rsd change as', 'rsd change k',\
        ]:
        out_keys[key] = ExperimentData.objects.get(name=key, type=ExperimentData.OUTPUT).id

    all_data_values = list(ExperimentDataValue.objects.filter(
        experiment_dataset=dataset_id,
        assessment_method=assessment_method_id,
        analyte=analyte_id,
        experiment_data__in=[in_keys[k] for k in in_keys]
        ).order_by('row'))

    def append_output(lst, value, experiment_data, series, level, parallel, marker, row):
        common.append_output_core(dataset_id, assessment_method_id, analyte_id,\
            lst, value, experiment_data, series, level, parallel, marker, row)

    # 1. By Level for Marker(Sample):
    # 1.1. Mean(SST parameter)
    # 1.2. RSD(SST parameter)
    # 1.3. By Area:
    # 1.3.1. Change (%, SST parameter)
    def level_results(level, data_value):
        level_output = []

        sst_headers = ['tr', 'rs', 'n', 'as', 'k']
        sst_parameters = [in_keys[x] for x in sst_headers]

        samp_ssts, samp_row, samp_series, samp_level, samp_parallel, samp_marker = \
            common.extract_columns(data_value, sst_parameters)

        samp_ssts_dict = {k: v for k, v in zip(sst_headers, samp_ssts)}

        # calculate
        for sst_param, lst in zip(sst_headers, samp_ssts):

            lst = [d for d in lst if d is not None]

            if len(lst) > 1:
                mean, _, rsd = common.mean_sd_rsd(lst)

                keys = ['mean ' + sst_param, 'rsd ' + sst_param]
                values = [mean, rsd]
                for k, v in zip(keys, values):
                    append_output(level_output, v, out_keys[k], None, level, None, 'Sample', None)
            elif len(lst) == 1:
                mean = lst[0]
            else:
                mean = None

            if mean:
                values = samp_ssts_dict[sst_param]
                sst_change = [(v - mean) * 100 / mean if v is not None else None for v in values]
                sst_change_packed = list(zip(sst_change, samp_row, samp_series, samp_level, samp_parallel, samp_marker))
                for val, r, s, l, p, m in sst_change_packed:
                    if val:
                        append_output(level_output, val, out_keys['change ' + sst_param], s, l, p, m, r)

        return level_output

    output = []

    dv_by_level = common.groupby_column(all_data_values, lambda x: x.level)

    for level, data_value in dv_by_level:
        output.extend(level_results(level, data_value))

    # 2. By Series:
    # 2.1. Calibration data
    # 2.2. If Marker(Calibration) is present:
    # Experimental concentration
    # 2.3. If Marker(Calibration) present and Expected concentration present for Marker (default):
    # Data for trueness
    def series_results(series, data_value):
        series_output = []

        data_value = list(data_value)

        source = common.trueness_source(data_value, in_keys['expected concentration'])

        cal_data = list(filter(lambda x: x.marker=='Calibration', data_value))
        samp_data = list(filter(lambda x: x.marker=='Sample', data_value))

        [cal_area, cal_expec_conc, cal_is_area, cal_is_expected_concentration], cal_row, cal_series, cal_level, cal_parallel, cal_marker =\
            common.extract_columns(cal_data, [in_keys['area'], in_keys['expected concentration'], in_keys['is area'], in_keys['is expected concentration']])

        if source in ('trueness', 'experimental concentration'):
            slope, intercept, _, _, _, _, is_area_ratio_cal, is_expected_conc_ratio_cal = \
                common.calibration(cal_area, cal_expec_conc, cal_is_area, cal_is_expected_concentration)

            keys = ['slope', 'intercept']
            values = [slope, intercept]
            for key, val in zip(keys, values):
                append_output(series_output, val, out_keys[key], series, None, None, 'Calibration', None)

            # If IS is present then
            # 1. Save IS slope andintercept ratios
            # 2. Calculate and save IS area ratio for datarows
            if cal_is_area[0]:
                for value, r, l, p, m in zip(is_area_ratio_cal, cal_row, cal_level, cal_parallel, cal_marker):
                    append_output(series_output, value, out_keys['is area ratio'], series, l, p, m, r)
                for value, r, l, p, m in zip(is_expected_conc_ratio_cal, cal_row, cal_level, cal_parallel, cal_marker):
                    append_output(series_output, value, out_keys['is expected concentration ratio'], series, l, p, m, r)

        else:
            slope = None
            intercept = None
            cal_results = list()

        # extract input lists
        [samp_area, samp_expec_conc, samp_is_area, samp_is_expected_concentration], samp_row, samp_series, samp_level, samp_parallel, samp_marker = \
            common.extract_columns(samp_data, [in_keys['area'], in_keys['expected concentration'], in_keys['is area'], in_keys['is expected concentration']])

        # calculate
        experimental_concentration, trueness, _, _, _ =\
            common.trueness_mean(slope, intercept, samp_area, samp_expec_conc, samp_is_area, samp_is_expected_concentration, source)

        # compose output
        exper_conc_packed = list(zip(experimental_concentration, samp_row, samp_level, samp_parallel, samp_marker))
        trueness_packed = list(zip(trueness, samp_row, samp_level, samp_parallel, samp_marker))

        for val, r, l, p, m in exper_conc_packed:
            append_output(series_output, val, out_keys['experimental concentration'], series, l, p, m, r)

        for val, r, l, p, m in trueness_packed:
            append_output(series_output, val, out_keys['trueness'], series, l, p, m, r)

        if samp_is_area[0]:
            is_area_ratio_samp = [b / i_a for b, i_a in zip(samp_area, samp_is_area)]
            for value, r, l, p, m in zip(is_area_ratio_samp, samp_row, samp_level, samp_parallel, samp_marker):
                append_output(series_output, value, out_keys['is area ratio'], series, l, p, m, r)

        return series_output

    dv_by_series = common.groupby_column(all_data_values, lambda x: x.series)

    for series, data_value in dv_by_series:
        output.extend(series_results(series, data_value))

    # 3. By Level for Marker(Sample):
    # 3.3. Mean(Change(%, SST parameter))
    # 3.4. If Marker(Calibration) is missing:
    # 3.4.1. Mean(Area)
    # 3.4.2. RSD(Area)
    # 3.5. If Marker(Calibration) present:
    # 3.5.1. Mean(Experimental concentration)
    # 3.5.2. RSD(Experimental concentration)
    # 3.6 If Marker(Calibration) present and Expected concentration present for Marker (default):
    # 3.6.1. Trueness
    # 3.6.2. RSD(Data for trueness)

    def level_results(level, data_value):
        level_output = []

        sst_in_headers = ['area']
        sst_out_headers = ['change tr', 'change rs', 'change n', 'change as', 'change k', 'experimental concentration', 'trueness']
        sst_headers = sst_in_headers + sst_out_headers

        sst_parameters = [in_keys[x] for x in sst_in_headers]
        sst_parameters.extend([out_keys[x] for x in sst_out_headers])

        samp_ssts, _, _, _, _, _ = \
            common.extract_columns(data_value, sst_parameters)

        for sst_param, lst in zip(sst_headers, samp_ssts):

            lst = [d for d in lst if d is not None]

            if len(lst) > 1:
                mean, _, rsd = common.mean_sd_rsd(lst)

                keys = ['mean ' + sst_param, 'rsd ' + sst_param]
                values = [mean, rsd]
                for k, v in zip(keys, values):
                    append_output(level_output, v, out_keys[k], None, level, None, 'Sample', None)

        return level_output

    all_levels_input = [ x for x in all_data_values if x.marker == 'Sample']
    all_levels_input.extend([ x for x in output if x.marker == 'Sample' and x.row != None])

    dv_by_level = common.groupby_column(all_levels_input, lambda x: x.level)

    for level, data_value in dv_by_level:
        output.extend(level_results(level, data_value))

    # Delete previous results
    common.clear_output(dataset_id, assessment_method_id, analyte_id)

    # Write into DB
    ExperimentDataValue.objects.bulk_create(output)

def dummy_check(data_value):
    return []

def dummy(dataset_id, assessment_method_id, analyte_id):
    pass
