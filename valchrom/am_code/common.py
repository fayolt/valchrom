import numpy as np
import itertools as it
import functools as ft
import statsmodels.api as sm
from math import sqrt
import re
import operator
from collections import namedtuple

# imports for graph generation
import matplotlib as mpl
mpl.use("Agg")
import seaborn as sb
import pandas as pd

# mports for saving file
from django.conf import settings
import os
import uuid

from experiment_validation.models import ExperimentData, ExperimentDataValue, ExperimentDataset, AssessmentMethod, Analyte

###
# Helper functions to group and orginize lists.
# Used for checkings
###

def groupby_column(data_values, keyfunc):
    """
    Groups data_values into pairs (key, matching set)
    @param data_values - collection of type A
    @param keyfunc - function that takes type A as input and returns some value
    """

    sorted_values = sorted(data_values, key=keyfunc)
    result = [(k, list(g)) for k, g in it.groupby(sorted_values, key=keyfunc)]
    return result

###
# Helper Functions to work with ExperimentDataValue objects
# Used for Querying, deleting and saving data to DB.
# Also extracting needed values from ExperimentDataVlue
###

def is_ruggedness_stability_parameter(marker):
    return marker in ['FreezeThaw', 'ShortTerm','LongTerm','Stock','Working','Processed','Autosampler']

def is_ruggedness_analyst_parameter(marker):
    kv = [x.strip() for x in marker.split(':', 1)]
    if len(kv) < 2:
        return False
    if kv[0] != 'Analyst':
        return False
    return True

def is_robustness_parameter(marker):
    rps = [x.strip() for x in marker.split(';')]
    for input in rps:
        kv = [x.strip() for x in input.split(':', 1)]
        if len(kv) < 2:
            return False
        if kv[0] not in ['pH', 'ratio', 'additives', 'columns', 'temp', 'flow']:
            return False
    return True

def get_input_columns(columns, data_values):
    """
    Extracts named "columns" from experiment_data_value structure.
    Function expects all data in datavalues to correspont to one datarow.

    @param columns - list of strings that match columns to be extracted
    @param data_values - list of experiment_data_value objects

    @return extracted values (converted to float) in the same order as their labels were.
    If conversion to float fails, then corresponding value is left None
    """
    values = [None for x in columns]
    for v in data_values:
        for k, i in zip(columns, it.count(0)):
            if v.experiment_data_id == k:
                try:
                    values[i] = float(v.value)
                except ValueError:
                    pass
    return values

def extract_columns(data_values, columns):
    """
    Extract values listed in columns and row_id, level, parallel, marker lists from given experiment_data_value list
    All data is converted to float, if possible, None otherwise

    @param data_values - list of experiment_data_value
    @param columns - list of columns to extract

    @return [list of parameter lists], row_ids, level, parallel, marker
    """
    values = [list() for x in columns]
    row_ids = list()
    series = list()
    level = list()
    parallel = list()
    marker = list()

    dv_by_row = groupby_column(data_values, lambda x: x.row)
    for row, dvs in dv_by_row:
        dv = list(dvs)
        temp = get_input_columns(columns, dv)
        for k, i in zip(columns, it.count(0)):
            values[i].append(temp[i])
        row_ids.append(row)
        series.append(dv[0].series)
        level.append(dv[0].level)
        parallel.append(dv[0].parallel)
        marker.append(dv[0].marker)
    return values, row_ids, series, level, parallel, marker

def get_raw_input_columns(columns, data_values):
    """
    Extracts named "columns" from experiment_data_value structure.
    Function expects all data in datavalues to correspont to one datarow.

    @param columns - list of strings that match columns to be extracted
    @param data_values - list of experiment_data_value objects

    @return extracted values in the same order as their labels were.
    """
    values = [None for x in columns]
    for v in data_values:
        for k, i in zip(columns, it.count(0)):
            if v.experiment_data_id == k:
                values[i] = v.value
    return values

def extract_formated_columns(data_values, columns, formats=None):
    """
    Extract values listed in columns and row_id, level, parallel, marker lists from given experiment_data_value list

    @param data_values - list of experiment_data_value
    @param columns - list of columns to extract
    @param formats - list of conversion functions, to convert values from string to needed type

    @return [list of parameter lists], row_ids, level, parallel, marker
    """
    values = [list() for x in columns]
    row_ids = list()
    series = list()
    level = list()
    parallel = list()
    marker = list()
    if formats == None:
        formats = [float for x in columns]

    dv_by_row = groupby_column(data_values, lambda x: x.row)
    for row, dvs in dv_by_row:
        dv = list(dvs)
        temp = get_raw_input_columns(columns, dv)
        for k, i in zip(columns, it.count(0)):
            try:
                values[i].append(formats[i](temp[i]))
            except ValueError:
                values[i].append(None)
            except TypeError:
                values[i].append(None)
        row_ids.append(row)
        series.append(dv[0].series)
        level.append(dv[0].level)
        parallel.append(dv[0].parallel)
        marker.append(dv[0].marker)
    return values, row_ids, series, level, parallel, marker

def clear_output(dataset_id, assessment_method_id, analyte_id):
    """
    Delete previous output from database

    @param dataset_id
    @param assessment_method_id
    @param analyte_id
    """
    # Delete previous results
    output_labels = map(lambda x: x.id, ExperimentData.objects.filter(type=ExperimentData.OUTPUT))
    ExperimentDataValue.objects.filter(
        experiment_dataset=dataset_id, assessment_method=assessment_method_id,
        analyte=analyte_id, experiment_data_id__in=output_labels).delete()

def append_output_core(experiment_dataset_id, assessment_method_id, analyte_id, lst, value, experiment_data_id, series, level, parallel, marker, row):
    lst.append(ExperimentDataValue(value=value,
        series=series,
        experiment_data_id=experiment_data_id,
        experiment_dataset_id=experiment_dataset_id,
        assessment_method_id=assessment_method_id,
        analyte_id=analyte_id,
        row=row,
        level=level,
        parallel=parallel,
        marker=marker)
    )

###
# Helper functions used for calculations
###

def draw_lmplot(data, labels, header, formula=None):
    """
    Composes and saves linear regression graph onto the server.

    @param data - 2 lists composed into list
    @param labels - [x label, y label]
    @param header - string graph's header
    @param formula - string of formula to show in legend. In progress.

    @return relative URL to the saved graph
    """
    points = dict()
    for d, l in zip(data, labels):
        points[l] = d
    df = pd.DataFrame(data=points)

    if formula:
        graph = sb.lmplot(x=labels[0], y=labels[1], data=df, ci=None, line_kws={'label': formula}, legend_out=True)
        graph.fig.legend()
    else:
        graph = sb.lmplot(x=labels[0], y=labels[1], data=df, ci=None)

    # graph = sb.lmplot(x=labels[0], y=labels[1], data=df, ci=None, legend=True)
    # if formula:
    #     graph._legend.texts[0].set_text(formula)
    #
    graph.fig.suptitle(header, fontsize=12)

    # save image return url
    unique_name = str(uuid.uuid4())[:12] + '.png'
    full_name = os.path.join(settings.MEDIA_ROOT, unique_name)
    graph.savefig(full_name)
    mpl.pyplot.close(graph.fig)
    url = settings.MEDIA_URL + unique_name
    return url

def draw_gridplot(data, labels, header):
    """
    Composes and saves graph onto the server.

    @param data - 2 lists composed into list
    @param labels - x label, y label
    @param header - string graph's header

    @return relative URL to the saved graph
    """
    points = dict()

    for d, l in zip(data, labels):
        points[l] = d
    df = pd.DataFrame(data=points)
    df.sort_values(by=labels[0])

    graph = sb.PairGrid(df, y_vars=labels[1],
                     x_vars=labels[0], height=15)

    # Draw a seaborn pointplot onto each Axes
    graph.map(sb.pointplot, scale=1.0, errwidth=0, color="xkcd:plum")
    graph.fig.suptitle(header, fontsize=12)

    # save image return url
    unique_name = str(uuid.uuid4())[:12] + '.png'
    full_name = os.path.join(settings.MEDIA_ROOT, unique_name)
    graph.savefig(full_name)
    mpl.pyplot.close(graph.fig)
    url = settings.MEDIA_URL + unique_name
    return url

def experimental_concentration(area, is_area, is_expected_concentration, slope_ratio, intercept_ratio):
    if is_area[0]:
        return [ ((a / i_a - intercept_ratio ) * i_e_c) / slope_ratio \
            for a, i_a, i_e_c in zip(area, is_area, is_expected_concentration)]
    else:
        return [(a - intercept_ratio) / slope_ratio for a in area]

def residuals(area, expected_concentration, slope, intercept):
    return [ a - (t * slope + intercept)
        for a, t in zip(area, expected_concentration)]

def rss(area, expected_concentration, slope, intercept):
    rsquared = [x * x for x in residuals(area, expected_concentration, slope, intercept)]
    return sum(rsquared)

def s0(exper_conc):
    """Specific for: LoD-LoQ: Calculate standard deviation from 6 (minimum) spikings (Eurachem)"""
    mean_conc, _, _ = mean_sd_rsd(exper_conc) # np.mean(exper_conc)
    result = sqrt(sum([(ce - mean_conc) * (ce - mean_conc) for ce in exper_conc]) / (len(exper_conc) - 1))
    return result

def s0prime(s0, n, nb):
    """Specific for: LoD-LoQ: Calculate standard deviation from 6 (minimum) spikings (Eurachem)"""
    if nb == 0:
        result = s0 / sqrt(n)
    else:
        result = s0 * sqrt(1 / n + 1/ nb)
    return result


def calibration(area, expected_concentration, is_area=[None], is_expected_concentration=[None]):
    """
    Executes linear regression on calibration data.
    If Internal standard area (is_area) is present, then Internal standard expected concentration is expected to be present.
    Also if is_area is present, area ratio and expected concentration ratio are calculated and returned.

    @param area - array_like of floats - consisting actual areas
    @param expected_concentration - array_like of floats - consisting expected_concentration....
    @param is_area - array_like of floats - consisting actual internal standard areas
    @param is_expected_concentration - array_like of floats - consisting internal standard expected_concentration

    @returns slope, intercept, rsquared, rss, sd_slope, sd_intercept
    """

    if is_area[0]:
        area_ratio = [a / i_a for a, i_a in zip(area, is_area)]
        expected_concentration_ratio = [a / i_a for a, i_a in zip(expected_concentration, is_expected_concentration)]

        area = area_ratio
        expected_concentration = expected_concentration_ratio
    else:
        area_ratio = None
        expected_concentration_ratio = None

    expec_conc_const = sm.add_constant(expected_concentration)
    mod = sm.OLS(area, expec_conc_const)
    res = mod.fit()

    slope = res.params[1]
    intercept = res.params[0]
    rss_value = rss(area, expected_concentration, slope, intercept)
    sd_slope = res.bse[1]
    sd_intercept = res.bse[0]

    return slope, intercept, res.rsquared, rss_value, sd_slope, sd_intercept, area_ratio, expected_concentration_ratio

def trueness_source(data_value, expect_conc_id, markers_func=lambda x: x == 'Sample'):
    """
    Check data for mean, sd, rsd calculation source: Area / Experimental concentration / Data for trueness
    All data is expected tohave marker 'Calibration' or 'Sample'

    @param data_value - all experiment_data_value object for assessment method
    @param expect_conc_id - id of 'expected concentration' experiment_data_id
    @param markers_func - function to test if marker is belongs to samples
    """

    calibration_missing = False
    expected_concentration_missing = False

    cnt_by_series = groupby_column(data_value, lambda x: '{}'.format(x.series))

    for series, dv_series in cnt_by_series:

        cal_data = list(filter(lambda x: x.marker=='Calibration', dv_series))
        samp_data = list(filter(lambda x: markers_func(x.marker), dv_series))

        cal_by_row = groupby_column(cal_data, lambda x: '{}'.format(x.row))

        if len(cal_by_row) < 2:
            calibration_missing = True

        # Check if any row has expected_conc missing
        [expect_conc], _, _, _, _,_ = extract_columns(samp_data, [expect_conc_id])

        expect_conc_none = list(filter(lambda x: x == None, expect_conc))

        if len(expect_conc_none) > 0:
            expected_concentration_missing = True

    if calibration_missing:
        return 'area'
    elif expected_concentration_missing:
        return 'experimental concentration'
    else:
         return 'trueness'

def trueness_mean(slope, intercept, area, expected_concentration, is_area, is_expected_concentration, source):
    """
    Calculates Mean and SD of (trueness/experimental concentration/area)

    @param slope - float
    @param intercept - float
    @param area - array_like of floats - consisting actual areas
    @param expected_concentration - array_like of floats - consisting expected_concentration....
    @param is_area - array_like of floats - internal standard areas
    @param is_expected_concentration - array_like of floats - internal standard expected_concentration
    @param source - determins which column to use while calculating

    # @returns experimental_concentration, trueness, mean_trueness, sd_trueness, rsd_trueness
    """

    experiment_conc =list()
    trueness = list()

    if source in ('trueness', 'experimental concentration'):

        experiment_conc = experimental_concentration(area, is_area, is_expected_concentration, slope, intercept)

        if source == 'trueness':
            trueness = [experiment / expected * 100 for experiment, expected in zip(experiment_conc, expected_concentration)]
            values = trueness
        else:
            values = experiment_conc
    else:
        values = area

    mean_trueness, sd_trueness, rsd_trueness = mean_sd_rsd(values)
    return experiment_conc, trueness, mean_trueness, sd_trueness, rsd_trueness

def mean_sd_rsd(values):
    """
    Calculates Mean and SD and RSD

    @param values - array_like of floats

    # @returns mean_trueness, sd_trueness, rsd_trueness
    """

    mean_trueness = np.mean(values)
    # standard way to calculate standard deviation is to use denominator N-1. Same way as excel does
    sd_trueness = np.std(values, ddof=1)
    rsd_trueness = sd_trueness / mean_trueness * 100
    return mean_trueness, sd_trueness, rsd_trueness

###
# General Data Checking for math modules
###

def general_checks_by_marker(data_values, headers, series=0, levels=0, parallels=0, datarows=0,\
    series_op=operator.ge, levels_op=operator.ge, parallels_op=operator.ge, datarows_op=operator.ge):
    """
    Check math module input data for general restrictions. Check return values for restrictions.
    Used by general_checks() function to check by marker.

    @param - data_values - list of ExperimentDataValue objects. All input values from csv.
    @param - headers - list of mandatory header code-names.
    @param - series - Nr of mandatory series.
    @param - levels - Nr of mandatory levels in series.
    @param - parallels - Nr of mandatory parallels in level.
    @param - datarows - Nr of mandatory datarows in level.
    @param - series_op - condition for checking series count gt, ge, eq, le, lt
    @param - levels_op - condition for checking levels count gt, ge, eq, le, lt
    @param - parallels_op - condition for checking parallels count gt, ge, eq, le, lt
    @param - datarows_op - condition for checking datarows count gt, ge, eq, le, lt

    @return - 0 - All OK
            - 1 - Mandatory column has missing value
            - 2 - Column 'Name' is missing for some row
            - 4 - Row has 'excpected concentration', but has missing 'area'
            - 8 - Marker(Calibration) does not have 2 rows
            - 16 - Marker(Calibration) does not have 'area' or 'expected concentration'
            - 32 - Marker(Sample) or Marker (Blank) does not have 'area'
            - 64 - Dataset is missing correct set of 'series', 'levels', 'parallels', 'replicates'
    """

    columns_present = True
    name_present = True
    area_have_expected_conc = True
    cal_has_2_rows = True
    cal_has_area_expect_conc = True
    samp_has_area = True
    enough_data = True

    header_objects = ExperimentData.objects.filter(name__in=headers)
    name_object = ExperimentData.objects.get(name='name')
    area_object = ExperimentData.objects.get(name='area')
    expected_conc_object = ExperimentData.objects.get(name='expected concentration')

    series_needed = 'series' in headers
    level_needed = 'level' in headers
    parallel_needed = 'parallel' in headers
    marker_needed = 'marker' in headers
    analyte_needed = 'analyte' in headers

    dv_by_row = groupby_column(data_values, lambda dv: dv.row)
    for r, dvs in dv_by_row:

        # Check mandatory columns
        cols_in_csv = [dv.experiment_data for dv in dvs]
        for h in header_objects:
            if h not in cols_in_csv:
                columns_present = False

        if (series_needed and dvs[0].series == None) \
            or (level_needed and dvs[0].level == None) \
            or (parallel_needed and dvs[0].parallel == None) \
            or (marker_needed and dvs[0].marker == None) \
            or (analyte_needed and dvs[0].analyte_id == None):
            columns_present = False

        # Every row should have Name data type
        if name_object not in cols_in_csv:
            name_present = False

        # Expected concentration has to have: Area
        area, expected_conc = get_input_columns([area_object.id, expected_conc_object.id], dvs)
        if expected_conc != None and area == None:
            area_have_expected_conc = False

    # Marker(Calibration) has to have: Area, Expected concentration; and 2 rows
    cal_data = filter(lambda x: x.marker == 'Calibration', data_values)
    cal_by_analyte = groupby_column(cal_data, lambda dv: '{}'.format(dv.analyte_id))

    for _, dvs in cal_by_analyte:

        cal_by_series = groupby_column(dvs, lambda dv: '{}'.format(dv.series))

        for _, dvs in cal_by_series:

            cal_by_row = groupby_column(dvs, lambda dv: '{}'.format(dv.row))

            # Marker(Calibration) has  2 rows
            if len(cal_by_row) < 2:
                cal_has_2_rows = False

            # Marker(Calibration) has to have: Area, Expected concentration
            for _, dv in cal_by_row:
                area, expected_conc = get_input_columns([area_object.id, expected_conc_object.id], dv)
                if expected_conc == None or area == None:
                    cal_has_area_expect_conc = False

    samp_data = filter(lambda x: x.marker == 'Sample' or x.marker == 'Blank', data_values)
    samp_by_row = groupby_column(samp_data, lambda dv: '{}'.format(dv.row))
    for r, dvs in samp_by_row:

        [area] = get_input_columns([area_object.id], dvs)
        if area == None:
            samp_has_area = False

    cnt_by_analyte = groupby_column(data_values, lambda dv: '{}'.format(dv.analyte_id))

    # print('Min series: {} Min level: {} Min parallel: {} Min datarows: {}'.format(series, levels, parallels, datarows))

    for a, dv_analyte in cnt_by_analyte:

        # TODO what of series is named 'None'??
        cnt_by_series = groupby_column(dv_analyte, lambda x: '{}'.format(x.series))
        correct_series = 0

        for s, dv_series in cnt_by_series:

            cnt_by_level = groupby_column(dv_series, lambda x: '{}'.format(x.level))
            correct_levels = 0

            for l, dv_level in cnt_by_level:

                cnt_by_parallel = list(groupby_column(dv_level, lambda x: '{}'.format(x.parallel)))
                cnt_by_rows = list(groupby_column(dv_level, lambda x: '{}'.format(x.row)))

                # print('Analyte:{} Series: {} level: {} parallel: {} datarows: {}'.format(a,s, l, len(cnt_by_parallel), len(cnt_by_rows)))

                if parallels_op(len(cnt_by_parallel), parallels) and datarows_op(len(cnt_by_rows), datarows):
                    correct_levels += 1

            if levels_op(correct_levels, levels):
                correct_series += 1

        if not series_op(correct_series, series):
            enough_data = False

    result = []
    if not columns_present:
        result.append(1)
    if not name_present:
        result.append(2)
    if not area_have_expected_conc:
        result.append(4)
    if not cal_has_2_rows:
        result.append(8)
    if not cal_has_area_expect_conc:
        result.append(16)
    if not samp_has_area:
        result.append(32)
    if not enough_data:
        result.append(64)

    return result

check_parameters_type = namedtuple('check_parameters_type'\
    ,['marker', 'headers', 'series', 'levels', 'parallels', 'datarows', 'series_op', 'levels_op', 'parallels_op', 'datarows_op']\
    )
check_parameters_type.__new__.__defaults__ = ('', [], 0, 0, 0, 0, operator.ge, operator.ge, operator.ge, operator.ge)

def general_checks(data_values, check_parameters_list):
    """
    Check math module input data for general restrictions. Check return values for restrictions.
    Used to check by marker.

    @param - data_values - list of ExperimentDataValue objects. All input values from csv.
    @param - check_parameters_list - list of check_parameters_type objects

    Usage of check_parameters_type(marker, headers, series, levels, parallels, datarows, series_op, levels_op, parallels_op, datarows_op)
    marker - string representing the marker for which the check should apply. If marker=None, The checks are applyed on all data.
    headers - list of mandatory headers
    series - expected number of series
    levels - expected number of levels
    parallels - expected number of parallels
    datarows - expected number of datarows
    series_op - check operator for series number. operator.[ ge | gt | eq | le | lt ]. operator module should be imported.
    levels_op - look series_op
    parallels_op - look series_op
    datarows_op - look series_op

    @return - 0 - All OK
            - 1 - Mandatory column has missing value
            - 2 - Column 'Name' is missing for som row
            - 4 - Row has 'excpected concentration', but has missing 'area'
            - 8 - Marker(Calibration) does not have 2 rows
            - 16 - Marker(Calibration) does not have 'area' or 'expected concentration'
            - 32 - Marker(Sample) or Marker (Blank) does not have 'area'
            - 64 - Dataset is missing correct set of 'series', 'levels', 'parallels', 'replicates'
            - 128 - Internal standard data is incorrect
    """
    result = []
    in_keys = {}
    for key in ['is area', 'is expected concentration']:
        in_keys[key] = ExperimentData.objects.get(name=key, type=ExperimentData.INPUT).id

    # Check IS logic
    data_by_series = groupby_column(data_values, lambda dv: '{}'.format(dv.series))
    for series, dv in data_by_series:

        # Check that if IS data is present then all rows have IS data
        [is_area, is_expected_concentration], _, _, _, _, _ = \
            extract_columns(dv, [in_keys['is area'], in_keys['is expected concentration']])

        is_area_filtered = [a for a in is_area if a != None]
        is_expected_concentration_filtered = [ec for ec in is_expected_concentration if ec != None]

        if (is_area_filtered != is_area or is_expected_concentration_filtered != is_expected_concentration) \
            and (is_area_filtered != [] or is_expected_concentration_filtered != []):
            result.append(128)

    for chk_ps in check_parameters_list:
        if chk_ps.marker:
            data_filtered = [x for x in data_values if x.marker == chk_ps.marker]
        else:
            data_filtered = data_values
        errors = general_checks_by_marker(data_filtered, chk_ps.headers, chk_ps.series, chk_ps.levels, chk_ps.parallels, chk_ps.datarows,\
            chk_ps.series_op, chk_ps.levels_op, chk_ps.parallels_op, chk_ps.datarows_op)
        result.extend(errors)
    return result

t1_a_homo_data = [
{'freedim': 1.0, '0.1': 3.078, '0.05': 6.314, '0.025': 12.706, '0.01': 31.821, '0.005': 63.656, '0.001': 318.289, '0.0005': 636.578},
{'freedim': 2.0, '0.1': 1.886, '0.05': 2.92, '0.025': 4.303, '0.01': 6.965, '0.005': 9.925, '0.001': 22.328, '0.0005': 31.6},
{'freedim': 3.0, '0.1': 1.638, '0.05': 2.353, '0.025': 3.182, '0.01': 4.541, '0.005': 5.841, '0.001': 10.214, '0.0005': 12.924},
{'freedim': 4.0, '0.1': 1.533, '0.05': 2.132, '0.025': 2.776, '0.01': 3.747, '0.005': 4.604, '0.001': 7.173, '0.0005': 8.61},
{'freedim': 5.0, '0.1': 1.476, '0.05': 2.015, '0.025': 2.571, '0.01': 3.365, '0.005': 4.032, '0.001': 5.894, '0.0005': 6.869},
{'freedim': 6.0, '0.1': 1.44, '0.05': 1.943, '0.025': 2.447, '0.01': 3.143, '0.005': 3.707, '0.001': 5.208, '0.0005': 5.959},
{'freedim': 7.0, '0.1': 1.415, '0.05': 1.895, '0.025': 2.365, '0.01': 2.998, '0.005': 3.499, '0.001': 4.785, '0.0005': 5.408},
{'freedim': 8.0, '0.1': 1.397, '0.05': 1.86, '0.025': 2.306, '0.01': 2.896, '0.005': 3.355, '0.001': 4.501, '0.0005': 5.041},
{'freedim': 9.0, '0.1': 1.383, '0.05': 1.833, '0.025': 2.262, '0.01': 2.821, '0.005': 3.25, '0.001': 4.297, '0.0005': 4.781},
{'freedim': 10.0, '0.1': 1.372, '0.05': 1.812, '0.025': 2.228, '0.01': 2.764, '0.005': 3.169, '0.001': 4.144, '0.0005': 4.587},
{'freedim': 11.0, '0.1': 1.363, '0.05': 1.796, '0.025': 2.201, '0.01': 2.718, '0.005': 3.106, '0.001': 4.025, '0.0005': 4.437},
{'freedim': 12.0, '0.1': 1.356, '0.05': 1.782, '0.025': 2.179, '0.01': 2.681, '0.005': 3.055, '0.001': 3.93, '0.0005': 4.318},
{'freedim': 13.0, '0.1': 1.35, '0.05': 1.771, '0.025': 2.16, '0.01': 2.65, '0.005': 3.012, '0.001': 3.852, '0.0005': 4.221},
{'freedim': 14.0, '0.1': 1.345, '0.05': 1.761, '0.025': 2.145, '0.01': 2.624, '0.005': 2.977, '0.001': 3.787, '0.0005': 4.14},
{'freedim': 15.0, '0.1': 1.341, '0.05': 1.753, '0.025': 2.131, '0.01': 2.602, '0.005': 2.947, '0.001': 3.733, '0.0005': 4.073},
{'freedim': 16.0, '0.1': 1.337, '0.05': 1.746, '0.025': 2.12, '0.01': 2.583, '0.005': 2.921, '0.001': 3.686, '0.0005': 4.015},
{'freedim': 17.0, '0.1': 1.333, '0.05': 1.74, '0.025': 2.11, '0.01': 2.567, '0.005': 2.898, '0.001': 3.646, '0.0005': 3.965},
{'freedim': 18.0, '0.1': 1.33, '0.05': 1.734, '0.025': 2.101, '0.01': 2.552, '0.005': 2.878, '0.001': 3.61, '0.0005': 3.922},
{'freedim': 19.0, '0.1': 1.328, '0.05': 1.729, '0.025': 2.093, '0.01': 2.539, '0.005': 2.861, '0.001': 3.579, '0.0005': 3.883},
{'freedim': 20.0, '0.1': 1.325, '0.05': 1.725, '0.025': 2.086, '0.01': 2.528, '0.005': 2.845, '0.001': 3.552, '0.0005': 3.85},
{'freedim': 21.0, '0.1': 1.323, '0.05': 1.721, '0.025': 2.08, '0.01': 2.518, '0.005': 2.831, '0.001': 3.527, '0.0005': 3.819},
{'freedim': 22.0, '0.1': 1.321, '0.05': 1.717, '0.025': 2.074, '0.01': 2.508, '0.005': 2.819, '0.001': 3.505, '0.0005': 3.792},
{'freedim': 23.0, '0.1': 1.319, '0.05': 1.714, '0.025': 2.069, '0.01': 2.5, '0.005': 2.807, '0.001': 3.485, '0.0005': 3.768},
{'freedim': 24.0, '0.1': 1.318, '0.05': 1.711, '0.025': 2.064, '0.01': 2.492, '0.005': 2.797, '0.001': 3.467, '0.0005': 3.745},
{'freedim': 25.0, '0.1': 1.316, '0.05': 1.708, '0.025': 2.06, '0.01': 2.485, '0.005': 2.787, '0.001': 3.45, '0.0005': 3.725},
{'freedim': 26.0, '0.1': 1.315, '0.05': 1.706, '0.025': 2.056, '0.01': 2.479, '0.005': 2.779, '0.001': 3.435, '0.0005': 3.707},
{'freedim': 27.0, '0.1': 1.314, '0.05': 1.703, '0.025': 2.052, '0.01': 2.473, '0.005': 2.771, '0.001': 3.421, '0.0005': 3.689},
{'freedim': 28.0, '0.1': 1.313, '0.05': 1.701, '0.025': 2.048, '0.01': 2.467, '0.005': 2.763, '0.001': 3.408, '0.0005': 3.674},
{'freedim': 29.0, '0.1': 1.311, '0.05': 1.699, '0.025': 2.045, '0.01': 2.462, '0.005': 2.756, '0.001': 3.396, '0.0005': 3.66},
{'freedim': 30.0, '0.1': 1.31, '0.05': 1.697, '0.025': 2.042, '0.01': 2.457, '0.005': 2.75, '0.001': 3.385, '0.0005': 3.646},
{'freedim': 60.0, '0.1': 1.296, '0.05': 1.671, '0.025': 2, '0.01': 2.39, '0.005': 2.66, '0.001': 3.232, '0.0005': 3.46},
{'freedim': 120.0, '0.1': 1.289, '0.05': 1.658, '0.025': 1.98, '0.01': 2.358, '0.005': 2.617, '0.001': 3.16, '0.0005': 3.373},
{'freedim': 1000.0, '0.1': 1.282, '0.05': 1.646, '0.025': 1.962, '0.01': 2.33, '0.005': 2.581, '0.001': 3.098, '0.0005': 3.3},
{'freedim': float('inf'), '0.1': 1.282, '0.05': 1.645, '0.025': 1.96, '0.01': 2.326, '0.005': 2.576, '0.001': 3.091, '0.0005': 3.291}
]

t1_a_homo = pd.DataFrame(t1_a_homo_data)

def find_t1_a_homo(freedim, alfa):
    cut = t1_a_homo[['freedim', str(alfa)]]
    low_bound = cut[cut.freedim <= freedim]
    return low_bound.loc[low_bound['freedim'].idxmax()][str(alfa)]
    # hi_bound = cut[cut.freedim > freedim]

deeta005 = {
'2': 5.516,
'3': 4.456,
'4': 4.067,
'5': 3.870,
'6': 3.752,
'7': 3.673,
'8': 3.617,
'9': 3.575,
'10': 3.543,
'11': 3.517,
'12': 3.496,
'13': 3.479,
'14': 3.464,
'15': 3.451,
'16': 3.440,
'17': 3.431,
'18': 3.422,
'19': 3.415,
'20': 3.408,
'21': 3.402,
'22': 3.397,
'23': 3.392,
'24': 3.387,
'25': 3.383,
'26': 3.380,
'27': 3.376,
'28': 3.373,
'29': 3.370,
'30': 3.367,
'31': 3.365,
'32': 3.362,
'33': 3.360,
'34': 3.358,
'35': 3.356,
'36': 3.354,
'37': 3.352,
'38': 3.350,
'39': 3.349,
'40': 3.347,
'41': 3.346,
'42': 3.344,
'43': 3.343,
'44': 3.342,
'45': 3.341,
'46': 3.339,
'47': 3.338,
'48': 3.337,
'49': 3.336,
'50': 3.335
}
