import os
from celery import Celery
from django.conf import settings
from celery.schedules import crontab

# set the default Django settings module for the 'celery' program.
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'valchrom.settings')

if 'BROKER_SERVICE' in os.environ:
    BROKER_URL = f"amqp://guest:guest@{os.environ['BROKER_SERVICE']}:{os.environ['BROKER_PORT']}//"
    RESULT_BACKEND = f"redis://{os.environ['CACHE_SERVICE']}:{os.environ['CACHE_PORT']}"
    
else:
    BROKER_URL = "amqp://guest:guest@localhost:5672//"
    RESULT_BACKEND = "redis://localhost:6379"

app = Celery('valchrom', 
  broker=BROKER_URL,
  include=['experiment_validation.tasks']
)

# Using a string here means the worker will not have to
# pickle the object when using Windows.
# app.config_from_object('django.conf:settings')
# app.autodiscover_tasks(lambda: settings.INSTALLED_APPS)

app.conf.update(
    task_serializer = 'json',
    accept_content = ['json'],  # Ignore other content
    result_serializer = 'json',
    enable_utc = True,
    # timezone = settings.TIME_ZONE,
    beat_schedule = {
        'task-backup-database': {
            'task': 'experiment_validation.tasks.backup_database',
            'schedule': crontab(minute=0, hour=1)
        }
    }
)