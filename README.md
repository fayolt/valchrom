# valchrom_backend

## Requirements

- Python 2.7 or 3.x
- pip
- virtualenv

```sh
pip install virtualenv
```

## Start application

- Create a virtual environment

```sh
virtualenv valchrom_venv
```

- Activate virtual environment

```sh
valchrom_venv\Scripts\activate
```

- Install dependencies

```sh
pip install -r requirements.txt
```

- Move to project directory

```sh
cd valchrom
```

- Run migrations to sync up the database

```sh
python manage.py migrate
```

- Start development server

```sh
python manage.py runserver
```

Application should be available at [`localhost:8000`](http://localhost:8000)

## Available Endpoints

- List all experimental plans [`localhost:8000/plans/`](http://localhost:8000/plans/)

- List all analytes GET [`localhost:8000/plans/analytes`](http://localhost:8000/plans/analytes)

- Create an analyte POST [`localhost:8000/plans/analytes`](http://localhost:8000/plans/analytes)

- Show/Update/Delete details of an analyte GET/PUT/DELETE [`localhost:8000/plans/analytes/:id`](http://localhost:8000/plans/analytes/:id)

- List all analytical methods [`localhost:8000/plans/methods`](http://localhost:8000/plans/methods)

- List all validation guidelines [`localhost:8000/plans/guidelines`](http://localhost:8000/plans/guidelines)

- List all validation parameters [`localhost:8000/plans/parameters`](http://localhost:8000/plans/parameters)

- List all assessment methods [`localhost:8000/plans/assessments`](http://localhost:8000/plans/assessments)

- List all assessment criteria [`localhost:8000/plans/criteria`](http://localhost:8000/plans/criteria)

- List all validation plan templates [`localhost:8000/plans/templates`](http://localhost:8000/plans/templates)
