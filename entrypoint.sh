#!/bin/sh


set -e

# Wait for Postgres to become available.
until psql -h $DB_SERVICE -U $DB_USER -c '\q' 2>/dev/null; do
  >&2 echo "Postgres is unavailable - sleeping"
  sleep 1
done

echo "Postgres is available: continuing with database setup..."

# Potentially Set up the database
python /app/valchrom/manage.py migrate

# Potentially Collect static files
python /app/valchrom/manage.py collectstatic --noinput --clear

# echo "\nTesting the installation..."
# "Proove" that install was successful by running the tests

echo "Launching web server..."
# Start the web server
cd valchrom/
gunicorn valchrom.wsgi:application -w 2 -b :4000
# python /app/valchrom/manage.py runserver 0.0.0.0:4000
